/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import eu.wedgess.plantid.data.User;

/**
 * Helpers methods for accessing the application {@link SharedPreferences}
 * <p>
 * Created by gar on 27/12/17.
 */

public class PlantPreferenceManager {

    private static final String TAG = PlantPreferenceManager.class.getSimpleName();

    public static final String PREF_KEY_USER = "pref_key_user";
    private static final String PREF_USER_LEARNED_INTRO = "pref_user_learned_intro";
    public static final String PREF_HIDE_LOCATION = "prefHideLocation";
    public static final String PREF_SHOW_DELETE_CONFIRMATION = "prefShowDeleteConfirmation";
    public static final String PREF_ENABLE_PROFANITY_FILTER = "prefEnableProfanityFilter";

    // Not instantiable
    private PlantPreferenceManager() {
    }

    public static User getUser(SharedPreferences mSharedPreferences) {
        String userJson = getStringPref(PREF_KEY_USER, "", mSharedPreferences);
        return new Gson().fromJson(userJson, User.class);
    }

    public static void setShowUserIntro(boolean userLearned, Context context) {
        setBooleanPref(PREF_USER_LEARNED_INTRO, userLearned, context);
    }

    public static boolean showUserIntro(Context context) {
        return getBooleanPref(PREF_USER_LEARNED_INTRO, true, context);
    }

    public static void saveUser(User user, SharedPreferences sharedPreferences) {
        String userString = new Gson().toJson(user);
        setStringPref(PREF_KEY_USER, userString, sharedPreferences);
    }

    public static void deleteUser(SharedPreferences sharedPreferences) {
        removePref(PREF_KEY_USER, sharedPreferences);
    }

    public static boolean hideUserLocation(Context context) {
        return getBooleanPref(PREF_HIDE_LOCATION, false, context);
    }

    public static boolean filterProfanity(Context context) {
        return getBooleanPref(PREF_ENABLE_PROFANITY_FILTER, false, context);
    }

    public static boolean showDeleteConfirmation(Context context) {
        return getBooleanPref(PREF_SHOW_DELETE_CONFIRMATION, true, context);
    }

    private static void setBooleanPref(String name, boolean value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(name, value).apply();
    }

    private static boolean getBooleanPref(String name, boolean def, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(name, def);
    }

    private static void setIntPref(String name, int value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(name, value).apply();
    }

    private static int getIntPref(String name, int def, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(name, def);
    }

    private static String getStringPref(String name, String def, SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(name, def);
    }


    private static void setStringPref(String name, String value, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putString(name, value).apply();
    }

    public static boolean exists(String name, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains(name);
    }

    private static void removePref(String name, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().remove(name).apply();
    }
}
