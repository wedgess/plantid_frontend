/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.util.Log;

import java.util.List;

import eu.wedgess.plantid.data.Thread;

/**
 * Diff Util implementation to load differences in updated list of threads in the
 * {@link eu.wedgess.plantid.features.threads.ThreadsAdapter} and
 * {@link eu.wedgess.plantid.features.userdetails.UsersThreadsAdapter}
 * <p>
 * Created by gar on 22/02/18.
 */

public class ThreadsDiffCallback extends DiffUtil.Callback {

    private static final String TAG = ThreadsDiffCallback.class.getSimpleName();

    public static final String BUNDLE_THREAD_TITLE = "BUNDLE_THREAD_TITLE";
    public static final String BUNDLE_THREAD_MSG = "BUNDLE_THREAD_MSG";
    public static final String BUNDLE_THREAD_COMMENT_COUNT = "BUNDLE_THREAD_COMMENT_COUNT";
    public static final String BUNDLE_THREAD_SOLVED = "BUNDLE_THREAD_SOLVED";

    private final List<Thread> mOldThreadList;
    private final List<Thread> mNewThreadList;

    public ThreadsDiffCallback(List<Thread> oldThreadList, List<Thread> newThreadList) {
        this.mOldThreadList = oldThreadList;
        this.mNewThreadList = newThreadList;
    }

    @Override
    public int getOldListSize() {
        return mOldThreadList != null ? mOldThreadList.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return mNewThreadList != null ? mNewThreadList.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldThreadList.get(oldItemPosition).getId().equals(
                mNewThreadList.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Thread oldThread = mOldThreadList.get(oldItemPosition);
        final Thread newThread = mNewThreadList.get(newItemPosition);

        return oldThread.equals(newThread);
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        Thread newThread = mNewThreadList.get(newItemPosition);
        Thread oldThread = mOldThreadList.get(oldItemPosition);

        Log.i(TAG, "New thread: " + newThread.toString());
        Log.i(TAG, "Old thread: " + oldThread.toString());

        Bundle diff = new Bundle();
        if (!newThread.getTitle().equals(oldThread.getTitle())) {
            diff.putString(BUNDLE_THREAD_TITLE, newThread.getTitle());
        }
        if (!newThread.getMessage().equals(oldThread.getMessage())) {
            diff.putString(BUNDLE_THREAD_MSG, newThread.getMessage());
        }
        if (newThread.isSolved() != oldThread.isSolved()) {
            diff.putBoolean(BUNDLE_THREAD_SOLVED, newThread.isSolved());
        }
        if (newThread.getCommentCount() != oldThread.getCommentCount()) {
            diff.putInt(BUNDLE_THREAD_COMMENT_COUNT, newThread.getCommentCount());
        }
        if (diff.size() == 0) {
            return null;
        }
        return diff;
    }
}
