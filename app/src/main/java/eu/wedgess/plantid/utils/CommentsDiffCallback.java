/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import java.util.List;

import eu.wedgess.plantid.data.Comment;

/**
 * Diff utility for updating {@link eu.wedgess.plantid.features.threaddetails.ThreadCommentsAdapter}
 * {@link Comment} items.
 * <p>
 * Created by gar on 22/02/18.
 */

public class CommentsDiffCallback extends DiffUtil.Callback {

    private final List<Comment> mOldCommentsList;
    private final List<Comment> mNewCommentsList;

    public CommentsDiffCallback(List<Comment> oldCommentsList, List<Comment> newCommentsList) {
        this.mOldCommentsList = oldCommentsList;
        this.mNewCommentsList = newCommentsList;
    }

    @Override
    public int getOldListSize() {
        return mOldCommentsList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewCommentsList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldCommentsList.get(oldItemPosition).getId().equals(
                mNewCommentsList.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Comment oldComment = mOldCommentsList.get(oldItemPosition);
        final Comment newComment = mNewCommentsList.get(newItemPosition);

        return oldComment.equals(newComment);
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
