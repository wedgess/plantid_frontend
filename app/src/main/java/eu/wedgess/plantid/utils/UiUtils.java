/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.text.NumberFormat;
import java.util.Locale;

import eu.wedgess.plantid.R;

/**
 * User interface helper for animations.
 * <p>
 * Created by gar on 03/11/17.
 */

public class UiUtils {

    public static final String TAG = UiUtils.class.getSimpleName();

    private static final long SELECTED_ANIMATION_DURATION = 400;

    private UiUtils() {
        // non instantiable
    }

    //region Activity reveal animation
    public static void animateRevealHide(final Context ctx, FloatingActionButton fab, final View view, @ColorRes final int color,
                                         final int finalRadius, final OnRevealAnimationListener listener) {
        int cx = (view.getLeft() + view.getRight()) / 2;
        int cy = (view.getTop() + view.getBottom()) - (int) fab.getY();
        int initialRadius = view.getWidth();

        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, finalRadius);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setBackgroundColor(ctx.getResources().getColor(color));
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);
                listener.onRevealHide();
            }
        });
        anim.setDuration(ctx.getResources().getInteger(R.integer.animation_duration));
        anim.start();
    }

    public static void animateRevealShow(final Context ctx, final View view, final int startRadius,
                                         @ColorRes final int color, int x, int y, final OnRevealAnimationListener listener) {
        float finalRadius = (float) Math.hypot(view.getWidth(), view.getHeight());

        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, x, y, startRadius, finalRadius);
        anim.setDuration(ctx.getResources().getInteger(R.integer.animation_duration));
        anim.setStartDelay(100);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setBackgroundColor(ctx.getResources().getColor(color));
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.VISIBLE);
                listener.onRevealShow();
            }
        });
        anim.start();
    }

    //endregion

    //region Search bar animation

    public static void animateSearchToolbar(final Toolbar toolbar, boolean show, final Context context) {
        int centerPoint = getCircularRevealStartAndEnd(toolbar, 2, context);
        Animator circularRevealAnimator;

        if (show) {
            toolbar.setBackgroundColor(ContextCompat.getColor(context, android.R.color.background_light));
            circularRevealAnimator = ViewAnimationUtils.createCircularReveal(
                    toolbar, centerPoint, toolbar.getHeight() / 2, 0.0f, centerPoint);
            circularRevealAnimator.setDuration(350);
        } else {
            circularRevealAnimator = ViewAnimationUtils.createCircularReveal(
                    toolbar, centerPoint, toolbar.getHeight() / 2, centerPoint, 0.0f);
            circularRevealAnimator.setDuration(200);
            circularRevealAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    toolbar.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                }
            });
        }
        circularRevealAnimator.start();
        if (context instanceof AppCompatActivity) {
            ((AppCompatActivity) context).getWindow().setStatusBarColor(
                    ContextCompat.getColor(context, show ? android.R.color.darker_gray : R.color.colorPrimaryDark));
        }
    }

    @SuppressLint("PrivateResource")
    private static int getCircularRevealStartAndEnd(Toolbar toolbar, int numberOfMenuIcons, Context context) {
        return toolbar.getWidth() -
                (numberOfMenuIcons == 0 ? context.getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material) : 0) -
                ((context.getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * numberOfMenuIcons));
    }

    public static ValueAnimator colorChangeAnimator(int colorFrom, int colorTo, int duration) {
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(duration); // milliseconds
        return colorAnimation;
    }
    //endregion

    //region Text counter animation
    public static void setCountAnimation(final String count, final TextView textView) {
        if (!Strings.isNullOrEmpty(count)) {
            int startValue;
            try {
                startValue = Integer.parseInt(count);
                startCountAnimation(startValue, textView);
            } catch (NumberFormatException e) {
                Log.e(TAG, "Exception formatting string: " + count + " trying to recover --> " + e.getMessage());
                startValue = Integer.parseInt((count.replaceAll("[^\\d.]", ""))); // remove non digits
                startCountAnimation(startValue, textView);
            }
        }
    }

    public static void startCountAnimation(final int count, final TextView textView) {
        textView.clearAnimation();
        ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues(0, count);
        animator.setDuration(1000);
        animator.addUpdateListener(animation -> textView.setText(NumberFormat.getNumberInstance(Locale.ENGLISH).format((int) animation.getAnimatedValue())));
        animator.start();
    }


    //endregion

    //region Selectable adapter animations
    // play item selection animation for multi select (ChatHome & Contacts)
    public static void playSelectAnimation(final ImageView selectedIV, float scaleFrom, float scaleTo) {
        selectedIV.clearAnimation();
        ObjectAnimator selectedAnimatorX = ObjectAnimator.ofFloat(selectedIV, View.SCALE_X, scaleFrom, scaleTo)
                .setDuration(SELECTED_ANIMATION_DURATION);
        ObjectAnimator selectedAnimatorY = ObjectAnimator.ofFloat(selectedIV, View.SCALE_Y, scaleFrom, scaleTo)
                .setDuration(SELECTED_ANIMATION_DURATION);
        // animator set for send button so animations play at same time
        AnimatorSet unselectedSet = new AnimatorSet();
        unselectedSet.setInterpolator(new AnticipateOvershootInterpolator());
        unselectedSet.playTogether(selectedAnimatorX, selectedAnimatorY);

        // if deselecting item
        if (scaleFrom == 1f && scaleTo == 0f) {
            unselectedSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    // when animation ends hide the send button or we still get its clicks
                    selectedIV.setVisibility(View.GONE);
                }
            });
        } else {
            // selecting item
            selectedIV.setScaleX(0f);
            selectedIV.setScaleY(0f);
            selectedIV.setVisibility(View.VISIBLE);
        }
        unselectedSet.start();
    }

    /**
     * Get the selectableItemBackground programmatically so that the recycler items give
     * touch feedback. Such as ripple effect, setting an items background as R.color.normal_bg
     * or and other color will override touch feedback.
     *
     * @param context - the current context
     * @return - resource id for attribute
     */
    public static int getSelectedItemBgAttr(Context context) {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray = context.obtainStyledAttributes(attrs);
        int resId = typedArray.getResourceId(0, 0);
        typedArray.recycle();
        return resId;
    }
    //endregion

    /**
     * Darkens the status bar color
     *
     * @param color - the apps primary colour
     * @return - darkened colour
     */
    public static int darkenColor(final int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.80f; // value component
        return Color.HSVToColor(hsv);
    }
}
