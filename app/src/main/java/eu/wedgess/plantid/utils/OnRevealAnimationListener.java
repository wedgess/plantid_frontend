package eu.wedgess.plantid.utils;

/**
 * Reveal animation callback used by {@link eu.wedgess.plantid.features.cameracapture.CameraCaptureActivity}
 * and {@link UiUtils}
 * <p>
 * Created by gar on 03/11/17.
 */

public interface OnRevealAnimationListener {
    void onRevealHide();

    void onRevealShow();
}
