/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Date Utitlity class for converting server time to readable time.
 * And converts it to relative time from now.
 * <p>
 * Created by gar on 27/12/17.
 */

public class DateUtility {

    public static final String SERVER_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static String getDBDateFormat() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(SERVER_FORMAT, Locale.ENGLISH);
        return sdf.format(date);
    }

    public static Date serverDateToNormalDateObj(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(SERVER_FORMAT, Locale.ENGLISH);
        return sdf.parse(date);
    }

    public static String dateToHistoryDateString(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(SERVER_FORMAT, Locale.ENGLISH);
        SimpleDateFormat output = new SimpleDateFormat("dd MMMM", Locale.ENGLISH);
        Date d = sdf.parse(date);
        return output.format(d);
    }

    public static String dateToProfileDateString(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(SERVER_FORMAT, Locale.ENGLISH);
        SimpleDateFormat output = new SimpleDateFormat("dd MMMM YYYY", Locale.ENGLISH);
        Date d = sdf.parse(date);
        return output.format(d);
    }

    public static String getRelativeTime(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(SERVER_FORMAT, Locale.ENGLISH);
        Date currentDate = new Date();
        long currentDateLong = currentDate.getTime();
        Date d = sdf.parse(date);
        long oldDate = d.getTime();

        return DateUtils.getRelativeTimeSpanString(oldDate, currentDateLong, 0).toString();
    }
}
