/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import eu.wedgess.plantid.network.sync.StubProvider;

/**
 * Network utility class for checking internet connection, and houses the {@link ConnectionReceiver}.
 * Alos contains a method {@link #createDummyAccount(Context)} needed for {@link eu.wedgess.plantid.network.sync.SyncAdapter}
 * <p>
 * Created by gar on 25/01/18.
 */

public class NetworkUtils {

    public interface ConnectionReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }

    public static boolean isNetworkAvailable(ConnectivityManager connectivityManager) {
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // receiver for when internet connection changes
    public static class ConnectionReceiver extends BroadcastReceiver {
        private ConnectionReceiverListener connectionReceiverListener;

        public ConnectionReceiver(ConnectionReceiverListener connectionReceiverListener) {
            super();
            this.connectionReceiverListener = connectionReceiverListener;
        }

        @Override
        public void onReceive(Context context, Intent arg1) {
            if (connectionReceiverListener != null) {
                connectionReceiverListener.onNetworkConnectionChanged(NetworkUtils.isNetworkAvailable((ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE)));
            }
        }
    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account createDummyAccount(Context context) {
        Account dummyAccount = new Account("dummyaccount", "eu.wedgess.plantid");
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        accountManager.addAccountExplicitly(dummyAccount, null, null);
        ContentResolver.setSyncAutomatically(dummyAccount, StubProvider.AUTHORITY, true);
        return dummyAccount;
    }
}
