/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Location Utility class for reading user loacation. Used by {@link eu.wedgess.plantid.features.userdetails.UserDetailsFragment}
 * {@link eu.wedgess.plantid.features.createeditthread.CreateEditThreadActivity}
 * {@link eu.wedgess.plantid.features.createeditthread.CreateEditThreadActivity}
 * Created by gar on 27/12/17.
 */

public class LocationUtils {

    public static final String TAG = LocationUtils.class.getSimpleName();

    private LocationUtils() {
    }


    public static String getLocationName(Context context) {
        String cityname = "";
        //Best location provider is decided by the criteria
        Criteria criteria = new Criteria();

        Location location = getLastKnownLocation(context);

        Geocoder gcd = new Geocoder(context, Locale.getDefault());

        if (location != null) {
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    cityname = addresses.get(0).getLocality();
                    for (Address add : addresses) {
                        Log.i(TAG, add.getLocality());
                        Log.i(TAG, "Sub: " + add.getSubLocality());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();

            }
        }
        return cityname;
    }

    private static Location getLastKnownLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission")
            Location location = locationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            } else {
                Log.i(TAG, "location not NULL");
            }
            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = location;
            }
        }
        return bestLocation;
    }
}
