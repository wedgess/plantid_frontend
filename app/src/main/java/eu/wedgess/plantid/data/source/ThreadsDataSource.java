package eu.wedgess.plantid.data.source;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.data.Thread;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Access point for accessing thread data.
 * Imaplemented by: {@link eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource} &
 * {@link eu.wedgess.plantid.data.source.remote.ThreadsRemoteDataSource}
 * <p>
 * Created by gar on 01/12/17.
 */
public interface ThreadsDataSource {

    /**
     * Gets all the threads from the data source.
     *
     * @return all the threads from the data source.
     */
    Observable<List<Thread>> getThreads();

    Single<Thread> getThreadById(@NonNull String tid);

    Observable<List<Thread>> getThreadsByAuthorId(@NonNull String authorId);

    /**
     * Inserts the thread in the data source, or, if this is an existing thread, it updates it.
     *
     * @param thread the thread to be inserted or updated.
     */
    Completable insertThread(@NonNull Thread thread);

    Completable updateThread(@NonNull Thread thread);

    Completable deleteThread(@NonNull Thread thread);

    void refreshThreads();

    Single<List<Thread>> getDirtyThreads();

    Completable updateDirtyThread(@NonNull Thread thread);
}
