package eu.wedgess.plantid.data.source.remote;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.TimeUnit;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.source.HistoryDataSource;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import eu.wedgess.plantid.network.RetrofitInstance;
import eu.wedgess.plantid.network.api.requests.HistoryRequests;
import eu.wedgess.plantid.network.api.responses.HistoryItemResponse;
import eu.wedgess.plantid.network.api.responses.HistoryItemsResponse;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Singleton Retrofit class to connect to the API endpoints for history items.{@link History}
 * Endpoints are found at: {@link HistoryRequests}
 * <p>
 * Created by gar on 11/11/17.
 */

public class HistoryRemoteDataSource implements HistoryDataSource {

    private static final String TAG = HistoryRemoteDataSource.class.getSimpleName();
    private static final int NETWORK_TIMEOUT_SEC = 6;

    private final HistoryRequests mHistoryRequestService;

    // Prevent direct instantiation.
    public HistoryRemoteDataSource() {
        mHistoryRequestService = RetrofitInstance.getRetrofitInstance().create(HistoryRequests.class);
    }

    /**
     * Gets all the history items from the remote DB.
     *
     * @param uid - user id of who to fetch history items for
     * @return - all the users history items
     */
    @Override
    public Observable<List<History>> getHistory(@NonNull String uid) {
        return mHistoryRequestService
                .getUsersHistory(uid)
                .map(HistoryItemsResponse::getHistoryItems)
                .toObservable()
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    /**
     * Get a single {@link History} item bu its id.
     *
     * @param uid - user id of whos history item it is
     * @param hid - the history items id
     * @return
     */
    @Override
    public Single<History> getHistoryItemById(@NonNull final String uid, @NonNull final String hid) {
        return mHistoryRequestService
                .getHistoryItemById(uid, hid)
                .map(HistoryItemResponse::getHistoryItem)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable insertHistoryItem(@NonNull History historyItem) {
        // IGNORE -  remote does not save history items here, the ClassificationRemoteDataSource uploads classification item inisitally
        throw new RuntimeException("Remote does not insert a history item through here, see ClassificationRemoteDataSource#upload()");
    }

    @Override
    public Completable updateHistoryItem(@NonNull History historyItem) {
        return mHistoryRequestService
                .updateHistoryItem(historyItem.getAuthorId(), historyItem.getId(), historyItem)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable updateDirtyHistory(@NonNull History historyItem) {
        return insertHistoryItem(historyItem);
    }

    @Override
    public Completable deleteHistoryItems(@NonNull List<History> historyItems) {
        return mHistoryRequestService
                .deleteHistoryItems(CurrentUserInstance.getInstance().getCurrentUser().getUid(), historyItems)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable deleteHistoryItem(@NonNull History history) {
        throw new RuntimeException("Only delete a single history item from local data source not from remote");
    }

    @Override
    public void refreshHistoryItems() {
        // Not required because the {@link PlantsRepository} handles the logic of refreshing the
        // plants from all the available data sources.
    }

    @Override
    public Single<List<History>> getDirtyHistory() {
        throw new RuntimeException("History items in remote are never dirty, must only be called from HistoryLocalDataSource");
    }
}
