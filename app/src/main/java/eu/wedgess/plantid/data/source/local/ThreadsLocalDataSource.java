package eu.wedgess.plantid.data.source.local;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.ThreadsDataSource;
import eu.wedgess.plantid.data.source.local.dao.ThreadDao;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Local data source for connecting to the thread table of the local database.
 * Used by: {@link ThreadsRepository}
 * <p>
 * Created by gar on 10/11/17.
 */

public class ThreadsLocalDataSource implements ThreadsDataSource {

    private static final String TAG = ThreadsLocalDataSource.class.getSimpleName();

    private final ThreadDao mThreadDao;
    
    public ThreadsLocalDataSource(@NonNull ThreadDao threadDao) {
        mThreadDao = checkNotNull(threadDao);
    }

    @Override
    public Observable<List<Thread>> getThreads() {
        return mThreadDao
                .getAll()
                .toObservable();
    }

    @Override
    public Single<Thread> getThreadById(@NonNull final String tid) {
        return mThreadDao
                .getThreadById(tid);
    }

    @Override
    public Observable<List<Thread>> getThreadsByAuthorId(@NonNull String authorId) {
        return mThreadDao
                .getThreadsByAuthorId(authorId)
                .toObservable();
    }

    @Override
    public Completable insertThread(@NonNull final Thread thread) {
        return Completable
                .fromAction(() -> mThreadDao
                        .insertThread(thread));
    }

    @Override
    public Completable updateThread(@NonNull Thread thread) {
        return Completable
                .fromAction(() -> mThreadDao
                        .updateThread(thread));
    }

    @Override
    public Completable deleteThread(@NonNull Thread thread) {
        return Completable
                .fromAction(() -> mThreadDao
                        .deleteThread(thread));
    }

    @Override
    public void refreshThreads() {
        // TODO: Not yet implemented
    }

    @Override
    public Single<List<Thread>> getDirtyThreads() {
        return mThreadDao
                .getDirtyThreads();
    }

    @Override
    public Completable updateDirtyThread(@NonNull Thread thread) {
        return updateThread(thread);
    }
}
