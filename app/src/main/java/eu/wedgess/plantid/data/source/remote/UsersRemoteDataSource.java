package eu.wedgess.plantid.data.source.remote;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;
import com.google.gson.Gson;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.UserDataSource;
import eu.wedgess.plantid.network.RetrofitInstance;
import eu.wedgess.plantid.network.api.requests.UserRequests;
import eu.wedgess.plantid.network.api.responses.UserResponse;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Singleton Retrofit class to connect to the API endpoints for users.{@link User}
 * Endpoints are found at: {@link UserRequests}
 * <p>
 * Created by gar on 11/11/17.
 */
public class UsersRemoteDataSource implements UserDataSource {

    private static final String TAG = UsersRemoteDataSource.class.getSimpleName();
    private static final int NETWORK_TIMEOUT_SEC = 6;

    private final UserRequests mUserRequestsService;

    public UsersRemoteDataSource() {
        mUserRequestsService = RetrofitInstance.getRetrofitInstance().create(UserRequests.class);
    }

    @Override
    public Maybe<User> getUserById(@NonNull final String uid) {
        return mUserRequestsService
                .getUserById(uid)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .map(UserResponse::getUser);
    }

    @Override
    public Completable insertOrUpdateUser(@NonNull User user) {
        throw new RuntimeException("Cannot insert user directly to remote, must use registerUser");
    }

    @Override
    public Completable insertCurrentUserToPreferences(@NonNull User user) {
        throw new RuntimeException("Cannot insert current user to preference from remote repository");
    }

    @Override
    public Single<User> logUserIn(@NonNull final String username, @NonNull final String password) {
        HashMap<String, String> params = new HashMap<>(2);
        params.put("username", username);
        params.put("password", password);

        return mUserRequestsService
                .performLogin(params)
                .map(UserResponse::getUser)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Single<User> registerUser(@NonNull final User user, File image) {
        String json = new Gson().toJson(user);
        MultipartBody.Part body = null;
        if (image != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), image);
            body = MultipartBody.Part.createFormData("u_img_url", image.getName(), reqFile);
        }
        RequestBody jsonData = RequestBody.create(MediaType.parse("multipart/form-data"), json);

        return mUserRequestsService
                .registerUser(body, jsonData).map(UserResponse::getUser)
                .timeout(NETWORK_TIMEOUT_SEC + 2, TimeUnit.SECONDS);
    }

    @Override
    public Single<User> uploadProfileImage(@NonNull File profileImage) {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), profileImage);
        MultipartBody.Part body = MultipartBody.Part.createFormData("u_img_url", profileImage.getName(), reqFile);
        // create new user id - user id needed to upload profile image
        return mUserRequestsService
                .uploadProfileImage(UUID.randomUUID().toString(), body)
                .map(UserResponse::getUser)
                .timeout(NETWORK_TIMEOUT_SEC * 2, TimeUnit.SECONDS);
    }

    @Override
    public Single<Optional<User>> getCurrentUser() {
        // IGNORE - local data source uses only
        throw new RuntimeException("Current user cannot be fetched from the remote, fetch from local ONLY!");
    }


    // used to keep remote local user in sync with remote user, incase details changed on another device
    public Maybe<User> getCurrentUser(@NonNull String uid) {
        // IGNORE - local data source uses only
        return mUserRequestsService.getUserById(uid).map(UserResponse::getUser);
    }

    @Override
    public Completable deleteUser(@NonNull User user) {
        return mUserRequestsService.deleteUserProfile(user.getUid());
    }

    @Override
    public Completable signUserOut(@NonNull User user) {
        // IGNORE - Local data source handles this
        throw new RuntimeException("Cannot sign out from remote");
    }

    @Override
    public Completable deleteUserProfile(@NonNull String uid) {
        return mUserRequestsService
                .deleteUserProfile(uid);
    }

    @Override
    public Single<User> updateUserProfile(@NonNull User user, @NonNull File image) {
        String json = new Gson().toJson(user);
        MultipartBody.Part body = null;
        if (image != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), image);
            body = MultipartBody.Part.createFormData("u_img_url", image.getName(), reqFile);
        }
        RequestBody jsonData = RequestBody.create(MediaType.parse("multipart/form-data"), json);

        return mUserRequestsService
                .updateUser(user.getUid(), body, jsonData).map(UserResponse::getUser)
                .timeout(NETWORK_TIMEOUT_SEC + 2, TimeUnit.SECONDS);
    }
}
