package eu.wedgess.plantid.data.source.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.ThreadsDataSource;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The repository is responsible of handling thread data operations.
 * Mainly used by : {@link eu.wedgess.plantid.features.threads.ThreadsPresenter}
 * <p>
 * Created by gar on 1/12/17.
 */
public class ThreadsRepository implements ThreadsDataSource {

    private static final String TAG = ThreadsRepository.class.getSimpleName();

    private final ThreadsDataSource mThreadsLocalDataSource;
    private final ThreadsDataSource mThreadsRemoteDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    private Map<String, Thread> mCachedThreads;
    private boolean mCacheIsDirty = false;
    private static volatile ThreadsRepository sSoleInstance;

    public static ThreadsRepository getInstance(@NonNull ThreadsDataSource threadsLocalDataSource,
                                                @NonNull ThreadsDataSource threadsRemoteDataSource) {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (ThreadsRepository.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new ThreadsRepository(threadsLocalDataSource, threadsRemoteDataSource);
            }
        }

        return sSoleInstance;
    }

    private ThreadsRepository(@NonNull ThreadsDataSource threadsLocalDataSource,
                              @NonNull ThreadsDataSource threadsRemoteDataSource) {
        //Prevent form the reflection api.
        if (sSoleInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }

        mThreadsLocalDataSource = checkNotNull(threadsLocalDataSource);
        mThreadsRemoteDataSource = checkNotNull(threadsRemoteDataSource);
    }

    @Override
    public void refreshThreads() {
        mCacheIsDirty = true;
    }

    @Override
    public Single<List<Thread>> getDirtyThreads() {
        return mThreadsLocalDataSource.getDirtyThreads();
    }

    @Override
    public Completable updateDirtyThread(@NonNull Thread thread) {
        return mThreadsRemoteDataSource.insertThread(thread)
                .andThen(mThreadsLocalDataSource
                        .updateThread(thread)
                        .doOnComplete(() -> {
                            if (mCachedThreads != null) {
                                mCachedThreads.put(thread.getId(), thread);
                            }
                        }));
    }

    private Observable<List<Thread>> getAndSaveRemoteThreads() {
        return mThreadsRemoteDataSource.getThreads()
                .flatMap(threadList -> Observable.fromIterable(threadList)
                        .doOnNext(thread -> {
                            mThreadsLocalDataSource.insertThread(thread)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.io())
                                    .subscribe(() -> Log.i(TAG, "Inserted thread item"),
                                            throwable -> {
                                                Log.e(TAG, "Error inserting thread item");
                                            });
                            mCachedThreads.put(thread.getId(), thread);
                        }).toList()
                        .toObservable());
    }

    private Observable<List<Thread>> getAndCacheLocalThreads() {
        return mThreadsLocalDataSource.getThreads()
                .flatMap(threadList -> Observable.fromIterable(threadList)
                        .doOnNext(thread -> {
                            mCachedThreads.put(thread.getId(), thread);
                            Log.i(TAG, "Thread added to cache");
                        })
                        .toList()
                        .toObservable());
    }


    private Observable<List<Thread>> getAndSaveRemoteThreadsForAuthorId(@NonNull String authorId) {
        return mThreadsRemoteDataSource.getThreadsByAuthorId(authorId)
                .flatMap(threadList -> Observable.fromIterable(threadList)
                        .doOnNext(thread ->
                                mThreadsLocalDataSource
                                        .insertThread(thread)
                                        .subscribeOn(Schedulers.io())
                                        .subscribe(() -> Log.i(TAG, "Inserted thread item"),
                                                throwable -> {
                                                    Log.e(TAG, "Error inserting thread item");
                                                })).toList()
                        .toObservable());
    }


    @Override
    public Observable<List<Thread>> getThreads() {
        // Respond immediately with cache if available and not dirty
        if (mCachedThreads != null && !mCachedThreads.isEmpty() && !mCacheIsDirty) {
            Log.i(TAG, "[CACHE] Got Threads from cache: " + mCachedThreads.size());
            List<Thread> threads = new ArrayList<>(mCachedThreads.values());
            return Observable.just(threads);
        } else if (mCachedThreads == null) {
            mCachedThreads = new LinkedHashMap<>();
        }

        Observable<List<Thread>> remoteThreads = getAndSaveRemoteThreads();
        Observable<List<Thread>> localThreads = getAndCacheLocalThreads();

        if (mCacheIsDirty) {
            Log.i(TAG, "[CACHE] Cache is dirty getting threads from remote");
            // If the cache is dirty we need to fetch new data from the network.
            return remoteThreads
                    .onErrorResumeNext(localThreads);
        } else {
            // Query the remote storage, on error fetch from local, otherwise load local data, until network data has been fetched
            // on first start the locaThreads will be empty, so take the first item and filter if list is empty, if empty switch to network
            return remoteThreads
                    .onErrorResumeNext(localThreads)
                    .publish(network ->
                            localThreads
                                    .takeUntil(network)
                                    .take(1)
                                    .filter(list -> !list.isEmpty())
                                    .switchIfEmpty(network));
        }
    }

    @Override
    public Single<Thread> getThreadById(@NonNull final String tid) {
        checkNotNull(tid);

        Thread cachedThread = getThreadWithId(tid);

        // Respond immediately with cache if available
        if (cachedThread != null) {
            return Single.just(cachedThread);
        }


        Single<Thread> remoteThread = mThreadsRemoteDataSource.getThreadById(tid);
        Single<Thread> localThread = mThreadsLocalDataSource.getThreadById(tid);

        return localThread
                .onErrorResumeNext(remoteThread);
    }

    @Override
    public Observable<List<Thread>> getThreadsByAuthorId(@NonNull String authorId) {
        List<Thread> cachedThreads = getCachedThreadsByAuthorId(authorId);
        if (!cachedThreads.isEmpty()) {
            return Observable.just(cachedThreads);
        }

        Observable<List<Thread>> remoteThreads = getAndSaveRemoteThreadsForAuthorId(authorId);
        Observable<List<Thread>> localThreads = mThreadsLocalDataSource.getThreadsByAuthorId(authorId);

        // Query the local storage if available. If not, query the network.
        return remoteThreads
                .onErrorResumeNext(localThreads)
                .publish(network ->
                        localThreads
                                .takeUntil(network)
                                .take(1)
                                .filter(list -> !list.isEmpty())
                                .switchIfEmpty(network));
    }

    private List<Thread> getCachedThreadsByAuthorId(String authorId) {
        List<Thread> threads = new ArrayList<>();
        if (mCachedThreads != null && !mCachedThreads.isEmpty()) {
            for (Thread thread : mCachedThreads.values()) {
                if (thread.getAuthor() != null
                        && thread.getAuthor().getUid().equals(authorId)) {
                    threads.add(thread);
                }
            }
        }
        return threads;
    }

    @Override
    public Completable insertThread(@NonNull final Thread thread) {
        return mThreadsLocalDataSource
                .insertThread(thread)
                .doOnComplete(() -> {
                    if (mCachedThreads != null) {
                        mCachedThreads.put(thread.getId(), thread);
                    }
                })
                .andThen(mThreadsRemoteDataSource
                        .insertThread(thread));
    }

    @Override
    public Completable updateThread(@NonNull Thread thread) {
        return mThreadsLocalDataSource
                .updateThread(thread
                ).doOnComplete(() -> {
                    if (mCachedThreads != null) {
                        mCachedThreads.put(thread.getId(), thread);
                    }
                }).andThen(mThreadsRemoteDataSource.updateThread(thread)
                        .doOnError(throwable -> {
                            thread.setDirty(true);
                            mThreadsLocalDataSource
                                    .updateThread(thread)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(() -> {
                                        Log.e(TAG, "updated thread as dirty and saved locally because could not update in remote DB");
                                    });
                        }));
    }

    @Override
    public Completable deleteThread(@NonNull Thread thread) {
        return mThreadsLocalDataSource
                .deleteThread(thread)
                .doOnComplete(() -> deleteItemsFromCache(thread))
                .andThen(mThreadsRemoteDataSource
                        .deleteThread(thread)
                        .doOnError(throwable -> {
                            thread.setDirty(true);
                            updateThread(thread);
                        }));
    }

    private void deleteItemsFromCache(Thread thread) {
        Thread hist = getThreadWithId(thread.getId());
        if (hist != null && hist.getId().equals(thread.getId())) {
            mCachedThreads.remove(hist.getId());
        }
    }

    @Nullable
    private Thread getThreadWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedThreads != null && !mCachedThreads.isEmpty()) {
            for (Thread thread : mCachedThreads.values()) {
                if (thread.getId().equals(id)) {
                    return thread;
                }
            }
        }
        return null;
    }

    public void removeCachedComment(Comment comment) {
        if (mCachedThreads != null && !mCachedThreads.isEmpty()) {
            for (Iterator<Map.Entry<String, Thread>> i = mCachedThreads.entrySet().iterator(); i.hasNext(); ) {
                Map.Entry<String, Thread> entry = i.next();
                if (entry.getValue().getId().equals(comment.getThreadId())) {
                    for (Comment c : entry.getValue().getComments()) {
                        if (comment.getId().equals(c.getId())) {
                            entry.getValue().getComments().remove(c);
                            Log.i(TAG, "Removing comment: " + c.toString());
                            return;
                        }
                    }
                }
            }
        }
    }
}
