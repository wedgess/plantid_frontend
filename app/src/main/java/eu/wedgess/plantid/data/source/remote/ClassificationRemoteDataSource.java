package eu.wedgess.plantid.data.source.remote;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.concurrent.TimeUnit;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.source.ClassificationDataSource;
import eu.wedgess.plantid.network.RetrofitInstance;
import eu.wedgess.plantid.network.api.requests.HistoryRequests;
import eu.wedgess.plantid.network.api.responses.HistoryItemResponse;
import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Singleton Retrofit class to connect to user classification end points from {@link HistoryRequests}.
 * Used to upload classification as user history item.
 * <p>
 * <p>
 * Created by gar on 11/11/17.
 */

public class ClassificationRemoteDataSource implements ClassificationDataSource {

    private static final String TAG = ClassificationRemoteDataSource.class.getSimpleName();
    private static final int NETWORK_TIMEOUT_SEC = 5;

    /**
     * Upload images for classification with the users location and id in order to save the history
     * item regardless of whether it is matched or not. The item will finally be updated only if the
     * user clicks the "match" button.
     * <p>
     * NOT USED ANYMORE, TFLITE WORKS LOCALLY AND IS MUCH QUICKER & OFFLINE USAGE
     *
     * @param location - useers location
     * @param userId   - id
     * @param image    - image being classified
     * @return
     */
    @Override
    public Single getClassificationResults(@NonNull String location, @NonNull String userId, @NonNull File image) {
        throw new RuntimeException("Classification runs locally via TensorFlow");
    }


    /**
     * Uploads the Image the user is attempting to classify to the server where it will be stored
     * as a history item for the current user. If the user matches the classified image then this
     * history item will be updated from {@link HistoryRemoteDataSource}.
     *
     * @param location - users location
     * @param uid      - current users id
     * @param image    - image the user is classifying
     * @return
     */
    @Override
    public Single<History> uploadIdentificationImage(@NonNull String location, @NonNull String uid, @NonNull File image) {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), image);
        MultipartBody.Part body = MultipartBody.Part.createFormData("img_upload", image.getName(), reqFile);
        // send the location as form data as the image needs to be sent also
        RequestBody jsonData = RequestBody.create(MediaType.parse("multipart/form-data"), location);

        HistoryRequests service = RetrofitInstance.getRetrofitInstance().create(HistoryRequests.class);
        return service
                .createHistoryItem(uid, body, jsonData)
                .map(HistoryItemResponse::getHistoryItem)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }
}
