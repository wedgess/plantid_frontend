package eu.wedgess.plantid.data.source.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import eu.wedgess.plantid.data.Comment;
import io.reactivex.Single;

/**
 * Database Access Object for {@link Comment} table.
 * <p>
 * Created by gar on 01/12/17.
 */

@Dao
public interface CommentsDao {

    @Query("SELECT * FROM comments")
    Single<List<Comment>> getAll();

    @Query("SELECT * FROM comments where c_thread_id = :threadId")
    Single<List<Comment>> findByThreadId(String threadId);

    @Query("SELECT * FROM comments WHERE c_dirty = 1")
    Single<List<Comment>> getDirtyComments();

    /**
     * Select a comment by id.
     *
     * @param cid the comment id.
     * @return the comment with pid.
     */
    @Query("SELECT * FROM comments WHERE id = :cid")
    Single<Comment> getCommentById(String cid);

    /**
     * Insert a comment in the database. If the thread already exists, replace it.
     *
     * @param comment the thread to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertComment(Comment comment);

    /**
     * Update a comment.
     *
     * @param comment thread to be updated
     * @return the number of thread updated. This should always be 1.
     */
    @Update
    int updateComment(Comment comment);

    @Delete
    void deleteComment(Comment comment);

}