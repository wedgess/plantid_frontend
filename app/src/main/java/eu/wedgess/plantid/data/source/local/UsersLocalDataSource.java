package eu.wedgess.plantid.data.source.local;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.io.File;

import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.UserDataSource;
import eu.wedgess.plantid.data.source.local.dao.UserDao;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.utils.PlantPreferenceManager;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Local data source for connecting to the users table of the local database.
 * Used by: {@link UserRepository}
 * <p>
 * <p>
 * Created by gar on 10/11/17.
 */
@SuppressWarnings("Guava")
public class UsersLocalDataSource implements UserDataSource {

    private static final String TAG = UsersLocalDataSource.class.getSimpleName();

    private final UserDao mUserDao;
    private final SharedPreferences mSharedPreferences;

    // Prevent direct instantiation.
    public UsersLocalDataSource(@NonNull UserDao userDao,
                                @NonNull SharedPreferences sharedPreferences) {
        mUserDao = checkNotNull(userDao, "User DAO cannot be null");
        mSharedPreferences = checkNotNull(sharedPreferences, "SharedPreferences cannot be null");
    }

    @Override
    public Maybe<User> getUserById(@NonNull final String uid) {
        return mUserDao.getUserById(uid);
    }

    @Override
    public Completable insertOrUpdateUser(@NonNull final User user) {
        return Completable
                .fromAction(() -> upsert(user));
    }

    private void upsert(User user) {
        long id = mUserDao.insert(user);
        if (id == -1) {
            mUserDao.update(user);
        }
    }

    @Override
    public Completable insertCurrentUserToPreferences(@NonNull User user) {
        return Completable
                .fromAction(() -> PlantPreferenceManager
                        .saveUser(user, mSharedPreferences))
                .andThen(insertOrUpdateUser(user));
    }

    @Override
    public Single<User> logUserIn(@NonNull String username, @NonNull String password) {
        // IGNORE - Only for remote data source
        throw new RuntimeException("Users can't login locally");
    }

    @Override
    public Single<User> registerUser(@NonNull User user, File image) {
        // IGNORE - Remote only uses this
        throw new RuntimeException("Users can't register locally");
    }

    @Override
    public Single<User> uploadProfileImage(@NonNull File profileImage) {
        // IGNORE - Only remote needs this
        throw new RuntimeException("Images cannot be uplaoded locally");
    }

    @Override
    public Single<Optional<User>> getCurrentUser() {
        return Single
                .just(Optional
                        .fromNullable(PlantPreferenceManager
                                .getUser(mSharedPreferences)));
    }

    @Override
    public Completable deleteUser(@NonNull final User user) {
        return Completable
                .fromAction(() -> mUserDao
                        .delete(user));
    }

    @Override
    public Completable signUserOut(@NonNull User user) {
        return Completable
                .fromAction(() -> PlantPreferenceManager
                        .deleteUser(mSharedPreferences))
                .andThen(deleteUser(user));
    }

    @Override
    public Completable deleteUserProfile(@NonNull String uid) {
        return Completable
                .fromAction(() ->
                        PlantPreferenceManager.deleteUser(mSharedPreferences))
                .andThen(
                        Completable
                                .fromAction(() ->
                                        mUserDao.deleteUserById(uid)));
    }

    @Override
    public Single<User> updateUserProfile(@NonNull User user, @NonNull File image) {
        throw new RuntimeException("Use insertOrUpdate instead, this method is only used in remote data source");
    }
}
