package eu.wedgess.plantid.data.source.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.base.Optional;

import java.io.File;

import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.UserDataSource;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The repository is responsible of handling user data operations.
 * Mianly used by {@link eu.wedgess.plantid.features.userdetails.UserDetailsPresenter}
 * <p>
 * Created by gar on 10/11/17.
 */
@SuppressWarnings("Guava")
public class UserRepository implements UserDataSource {

    private static final String TAG = UserRepository.class.getSimpleName();

    private final UserDataSource mUsersLocalDataSource;
    private final UserDataSource mUsersRemoteDataSource;

    private User mCachedUser;
    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    boolean mCacheIsDirty = false;

    private static volatile UserRepository sSoleInstance;

    public static UserRepository getInstance(@NonNull UserDataSource usersLocalDataSource,
                                             @NonNull UserDataSource usersRemoteDataSource) {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (UserRepository.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new UserRepository(usersLocalDataSource, usersRemoteDataSource);
            }
        }

        return sSoleInstance;
    }

    private UserRepository(UserDataSource usersLocalDataSource, UserDataSource usersRemoteDataSource) {
        //Prevent form the reflection api.
        if (sSoleInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
        mUsersLocalDataSource = checkNotNull(usersLocalDataSource);
        mUsersRemoteDataSource = checkNotNull(usersRemoteDataSource);
    }

    @Override
    public Maybe<User> getUserById(@NonNull final String uid) {
        checkNotNull(uid);
        User cachedUser = getUserWithId(uid);
        if (cachedUser != null) {
            Maybe.just(cachedUser);
        }
        Maybe<User> remoteUserId = getUserByIdFromRemote(uid);
        Maybe<User> localUserId = mUsersLocalDataSource.getUserById(uid);
        return remoteUserId.onErrorResumeNext(localUserId);
    }


    private Maybe<User> getUserByIdFromRemote(@NonNull final String uid) {
        //TODO: Implement cache
        return mUsersRemoteDataSource
                .getUserById(uid)
                .doOnSuccess(user -> mUsersLocalDataSource
                        .insertOrUpdateUser(user)
                        .subscribeOn(Schedulers.io())
                        .subscribe(() -> Log.i(TAG, "User inserted"))
                );
    }

    @Nullable
    private User getUserWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedUser != null && mCachedUser.getUid().equals(id)) {
            return mCachedUser;
        }
        return null;
    }

    @Override
    public Completable insertOrUpdateUser(@NonNull User user) {
        // user is stored in remote through register so only need to store locally here
        return mUsersLocalDataSource
                .insertOrUpdateUser(user);
    }

    @Override
    public Completable insertCurrentUserToPreferences(@NonNull User user) {
        return mUsersLocalDataSource
                .insertCurrentUserToPreferences(user)
                .andThen(Completable.fromAction(() -> CurrentUserInstance.getInstance().setCurrentUser(user)));
    }

    @Override
    public Single<User> logUserIn(@NonNull String username, @NonNull String password) {
        return mUsersRemoteDataSource
                .logUserIn(username, password)
                .doOnSuccess(user -> {
                    mCachedUser = user;
                });
    }

    @Override
    public Single<User> registerUser(@NonNull final User user, File image) {
        return mUsersRemoteDataSource
                .registerUser(user, image)
                .doOnSuccess(registeredUser -> {
                    mCachedUser = registeredUser;
                    mUsersLocalDataSource
                            .insertOrUpdateUser(registeredUser);
                });
    }

    @Override
    public Single<User> uploadProfileImage(@NonNull File profileImage) {
        return mUsersRemoteDataSource
                .uploadProfileImage(profileImage);
    }

    @Override
    public Single<Optional<User>> getCurrentUser() {
        // Respond immediately with cache if available
        if (mCachedUser != null) {
            return Single
                    .just(Optional.fromNullable(mCachedUser));
        }

        return getCurrentUserFromLocalRepository()
                .doOnSuccess(user -> mCachedUser = user.isPresent() ? user.get() : null);
    }

    @NonNull
    private Single<Optional<User>> getCurrentUserFromLocalRepository() {
        return mUsersLocalDataSource
                .getCurrentUser()
                .doOnSuccess(userOptional -> {
                    if (userOptional.isPresent()) {
                        mCachedUser = userOptional.get();
                    }
                });
    }

    // similar to below, only used by users presenter to get the current user and keep locsal user in sync
    @NonNull
    public Maybe<User> getCurrentUserFromRemoteRepository(@NonNull String uid) {
        return mUsersRemoteDataSource
                .getUserById(uid);
    }

    // allow for cached user to be reset from presenter. (only used when remote and current user are different)
    public void updateCachedUser(@NonNull User user) {
        this.mCachedUser = user;
    }

    @Override
    public Completable deleteUser(@NonNull User user) {
        mCachedUser = null;
        return mUsersRemoteDataSource
                .deleteUser(user)
                .andThen(mUsersLocalDataSource
                        .deleteUser(user));
    }

    @Override
    public Completable signUserOut(@NonNull User user) {
        return mUsersLocalDataSource
                .signUserOut(user)
                .andThen(deleteUser(user));
    }

    @Override
    public Completable deleteUserProfile(@NonNull String uid) {
        return mUsersRemoteDataSource
                .deleteUserProfile(uid)
                .andThen(mUsersLocalDataSource
                        .deleteUserProfile(uid));
    }

    @Override
    public Single<User> updateUserProfile(@NonNull User user, @Nullable File image) {
        return mUsersRemoteDataSource
                .updateUserProfile(user, image)
                .doOnSuccess(user1 -> mCachedUser = user1);
    }
}
