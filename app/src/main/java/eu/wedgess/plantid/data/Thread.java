package eu.wedgess.plantid.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Thread object:
 * <p>
 * {@link ColumnInfo} is for Room database column name mapping
 * {@link SerializedName} is for GSON mapping
 * <p>
 * Created by gar on 26/11/17.
 */
@Entity(tableName = "threads", foreignKeys = {
//        @ForeignKey(entity = User.class,
//                parentColumns = "u_id",
//                childColumns = "author_id",
//                onDelete = ForeignKey.NO_ACTION)
})
public class Thread extends Post {

    @Expose
    @NonNull
    @SerializedName("t_title")
    @ColumnInfo(name = "t_title")
    private String title;

    @Expose
    @NonNull
    @SerializedName("t_img_url")
    @ColumnInfo(name = "t_img_url")
    private String threadImgUrl;

    @Expose
    @Nullable
    @SerializedName("t_location")
    @ColumnInfo(name = "t_location")
    private String location;

    @Expose
    @SerializedName("t_solved")
    @ColumnInfo(name = "t_solved")
    private boolean solved;

    @Expose
    @ColumnInfo(name = "t_dirty")
    private boolean dirty;

    @Ignore
    private List<Comment> comments;

    public User getAuthor() {
        return author;
    }

    @Ignore
    private User author;

    public Thread(String pid, @NonNull String title, String message, String authorId, String dateCreated, @NonNull String threadImgUrl, @Nullable String location, boolean solved, boolean dirty) {
        super(pid, message, authorId, dateCreated);
        this.title = title;
        this.threadImgUrl = threadImgUrl;
        this.location = location;
        this.solved = solved;
        this.dirty = dirty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThreadImgUrl() {
        return threadImgUrl;
    }

    public void setThreadImgUrl(String threadImgUrl) {
        this.threadImgUrl = threadImgUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

    @Override
    public String toString() {
        return "Thread{" +
                " id='" + pid + '\'' +
                ", title='" + title + '\'' +
                ", threadImgUrl='" + threadImgUrl + '\'' +
                ", location='" + location + '\'' +
                ", date='" + getDateCreated() + '\'' +
                ", message='" + getMessage() + '\'' +
                ", commentCount='" + getCommentCount() + '\'' +
                ", authorId='" + getAuthorId() + '\'' +
                ", solved=" + solved +
                '}';
    }

    public int getCommentCount() {
        return comments == null ? 0 : comments.size();
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Thread)) return false;

        Thread thread = (Thread) o;

        if (solved != thread.solved) return false;
        if (dirty != thread.dirty) return false;
        if (!title.equals(thread.title)) return false;
        if (!threadImgUrl.equals(thread.threadImgUrl)) return false;
        if (location != null ? !location.equals(thread.location) : thread.location != null)
            return false;
        if (comments != null ? !comments.equals(thread.comments) : thread.comments != null)
            return false;
        return author != null ? author.equals(thread.author) : thread.author == null;
    }


    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + threadImgUrl.hashCode();
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (solved ? 1 : 0);
        result = 31 * result + (dirty ? 1 : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.threadImgUrl);
        dest.writeString(this.location);
        dest.writeByte(this.solved ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dirty ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.comments);
        dest.writeParcelable(this.author, flags);
    }

    protected Thread(Parcel in) {
        this.title = in.readString();
        this.threadImgUrl = in.readString();
        this.location = in.readString();
        this.solved = in.readByte() != 0;
        this.dirty = in.readByte() != 0;
        this.comments = in.createTypedArrayList(Comment.CREATOR);
        this.author = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Thread> CREATOR = new Creator<Thread>() {
        @Override
        public Thread createFromParcel(Parcel source) {
            return new Thread(source);
        }

        @Override
        public Thread[] newArray(int size) {
            return new Thread[size];
        }
    };
}
