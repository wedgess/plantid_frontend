package eu.wedgess.plantid.data.source.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.util.List;

import eu.wedgess.plantid.data.Classifier;
import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.source.ClassificationDataSource;
import io.reactivex.Single;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The repository is responsible of handling all image classification data operations.
 * Used by teh presenters, main usage is at:
 * -- {@link eu.wedgess.plantid.features.classification.ClassificationPresenter}
 * <p>
 * Created by gar on 10/11/17.
 */
public class ClassificationRepository implements ClassificationDataSource {

    private static final String TAG = ClassificationRepository.class.getSimpleName();
    private static final String INVALID_PLANT = "-1";

    private final ClassificationDataSource mClassificationsLocalDataSource;
    private final ClassificationDataSource mClassificationsRemoteDataSource;

    // after the history item has been created on the server store it in case it is matched with a plant by user
    private History mHistoryItem;

    private static volatile ClassificationRepository sSoleInstance;

    public static ClassificationRepository getInstance(@NonNull ClassificationDataSource classificationLocalDataSource,
                                                       @NonNull ClassificationDataSource classificationRemoteDataSource) {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (ClassificationRepository.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new ClassificationRepository(classificationLocalDataSource, classificationRemoteDataSource);
            }
        }

        return sSoleInstance;
    }

    private ClassificationRepository(@NonNull ClassificationDataSource classificationLocalDataSource,
                                     @NonNull ClassificationDataSource classificationRemoteDataSource) {
        //Prevent form the reflection api.
        if (sSoleInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
        mClassificationsLocalDataSource = checkNotNull(classificationLocalDataSource);
        mClassificationsRemoteDataSource = checkNotNull(classificationRemoteDataSource);
    }

    /**
     * Only run locally, remote does not classify images.
     */
    @Override
    public Single<List<Classifier.Recognition>> getClassificationResults(@Nullable final String location,
                                                                         @Nullable final String userId, @Nullable final File image) {
        return mClassificationsLocalDataSource
                .getClassificationResults(location, userId, image);
    }

    @Override
    public Single<History> uploadIdentificationImage(@NonNull String location, @NonNull String uid, @NonNull File image) {
        return mClassificationsRemoteDataSource
                .uploadIdentificationImage(location, uid, image)
                .map(history -> {
                    mHistoryItem = history;
                    return history;
                });
    }

    // returns the history item created during classification
    public History getHistoryItem() {
        return mHistoryItem;
    }
}
