package eu.wedgess.plantid.data.source.remote;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;
import java.util.concurrent.TimeUnit;

import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.source.PlantDataSource;
import eu.wedgess.plantid.network.RetrofitInstance;
import eu.wedgess.plantid.network.api.requests.PlantRequests;
import eu.wedgess.plantid.network.api.responses.PlantsListResponse;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

/**
 * Singleton Retrofit class to connect to the API endpoints for plants.{@link Plant}
 * Endpoints are found at: {@link PlantRequests}
 * <p>
 * Created by gar on 11/11/17.
 */

@SuppressWarnings("Guava")
public class PlantsRemoteDataSource implements PlantDataSource {

    private static final String TAG = PlantsRemoteDataSource.class.getSimpleName();
    private static final int NETWORK_TIMEOUT_SEC = 5;

    private final PlantRequests mPlantRequestService;
    
    public PlantsRemoteDataSource() {
        mPlantRequestService = RetrofitInstance.getRetrofitInstance().create(PlantRequests.class);
    }

    /**
     * Gets all the plants {@link Plant} from the remote database.
     *
     * @return
     */
    @Override
    public Observable<List<Plant>> getPlants() {
        return mPlantRequestService
                .getPlantsData()
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .map((Function<PlantsListResponse, List<Plant>>) PlantsListResponse::getPlants).toObservable();
    }

    @Override
    public Single<Optional<Plant>> getPlantById(@NonNull final String pid) {
        return mPlantRequestService
                .getPlantById(pid)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .map(response -> Optional.fromNullable(response.getPlant()));
    }

    @Override
    public Completable insertOrUpdatePlant(Plant plant) {
        // IGNORE -- The remote DB is READ ONLY -- Nothing to do here
        throw new RuntimeException("Client cannot update or insert plant on remote!");
    }

    @Override
    public void refreshPlants() {
        // Not required because the {@link PlantsRepository} handles the logic of refreshing the
        // plants from all the available data sources.
    }
}
