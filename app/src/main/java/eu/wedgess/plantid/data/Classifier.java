package eu.wedgess.plantid.data;

/*
 *    Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 * Generic interface for interacting with different recognition engines.
 * Modified form original.
 */
public interface Classifier {
    /**
     * An immutable result returned by a Classifier describing what was recognized.
     * Modified for GSON parsing {@link SerializedName}.
     */
    class Recognition implements Parcelable {
        /**
         * A unique identifier for what has been recognized. Specific to the class, not the instance of
         * the object.
         */
        @Expose
        @SerializedName("c_id")
        private final String id;

        /**
         * Display name for the recognition.
         */
        @Expose
        @SerializedName("c_title")
        private final String title;

        /**
         * Id of plant from DB
         */
        @Expose
        @SerializedName("c_plant_id")
        private final String plantId;

        /**
         * A sortable score for how good the recognition is relative to others. Higher should be better.
         */
        @Expose
        @SerializedName("c_confidence")
        private final Float confidence;

        private Plant plantInfo;

        public Recognition(final String id, final String title, final Float confidence, final String plantId) {
            this.id = id;
            this.title = title;
            this.confidence = confidence;
            this.plantId = plantId;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public Float getConfidence() {
            return confidence;
        }

        public String getPlantId() {
            return plantId;
        }

        public String getDisplayableConfidence() {
            return String.format(Locale.ENGLISH, "(%.1f%%) ", confidence * 100.0f);
        }

        @Override
        public String toString() {
            String resultString = "";
            if (id != null) {
                resultString += "[" + id + "] ";
            }

            if (title != null) {
                resultString += title + " ";
            }

            if (confidence != null) {
                resultString += String.format(Locale.ENGLISH, "(%.1f%%) ", confidence * 100.0f);
            }

            if (plantId != null) {
                resultString += " " + plantId;
            }

            return resultString.trim();
        }

        public Plant getPlantInfo() {
            return plantInfo;
        }

        public void setPlantInfo(Plant plantInfo) {
            this.plantInfo = plantInfo;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.title);
            dest.writeString(this.plantId);
            dest.writeValue(this.confidence);
            dest.writeParcelable(this.plantInfo, flags);
        }

        protected Recognition(Parcel in) {
            this.id = in.readString();
            this.title = in.readString();
            this.plantId = in.readString();
            this.confidence = (Float) in.readValue(Float.class.getClassLoader());
            this.plantInfo = in.readParcelable(Plant.class.getClassLoader());
        }

        public static final Parcelable.Creator<Recognition> CREATOR = new Parcelable.Creator<Recognition>() {
            @Override
            public Recognition createFromParcel(Parcel source) {
                return new Recognition(source);
            }

            @Override
            public Recognition[] newArray(int size) {
                return new Recognition[size];
            }
        };
    }
}

