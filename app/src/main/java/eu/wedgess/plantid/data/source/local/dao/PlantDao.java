package eu.wedgess.plantid.data.source.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import eu.wedgess.plantid.data.Plant;
import io.reactivex.Single;

/**
 * Plants database access object {@link Plant}
 * <p>
 * Created by gar on 10/11/17.
 */

@Dao
public interface PlantDao {

    @Query("SELECT * FROM plants")
    Single<List<Plant>> getAll();

    @Query("SELECT * FROM plants")
    Single<List<Plant>> getAllPlants();

    /**
     * Select a plant by id.
     *
     * @param pid the plant id.
     * @return the plant with pid.
     */
    @Query("SELECT * FROM plants WHERE p_id = :pid")
    Single<Plant> getPlantById(String pid);

    /**
     * Insert a plant in the database. If the plant already exists, replace it.
     *
     * @param plant the plant to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPlant(Plant plant);
}