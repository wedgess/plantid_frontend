package eu.wedgess.plantid.data.source.local;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.source.HistoryDataSource;
import eu.wedgess.plantid.data.source.local.dao.HistoryDao;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Local data source for connecting to the history table of the local database.
 * Used by: {@link HistoryRepository}
 * <p>
 * Created by gar on 10/11/17.
 */

public class HistoryLocalDataSource implements HistoryDataSource {

    private final HistoryDao mHistoryDao;

    public HistoryLocalDataSource(@NonNull HistoryDao historyDao) {
        mHistoryDao = checkNotNull(historyDao);
    }

    /**
     * Get all users history from local DB
     *
     * @param uid - ignored for local data source - only remote needs uid param
     */
    @Override
    public Observable<List<History>> getHistory(@Nullable String uid) {
        return mHistoryDao
                .getAll()
                .toObservable();
    }

    /**
     * @param uid- can be null - only need for remote data source
     * @param hid  - history item id to get
     */
    @Override
    public Single<History> getHistoryItemById(@Nullable String uid, @NonNull final String hid) {
        return mHistoryDao
                .findById(hid);
    }

    @Override
    public Completable insertHistoryItem(@NonNull History historyItem) {
        return Completable
                .fromAction(() -> upsert(historyItem));
    }

    @Override
    public Completable updateHistoryItem(@NonNull History historyItem) {
        return Completable
                .fromAction(() -> upsert(historyItem));
    }

    @Override
    public Completable updateDirtyHistory(@NonNull History historyItem) {
        return insertHistoryItem(historyItem);
    }

    @Override
    public Completable deleteHistoryItems(@NonNull List<History> historyItems) {
        return Completable
                .fromAction(() -> mHistoryDao
                        .deleteAll(historyItems));
    }

    @Override
    public Completable deleteHistoryItem(@NonNull History history) {
        return Completable.fromAction(() -> mHistoryDao.deleteHistory(history));
    }

    private void upsert(History history) {
        long id = mHistoryDao.insert(history);
        if (id == -1) {
            mHistoryDao.update(history);
        }
    }

    @Override
    public void refreshHistoryItems() {
        // Not required because the {@link HistoryRepository} handles the logic of refreshing the
        // plants from all the available data sources.
    }

    @Override
    public Single<List<History>> getDirtyHistory() {
        return mHistoryDao.getDirtyHistory();
    }
}
