package eu.wedgess.plantid.data.source.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Optional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.source.PlantDataSource;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The repository is responsible from handling plant data operations.
 * Mainly used by: {@link eu.wedgess.plantid.features.plants.PlantsPresenter}
 * <p>
 * Created by gar on 10/11/17.
 */
public class PlantsRepository implements PlantDataSource {

    private static final String TAG = PlantsRepository.class.getSimpleName();

    private final PlantDataSource mPlantsLocalDataSource;
    private final PlantDataSource mPlantsRemoteDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    private Map<String, Plant> mCachedPlants;
    private boolean mCacheIsDirty = false;

    private static volatile PlantsRepository sSoleInstance;

    public static PlantsRepository getInstance(@NonNull PlantDataSource plantsLocalDataSource,
                                               @NonNull PlantDataSource plantsRemoteDataSource) {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time
            synchronized (PlantsRepository.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new PlantsRepository(plantsLocalDataSource, plantsRemoteDataSource);
            }
        }

        return sSoleInstance;
    }

    private PlantsRepository(PlantDataSource plantsLocalDataSource, PlantDataSource plantsRemoteDataSource) {
        mPlantsLocalDataSource = checkNotNull(plantsLocalDataSource);
        mPlantsRemoteDataSource = checkNotNull(plantsRemoteDataSource);
    }

    @Override
    public void refreshPlants() {
        // this will re-pull from remote
        mCacheIsDirty = true;
    }

    private void refreshCache(List<Plant> plants) {
        if (mCachedPlants == null) {
            mCachedPlants = new LinkedHashMap<>();
        }
        mCachedPlants.clear();
        for (Plant plant : plants) {
            mCachedPlants.put(plant.getId(), plant);
        }
        mCacheIsDirty = false;
    }


    private Observable<List<Plant>> getAndSaveRemotePlants() {
        if (mCachedPlants == null) {
            mCachedPlants = new LinkedHashMap<>();
        }
        return mPlantsRemoteDataSource
                .getPlants()
                .flatMap(plants -> Observable.fromIterable(plants)
                        .doOnNext(plant -> {
                            mPlantsLocalDataSource
                                    .insertOrUpdatePlant(plant)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(() -> mCachedPlants.put(plant.getId(), plant));
                        })
                        .toList()
                        .toObservable())
                .doOnComplete(() -> mCacheIsDirty = false);
    }

    private Observable<List<Plant>> getAndCacheLocalPlants() {
        return mPlantsLocalDataSource.getPlants()
                .flatMap(plants -> Observable.fromIterable(plants)
                        .doOnNext(plant -> mCachedPlants.put(plant.getId(), plant))
                        .toList()
                        .toObservable());
    }

    @Override
    public Observable<List<Plant>> getPlants() {
        // Respond immediately with cache if available and not dirty
        if (mCachedPlants != null && !mCachedPlants.isEmpty() && !mCacheIsDirty) {
            return Observable
                    .fromIterable(mCachedPlants.values())
                    .toList()
                    .toObservable();
        } else if (mCachedPlants == null) {
            mCachedPlants = new LinkedHashMap<>();
        }

        Observable<List<Plant>> remotePlants = getAndSaveRemotePlants();
        if (mCacheIsDirty) {
            return remotePlants;
        } else {
            // Query the local storage if available. If not, query the network.
            Observable<List<Plant>> localPlants = getAndCacheLocalPlants();
            return remotePlants
                    .onErrorResumeNext(localPlants)
                    .publish(network ->
                            Observable.concat(network,
                                    localPlants
                                            .takeUntil(network)));
        }
    }

    @Override
    public Single<Optional<Plant>> getPlantById(@NonNull final String pid) {
        checkNotNull(pid);

        Plant cachedPlant = getPlantWithId(pid);
        // Respond immediately with cache if available
        if (cachedPlant != null) {
            return Single.just(Optional.fromNullable(cachedPlant));
        }

        Single<Optional<Plant>> remotePlant = mPlantsRemoteDataSource.getPlantById(pid);
        Single<Optional<Plant>> localPlant = mPlantsLocalDataSource.getPlantById(pid);

        return localPlant
                .onErrorResumeNext(remotePlant);
    }

    @Override
    public Completable insertOrUpdatePlant(Plant plant) {
        return mPlantsLocalDataSource
                .insertOrUpdatePlant(plant);
    }

    @Nullable
    private Plant getPlantWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedPlants != null && !mCachedPlants.isEmpty()) {
            for (Plant plant : mCachedPlants.values()) {
                if (plant.getId().equals(id)) {
                    return plant;
                }
            }
        }
        return null;
    }
}
