package eu.wedgess.plantid.data.source;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.data.Comment;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Access point for accessing commesnts data.
 * Implemented by both:
 * {@link eu.wedgess.plantid.data.source.local.CommentsLocalDataSource} &
 * {@link eu.wedgess.plantid.data.source.remote.CommentsRemoteDataSource}
 * <p>
 * Created by gar on 01/12/17.
 */
public interface CommentsDataSource {

    /**
     * Gets all the comments from the data source.
     *
     * @return all the comments from the data source with given threadId.
     */
    Observable<List<Comment>> getCommentsByThreadId(@NonNull String threadId);

    /**
     * Get comment by its id, thread id is required for endpoints route.
     */
    Single<Comment> getCommentById(@NonNull final String tid, @NonNull final String cid);

    /**
     * Inserts the comment in the data source, or, if this is an existing comment, it updates it.
     *
     * @param comment the comment to be inserted or updated.
     */
    Completable insertOrUpdateComment(@NonNull Comment comment);

    Completable updateComment(@NonNull Comment comment);

    Completable updateDirtyComment(@NonNull Comment comment);

    Completable deleteComment(@NonNull Comment comment);

    void refreshComments();

    Single<List<Comment>> getDirtyComments();
}
