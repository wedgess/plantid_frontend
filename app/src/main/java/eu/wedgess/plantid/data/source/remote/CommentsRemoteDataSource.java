package eu.wedgess.plantid.data.source.remote;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.TimeUnit;

import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.source.CommentsDataSource;
import eu.wedgess.plantid.network.RetrofitInstance;
import eu.wedgess.plantid.network.api.requests.CommentRequests;
import eu.wedgess.plantid.network.api.responses.CommentResponse;
import eu.wedgess.plantid.network.api.responses.CommentsResponse;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Singleton Retrofit class to connect to the API endpoints for comments.{@link Comment}
 * Endpoints are found at: {@link CommentRequests}
 * <p>
 * Created by gar on 11/11/17.
 */

public class CommentsRemoteDataSource implements CommentsDataSource {

    private static final String TAG = CommentsRemoteDataSource.class.getSimpleName();
    private static final int NETWORK_TIMEOUT_SEC = 6;

    private final CommentRequests mCommentsRequestService;

    // Prevent direct instantiation.
    public CommentsRemoteDataSource() {
        mCommentsRequestService = RetrofitInstance.getRetrofitInstance().create(CommentRequests.class);
    }

    /**
     * Get a list of comments {@link Comment} for a thread by the thread id {@link Thread}.
     *
     * @param threadId - thread id to get comments for
     * @return - comments for the thread
     */
    @Override
    public Observable<List<Comment>> getCommentsByThreadId(@NonNull String threadId) {
        return mCommentsRequestService
                .getCommentsByThreadId(threadId)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .map(CommentsResponse::getComments);
    }

    /**
     * Get a single {@link Comment} by its id.
     *
     * @param tid - thread is the comment belongs to
     * @param cid - comments id to get
     * @return - the comment
     */
    @Override
    public Single<Comment> getCommentById(@NonNull final String tid, @NonNull final String cid) {
        return mCommentsRequestService
                .getCommentById(tid, cid)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .map(CommentResponse::getComment);
    }

    @Override
    public Completable insertOrUpdateComment(@NonNull Comment comment) {
        return mCommentsRequestService
                .createComment(comment.getThreadId(), comment)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable updateComment(@NonNull Comment comment) {
        return mCommentsRequestService
                .updateComment(comment.getThreadId(), comment.getId(), comment)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable updateDirtyComment(@NonNull Comment comment) {
        return insertOrUpdateComment(comment);
    }

    @Override
    public Completable deleteComment(@NonNull Comment comment) {
        return mCommentsRequestService
                .deleteComment(comment.getThreadId(), comment.getId())
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public void refreshComments() {
        throw new RuntimeException("Not yet implemented refreshComments.");
    }

    @Override
    public Single<List<Comment>> getDirtyComments() {
        throw new RuntimeException("Comments are not dirty on remote. Only local comments can be dirty");
    }
}
