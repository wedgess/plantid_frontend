package eu.wedgess.plantid.data.source.local;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.source.CommentsDataSource;
import eu.wedgess.plantid.data.source.local.dao.CommentsDao;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Local data source for connecting to the comments table of the local database.
 * Used by: {@link CommentsRepository}
 * <p>
 * Created by gar on 10/11/17.
 */

public class CommentsLocalDataSource implements CommentsDataSource {

    private static final String TAG = CommentsLocalDataSource.class.getSimpleName();

    private final CommentsDao mCommentDao;

    public CommentsLocalDataSource(@NonNull CommentsDao commentsDao) {
        mCommentDao = checkNotNull(commentsDao);
    }

    @Override
    public Observable<List<Comment>> getCommentsByThreadId(@NonNull final String threadId) {
        return mCommentDao
                .findByThreadId(threadId)
                .toObservable();
    }

    @Override
    public Single<Comment> getCommentById(@NonNull final String tid, @NonNull final String cid) {
        return mCommentDao
                .getCommentById(cid);
    }

    @Override
    public Completable insertOrUpdateComment(@NonNull final Comment comment) {
        return Completable
                .fromAction(() -> mCommentDao
                        .insertComment(comment))
                .doOnComplete(() -> Log.i(TAG, "Inserted comment"));
    }

    @Override
    public Completable updateComment(@NonNull Comment comment) {
        return Completable
                .fromAction(() -> mCommentDao
                        .updateComment(comment));
    }

    @Override
    public Completable updateDirtyComment(@NonNull Comment comment) {
        return updateComment(comment);
    }

    @Override
    public Completable deleteComment(@NonNull Comment comment) {
        return Completable
                .fromAction(() -> mCommentDao
                        .deleteComment(comment));
    }

    @Override
    public void refreshComments() {
        // IGNORE
    }

    @Override
    public Single<List<Comment>> getDirtyComments() {
        return mCommentDao.getDirtyComments();
    }
}
