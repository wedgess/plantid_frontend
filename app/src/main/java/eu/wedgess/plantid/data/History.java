package eu.wedgess.plantid.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Thread object:
 * <p>
 * {@link ColumnInfo} is for Room database column name mapping
 * {@link SerializedName }is for GSON mapping
 * {@link Ignore} - ignore certain fields in database
 * <p>
 * Created by gar on 07/12/17.
 */

@Entity(tableName = "history",
        foreignKeys = {
                @ForeignKey(entity = User.class,
                        parentColumns = "u_id",
                        childColumns = "h_user_id",
                        onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value = "h_user_id")})
public class History implements Parcelable {

    @Expose
    @PrimaryKey
    @NonNull
    @SerializedName("h_id")
    @ColumnInfo(name = "h_id")
    private String id;

    @Expose
    @SerializedName("h_img_url")
    @ColumnInfo(name = "h_img_url")
    private String imgUrl;

    @Expose
    @SerializedName("h_is_matched")
    @ColumnInfo(name = "h_is_matched")
    private boolean isMatch;

    @Expose
    @SerializedName("h_plant_id")
    @ColumnInfo(name = "h_plant_id")
    private String plantId;

    @Expose
    @NonNull
    @SerializedName("h_date_created")
    @ColumnInfo(name = "h_date_created")
    private String idDate;

    @Expose
    @SerializedName("h_location")
    @ColumnInfo(name = "h_location")
    private String location;

    @Expose
    @SerializedName("h_user_id")
    @ColumnInfo(name = "h_user_id")
    private String authorId;

    @ColumnInfo(name = "h_dirty")
    private boolean dirty;

    @Ignore
    private Plant plantInfo;


    public History(@NonNull String id, @Nullable String imgUrl, @NonNull String authorId, boolean isMatch, String plantId, @NonNull String idDate, @Nullable String location, boolean dirty) {
        this.id = id;
        this.dirty = dirty;
        this.setImgUrl(imgUrl);
        this.setMatch(isMatch);
        this.plantId = plantId;
        this.idDate = idDate;
        this.setLocation(location);
        this.authorId = authorId;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getPlantId() {
        return plantId;
    }

    public void setPlantId(@NonNull String plantId) {
        this.plantId = plantId;
    }

    @NonNull
    public String getIdDate() {
        return idDate;
    }

    public void setIdDate(@NonNull String idDate) {
        this.idDate = idDate;
    }

    @NonNull
    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(@Nullable String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @NonNull
    public boolean isMatch() {
        return isMatch;
    }

    public void setMatch(@NonNull boolean match) {
        isMatch = match;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public Plant getPlantInfo() {
        return plantInfo;
    }

    public void setPlantInfo(Plant plantInfo) {
        this.plantInfo = plantInfo;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    @Override
    public String toString() {
        return "History{" +
                "id='" + id + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", isMatch=" + isMatch +
                ", plantId='" + plantId + '\'' +
                ", idDate='" + idDate + '\'' +
                ", location='" + location + '\'' +
                ", authorId='" + authorId + '\'' +
                ", plantInfo=" + plantInfo +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.imgUrl);
        dest.writeByte(this.isMatch ? (byte) 1 : (byte) 0);
        dest.writeString(this.plantId);
        dest.writeString(this.idDate);
        dest.writeString(this.location);
        dest.writeString(this.authorId);
        dest.writeByte(this.dirty ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.plantInfo, flags);
    }

    protected History(Parcel in) {
        this.id = in.readString();
        this.imgUrl = in.readString();
        this.isMatch = in.readByte() != 0;
        this.plantId = in.readString();
        this.idDate = in.readString();
        this.location = in.readString();
        this.authorId = in.readString();
        this.dirty = in.readByte() != 0;
        this.plantInfo = in.readParcelable(Plant.class.getClassLoader());
    }

    public static final Parcelable.Creator<History> CREATOR = new Parcelable.Creator<History>() {
        @Override
        public History createFromParcel(Parcel source) {
            return new History(source);
        }

        @Override
        public History[] newArray(int size) {
            return new History[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        History history = (History) o;

        if (isMatch != history.isMatch) return false;
        if (!id.equals(history.id)) return false;
        if (imgUrl != null ? !imgUrl.equals(history.imgUrl) : history.imgUrl != null) return false;
        if (plantId != null ? !plantId.equals(history.plantId) : history.plantId != null)
            return false;
        if (!idDate.equals(history.idDate)) return false;
        if (location != null ? !location.equals(history.location) : history.location != null)
            return false;
        if (authorId != null ? !authorId.equals(history.authorId) : history.authorId != null)
            return false;
        return plantInfo != null ? plantInfo.equals(history.plantInfo) : history.plantInfo == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (imgUrl != null ? imgUrl.hashCode() : 0);
        result = 31 * result + (isMatch ? 1 : 0);
        result = 31 * result + (plantId != null ? plantId.hashCode() : 0);
        result = 31 * result + idDate.hashCode();
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        result = 31 * result + (plantInfo != null ? plantInfo.hashCode() : 0);
        return result;
    }
}
