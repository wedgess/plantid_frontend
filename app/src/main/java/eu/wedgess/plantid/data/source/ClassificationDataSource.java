package eu.wedgess.plantid.data.source;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.List;

import eu.wedgess.plantid.data.Classifier;
import eu.wedgess.plantid.data.History;
import io.reactivex.Single;

/**
 * Access point for classifying an image.
 * Implemented by both {@link eu.wedgess.plantid.data.source.local.ClassificationLocalDataSource}
 * and the remote {@link eu.wedgess.plantid.data.source.remote.ClassificationRemoteDataSource}
 * <p>
 * Created by gar on 09/11/17.
 */
public interface ClassificationDataSource {

    /**
     * Gets classficiation results by running Tensorflow Lite locally.
     * -- {@link eu.wedgess.plantid.data.source.remote.ClassificationRemoteDataSource}
     * -- doesn't use this acces point anymore.
     *
     * @return all the plants from the data source.
     */
    Single<List<Classifier.Recognition>> getClassificationResults(@NonNull String location, @NonNull String userId, @NonNull File image);

    /**
     * Uploads the image the user is classifying while the user is viewing results,
     * this way if it is matched the update will be instant and no wait for image uploads.
     *
     * @param location - users location
     * @param uid      - users id
     * @param image    - the i mage the user is classiying
     * @return
     */
    Single<History> uploadIdentificationImage(@NonNull String location, @NonNull String uid, @NonNull File image);

}
