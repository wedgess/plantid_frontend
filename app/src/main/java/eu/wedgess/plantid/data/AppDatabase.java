/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.wedgess.plantid.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import eu.wedgess.plantid.data.source.local.dao.CommentsDao;
import eu.wedgess.plantid.data.source.local.dao.HistoryDao;
import eu.wedgess.plantid.data.source.local.dao.PlantDao;
import eu.wedgess.plantid.data.source.local.dao.ThreadDao;
import eu.wedgess.plantid.data.source.local.dao.UserDao;

/**
 * The Room Database that contains the apps database table.
 * Used to setup DB and access the Database Access Objects
 */
@Database(entities = {User.class, Plant.class, Thread.class, Comment.class, History.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "PlantSnap.db";

    private static AppDatabase INSTANCE;

    public abstract UserDao userDao();

    public abstract PlantDao plantDao();

    public abstract ThreadDao threadDao();

    public abstract CommentsDao commentDao();

    public abstract HistoryDao historyDao();

    private static final Object sLock = new Object();

    public static AppDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, DATABASE_NAME)
                        .build();
            }
            return INSTANCE;
        }
    }

}
