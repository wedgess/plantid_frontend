package eu.wedgess.plantid.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

/**
 * Plant object:
 * <p>
 * {@link ColumnInfo} is for Room database column name mapping
 * {@link SerializedName} is for GSON mapping
 * <p>
 * Created by Gareth on 27/10/2017.
 */
@Entity(tableName = "plants")
public class Plant implements Parcelable {

    @Expose
    @PrimaryKey
    @NonNull
    @SerializedName("p_id")
    @ColumnInfo(name = "p_id")
    private String id;

    @Expose
    @Nullable
    @SerializedName("p_img_url")
    @ColumnInfo(name = "p_img_url")
    private String imgUrl;

    @Expose
    @Nullable
    @SerializedName("p_botanical_name")
    @ColumnInfo(name = "p_botanical_name")
    private String botanicalName;

    @Expose
    @Nullable
    @SerializedName("p_common_name")
    @ColumnInfo(name = "p_common_name")
    private String commonName;

    @Expose
    @Nullable
    @SerializedName("p_plant_type")
    @ColumnInfo(name = "p_type")
    private String type;

    @Expose
    @Nullable
    @SerializedName("p_evergreen_or_deciduous")
    @ColumnInfo(name = "p_evergreen_or_deciduous")
    private String evergreenOrDeciduous;

    @Expose
    @Nullable
    @SerializedName("p_height")
    @ColumnInfo(name = "p_height")
    private String height;

    @Expose
    @Nullable
    @SerializedName("p_width")
    @ColumnInfo(name = "p_width")
    private String width;

    @Expose
    @Nullable
    @SerializedName("p_flower_colors")
    @ColumnInfo(name = "p_flower_colors")
    private String flowerColors;

    @Expose
    @Nullable
    @SerializedName("p_foliage_colors")
    @ColumnInfo(name = "p_foliage_colors")
    private String foliageColors;

    @Expose
    @Nullable
    @SerializedName("p_features")
    @ColumnInfo(name = "p_features")
    private String features;

    @Expose
    @Nullable
    @SerializedName("p_description")
    @ColumnInfo(name = "p_description")
    private String description;

    @Expose
    @Nullable
    @SerializedName("p_sun_exposure")
    @ColumnInfo(name = "p_sun_exposure")
    private String sunExposure;

    @Expose
    @Nullable
    @SerializedName("p_shade_type")
    @ColumnInfo(name = "p_shade_type")
    private String shadeType;

    @Expose
    @Nullable
    @SerializedName("p_hardiness")
    @ColumnInfo(name = "p_hardiness")
    private String hardiness;

    @Expose
    @Nullable
    @SerializedName("p_maintenance")
    @ColumnInfo(name = "p_maintenance")
    private String maintenance;

    @Expose
    @Nullable
    @SerializedName("p_soil_type")
    @ColumnInfo(name = "p_soil_type")
    private String soilType;

    @Expose
    @Nullable
    @SerializedName("p_attracts")
    @ColumnInfo(name = "p_attracts")
    private String attracts;

    @Expose
    @Nullable
    @SerializedName("p_toxicity")
    @ColumnInfo(name = "p_toxicity")
    private String toxicity;

    @Expose
    @Nullable
    @SerializedName("p_matches_plants")
    @ColumnInfo(name = "p_matches_plants")
    private String matchesPlants;

    @Expose
    @Nullable
    @SerializedName("p_effects")
    @ColumnInfo(name = "p_effects")
    private String effects;

    @Expose
    @Nullable
    @SerializedName("p_wildlife_features")
    @ColumnInfo(name = "p_wildlife_features")
    private String wildlifeFeatures;

    @Expose
    @Nullable
    @SerializedName("p_collect_seed_time")
    @ColumnInfo(name = "p_collect_seed_time")
    private String collectSeedTime;

    @Expose
    @Nullable
    @SerializedName("p_sow_time")
    @ColumnInfo(name = "p_sow_time")
    private String sowTime;

    @Expose
    @Nullable
    @SerializedName("p_plant_time")
    @ColumnInfo(name = "p_plant_time")
    private String plantTime;

    @Expose
    @Nullable
    @SerializedName("p_prune_time")
    @ColumnInfo(name = "p_prune_time")
    private String pruneTime;

    @Expose
    @Nullable
    @SerializedName("p_flower_time")
    @ColumnInfo(name = "p_flower_time")
    private String flowerTime;


    @Ignore
    public Plant(@Nullable String commonName) {
        this.id = UUID.randomUUID().toString();
        this.commonName = commonName;
    }

    @Ignore
    public Plant(@NonNull String id, @Nullable String commonName) {
        this.id = id;
        this.commonName = commonName;
    }

    public Plant(@NonNull String id, String imgUrl, String botanicalName, String commonName, String type, String evergreenOrDeciduous,
                 String height, String width, String flowerColors, String foliageColors, String features, String description,
                 String sunExposure, String shadeType, String hardiness, String maintenance,
                 String soilType, String attracts, String toxicity, String matchesPlants, String effects, @Nullable String wildlifeFeatures,
                 @Nullable String collectSeedTime, String sowTime, String plantTime, String pruneTime, String flowerTime) {
        this.id = id;
        this.imgUrl = imgUrl;
        this.botanicalName = botanicalName;
        this.commonName = commonName;
        this.type = type;
        this.evergreenOrDeciduous = evergreenOrDeciduous;
        this.height = height;
        this.width = width;
        this.flowerColors = flowerColors;
        this.foliageColors = foliageColors;
        this.features = features;
        this.description = description;
        this.sunExposure = sunExposure;
        this.shadeType = shadeType;
        this.hardiness = hardiness;
        this.maintenance = maintenance;
        this.soilType = soilType;
        this.attracts = attracts;
        this.toxicity = toxicity;
        this.matchesPlants = matchesPlants;
        this.effects = effects;
        this.wildlifeFeatures = wildlifeFeatures;
        this.collectSeedTime = collectSeedTime;
        this.sowTime = sowTime;
        this.plantTime = plantTime;
        this.pruneTime = pruneTime;
        this.flowerTime = flowerTime;
    }

    private String[] csvToArray(String csv) {
        if (TextUtils.isEmpty(csv)) {
            return new String[0];
        } else if (!csv.contains(",")) {
            return new String[]{csv};
        } else {
            return csv.split("\\s*,\\s*");
        }
    }

    private String parseMeasurement(String measurement) {
        String stripped = measurement.replace("cm", "");
        double measure = Double.parseDouble(stripped);
        if (measure >= 1000d) {
            return String.valueOf(measure / 1000d) + "m";
        } else {
            return measurement;
        }
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getBotanicalName() {
        return botanicalName;
    }

    public String getCommonName() {
        if (TextUtils.isEmpty(commonName)) {
            if (!TextUtils.isEmpty(getBotanicalName())) {
                return getBotanicalName().replaceAll("(\'.*?\')", "").trim();
            }
        } else {
            return commonName;
        }
        return "";
    }

    public String getType() {
        return type;
    }


    public String getParsedHeight() {
        return parseMeasurement(height);
    }

    public String getParsedWidth() {
        return parseMeasurement(width);
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String[] getFlowerColorsArray() {
        return csvToArray(getFlowerColors());
    }

    public String[] getFoliageColorsArray() {
        return csvToArray(getFoliageColors());
    }

    public String getFeatures() {
        return features;
    }

    public String getDescription() {
        return description;
    }

    public String getSunExposure() {
        return sunExposure;
    }

    public String getShadeType() {
        return shadeType;
    }

    public String getHardiness() {
        return hardiness;
    }

    public String getMaintenance() {
        return maintenance;
    }

    public String getSoilType() {
        return soilType;
    }

    public String[] getAttractsArray() {
        return csvToArray(getAttracts());
    }

    public String[] getToxicityArray() {
        return csvToArray(getToxicity());
    }

    public String[] getMatchesPlantsArray() {
        return csvToArray(getMatchesPlants());
    }

    public String getEffects() {
        return effects;
    }

    public String getWildlifeFeatures() {
        return wildlifeFeatures;
    }

    public String[] getCollectSeedTimeArray() {
        return csvToArray(getCollectSeedTime());
    }

    public String[] getSowTimeArray() {
        return csvToArray(getSowTime());
    }

    public String[] getPlantTimeArray() {
        return csvToArray(getPlantTime());
    }

    public String[] getPruneTimeArray() {
        return csvToArray(getPruneTime());
    }

    public String[] getFlowerTimeArray() {
        return csvToArray(getFlowerTime());
    }

    @Override
    public String toString() {
        return "Plant{" +
                "id='" + getId() + '\'' +
                ", imgUrl='" + getImgUrl() + '\'' +
                ", botanicalName='" + getBotanicalName() + '\'' +
                ", commonName='" + getCommonName() + '\'' +
                ", type='" + getType() + '\'' +
                ", evergreenOrDeciduous='" + getEvergreenOrDeciduous() + '\'' +
                ", height='" + getHeight() + '\'' +
                ", width='" + getWidth() + '\'' +
                ", flowerColors=" + getFlowerColors() +
                ", foliageColors=" + getFoliageColors() +
                ", features='" + getFeatures() + '\'' +
                //", description='" + getDescription() + '\'' +
                ", sunExposure='" + getSunExposure() + '\'' +
                ", shadeType='" + getShadeType() + '\'' +
                ", hardiness='" + getHardiness() + '\'' +
                ", maintenance='" + getMaintenance() + '\'' +
                ", soilType='" + getSoilType() + '\'' +
                ", attracts=" + getAttracts() +
                ", toxicity=" + getToxicity() +
                ", matchesPlants=" + getMatchesPlants() +
                ", effects='" + getEffects() + '\'' +
                ", wildlifeFeatures='" + getWildlifeFeatures() + '\'' +
                ", collectSeedTime=" + getCollectSeedTime() +
                ", sowTime=" + getSowTime() +
                ", plantTime=" + getPlantTime() +
                ", pruneTime=" + getPruneTime() +
                ", flowerTime=" + getFlowerTime() +
                '}';
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public void setImgUrl(@Nullable String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setBotanicalName(@Nullable String botanicalName) {
        this.botanicalName = botanicalName;
    }

    public void setCommonName(@Nullable String commonName) {
        this.commonName = commonName;
    }

    public void setType(@Nullable String type) {
        this.type = type;
    }

    public void setEvergreenOrDeciduous(@Nullable String evergreenOrDeciduous) {
        this.evergreenOrDeciduous = evergreenOrDeciduous;
    }

    public void setHeight(@Nullable String height) {
        this.height = height;
    }

    public void setWidth(@Nullable String width) {
        this.width = width;
    }

    public void setFlowerColors(@Nullable String flowerColors) {
        this.flowerColors = flowerColors;
    }

    public void setFoliageColors(@Nullable String foliageColors) {
        this.foliageColors = foliageColors;
    }

    public void setFeatures(@Nullable String features) {
        this.features = features;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public void setSunExposure(@Nullable String sunExposure) {
        this.sunExposure = sunExposure;
    }

    public void setShadeType(@Nullable String shadeType) {
        this.shadeType = shadeType;
    }

    public void setHardiness(@Nullable String hardiness) {
        this.hardiness = hardiness;
    }

    public void setMaintenance(@Nullable String maintenance) {
        this.maintenance = maintenance;
    }

    public void setSoilType(@Nullable String soilType) {
        this.soilType = soilType;
    }

    public void setAttracts(@Nullable String attracts) {
        this.attracts = attracts;
    }

    public void setToxicity(@Nullable String toxicity) {
        this.toxicity = toxicity;
    }

    public void setMatchesPlants(@Nullable String matchesPlants) {
        this.matchesPlants = matchesPlants;
    }

    public void setEffects(@Nullable String effects) {
        this.effects = effects;
    }

    public void setWildlifeFeatures(@Nullable String wildlifeFeatures) {
        this.wildlifeFeatures = wildlifeFeatures;
    }

    public void setCollectSeedTime(@Nullable String collectSeedTime) {
        this.collectSeedTime = collectSeedTime;
    }

    public void setSowTime(@Nullable String sowTime) {
        this.sowTime = sowTime;
    }

    public void setPlantTime(@Nullable String plantTime) {
        this.plantTime = plantTime;
    }

    public void setPruneTime(@Nullable String pruneTime) {
        this.pruneTime = pruneTime;
    }

    public void setFlowerTime(@Nullable String flowerTime) {
        this.flowerTime = flowerTime;
    }

    @Nullable
    public String getAttracts() {
        return attracts;
    }

    @Nullable
    public String getToxicity() {
        return toxicity;
    }

    @Nullable
    public String getMatchesPlants() {
        return matchesPlants;
    }

    @Nullable
    public String getCollectSeedTime() {
        return collectSeedTime;
    }

    @Nullable
    public String getSowTime() {
        return sowTime;
    }

    @Nullable
    public String getPlantTime() {
        return plantTime;
    }

    @Nullable
    public String getPruneTime() {
        return pruneTime;
    }

    @Nullable
    public String getFlowerTime() {
        return flowerTime;
    }

    @Nullable
    public String getEvergreenOrDeciduous() {
        return evergreenOrDeciduous;
    }

    @Nullable
    public String getFlowerColors() {
        return flowerColors;
    }

    @Nullable
    public String getFoliageColors() {
        return foliageColors;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.imgUrl);
        dest.writeString(this.botanicalName);
        dest.writeString(this.commonName);
        dest.writeString(this.type);
        dest.writeString(this.evergreenOrDeciduous);
        dest.writeString(this.height);
        dest.writeString(this.width);
        dest.writeString(this.flowerColors);
        dest.writeString(this.foliageColors);
        dest.writeString(this.features);
        dest.writeString(this.description);
        dest.writeString(this.sunExposure);
        dest.writeString(this.shadeType);
        dest.writeString(this.hardiness);
        dest.writeString(this.maintenance);
        dest.writeString(this.soilType);
        dest.writeString(this.attracts);
        dest.writeString(this.toxicity);
        dest.writeString(this.matchesPlants);
        dest.writeString(this.effects);
        dest.writeString(this.wildlifeFeatures);
        dest.writeString(this.collectSeedTime);
        dest.writeString(this.sowTime);
        dest.writeString(this.plantTime);
        dest.writeString(this.pruneTime);
        dest.writeString(this.flowerTime);
    }

    protected Plant(Parcel in) {
        this.id = in.readString();
        this.imgUrl = in.readString();
        this.botanicalName = in.readString();
        this.commonName = in.readString();
        this.type = in.readString();
        this.evergreenOrDeciduous = in.readString();
        this.height = in.readString();
        this.width = in.readString();
        this.flowerColors = in.readString();
        this.foliageColors = in.readString();
        this.features = in.readString();
        this.description = in.readString();
        this.sunExposure = in.readString();
        this.shadeType = in.readString();
        this.hardiness = in.readString();
        this.maintenance = in.readString();
        this.soilType = in.readString();
        this.attracts = in.readString();
        this.toxicity = in.readString();
        this.matchesPlants = in.readString();
        this.effects = in.readString();
        this.wildlifeFeatures = in.readString();
        this.collectSeedTime = in.readString();
        this.sowTime = in.readString();
        this.plantTime = in.readString();
        this.pruneTime = in.readString();
        this.flowerTime = in.readString();
    }

    public static final Parcelable.Creator<Plant> CREATOR = new Parcelable.Creator<Plant>() {
        @Override
        public Plant createFromParcel(Parcel source) {
            return new Plant(source);
        }

        @Override
        public Plant[] newArray(int size) {
            return new Plant[size];
        }
    };
}
