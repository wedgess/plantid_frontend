package eu.wedgess.plantid.data.source.local;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.IOException;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.source.ClassificationDataSource;
import eu.wedgess.plantid.data.source.repositories.ClassificationRepository;
import eu.wedgess.plantid.features.classification.TensorflowImageClassifier;
import io.reactivex.Observable;
import io.reactivex.Single;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Local classification data source used by {@link ClassificationRepository
 * }
 * Created by gar on 11/11/17.
 */

public class ClassificationLocalDataSource implements ClassificationDataSource {

    private static final String TAG = ClassificationLocalDataSource.class.getSimpleName();

    private final AssetManager mAssetManager;

    public ClassificationLocalDataSource(@NonNull AssetManager assManager) {
        this.mAssetManager = checkNotNull(assManager);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Single getClassificationResults(@NonNull String location, @Nullable String userId, @NonNull File image) {
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
        bitmap = Bitmap.createScaledBitmap(bitmap, TensorflowImageClassifier.DIM_IMG_SIZE_X, TensorflowImageClassifier.DIM_IMG_SIZE_Y, false);
        TensorflowImageClassifier classifier = null;
        try {
            classifier = new TensorflowImageClassifier(mAssetManager);
            return Single.just(classifier.classifyFrame(bitmap));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bitmap.recycle();
            if (classifier != null) {
                classifier.close();
            }
        }

        return Single.just(Observable.empty());
    }

    @Override
    public Single<History> uploadIdentificationImage(@NonNull String location, @NonNull String uid, @NonNull File image) {
        // IGNORE - nowhere to uplad locally
        throw new RuntimeException("Images cannot be uploaded locally...");
    }
}
