/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.requests;

import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.network.api.responses.ThreadResponse;
import eu.wedgess.plantid.network.api.responses.ThreadsListResponse;
import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Retrofit requests to API endpoints for threads. {@link Thread}
 * used by: {@link eu.wedgess.plantid.data.source.remote.ThreadsRemoteDataSource}
 * <p>
 * Created by gar on 02/01/18.
 */

public interface ThreadRequests {

    @Headers("Content-Type: application/json")
    @POST("/threads")
    Completable createThread(@Body Thread thread);

    @Headers("Content-Type: application/json")
    @PUT("/threads/{tid}")
    Completable updateThread(@Path("tid") String tid, @Body Thread thread);

    @GET("/threads")
    Single<ThreadsListResponse> getAllThreads();

    @GET("/threads/{tid}")
    Single<ThreadResponse> getThreadById(@Path("tid") String tid);

    @DELETE("/threads/{tid}")
    Completable deleteThread(@Path("tid") String tid);

    @GET("/users/{auth_id}/threads")
    Single<ThreadsListResponse> getThreadsByAuthorId(@Path("auth_id") String authorId);

}
