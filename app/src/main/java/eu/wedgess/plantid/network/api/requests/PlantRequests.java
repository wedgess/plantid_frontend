/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.requests;

import eu.wedgess.plantid.network.api.responses.PlantResponse;
import eu.wedgess.plantid.network.api.responses.PlantsListResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Retrofit class ot call the servers API endpoints for history items. {@link eu.wedgess.plantid.data.Plant}
 * Used by: {@link eu.wedgess.plantid.data.source.remote.PlantsRemoteDataSource}
 * <p>
 * Created by gar on 12/12/17.
 */

public interface PlantRequests {

    @GET("/plants")
    Single<PlantsListResponse> getPlantsData();

    @GET("/plants/{pid}")
    Single<PlantResponse> getPlantById(@Path("pid") String pid);
}
