/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import eu.wedgess.plantid.data.Comment;

/**
 * A class for getting a list of comments from server response. {@List<Comment>}
 * <p>
 * {@link SerializedName} is used by GSON to parse the data
 * <p>
 * Created by gar on 02/12/17.
 */
public class CommentsResponse extends BaseServerResponse {

    @Expose
    @SerializedName(KEY_DATA)
    private final ArrayList<Comment> comments;

    public CommentsResponse(boolean error, String message, ArrayList<Comment> comments) {
        super(error, message);
        this.comments = comments;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }
}
