/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Singleton for getting sRetrofit instance, and setting up its cache and logging.
 * <p>
 * Created by gar on 12/12/17.
 */

public class RetrofitInstance {

    private static volatile Retrofit sRetrofit;
    private static final String BASE_URL = "http://garethwilliams.host:5000/PlantID_backend/";
//    private static final String BASE_URL = "http://192.168.1.23:5000/";

    public static Retrofit getRetrofitInstance() {
        if (sRetrofit == null) {

            synchronized (RetrofitInstance.class) {
                if (sRetrofit == null) {
                    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                    // add logging as last interceptor
//            httpClient.addInterceptor(getLogginInterceptor());
                    httpClient.cache(getHttpCache());

                    sRetrofit = new retrofit2.Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create(getGson()))
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .client(httpClient.build())
                            .build();
                }
            }
        }
        return sRetrofit;
    }

    // prevent direct instantiation
    private RetrofitInstance() {
    }

    private static HttpLoggingInterceptor getLogginInterceptor() {
        // can add other interceptors such as HEADERS etc...
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    }

    private static Cache getHttpCache() {
        // TODO: can't get context here so hardcode for now
        File cacheFile = new File("/data/data/eu.wedgess.plantid/cache", "okhttp.cache");
        cacheFile.mkdirs();
        return new Cache(cacheFile, 6 * 1024 * 1024); // 6MB
    }

    private static Gson getGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }
}
