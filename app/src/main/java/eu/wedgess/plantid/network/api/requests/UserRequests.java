/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.requests;

import java.util.HashMap;

import eu.wedgess.plantid.network.api.responses.ThreadsListResponse;
import eu.wedgess.plantid.network.api.responses.UserResponse;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Retrofit class ot call the servers API endpoints for user items. {@link eu.wedgess.plantid.data.User}
 * Used by {@link eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource}
 * <p>
 * Created by gar on 12/12/17.
 */

public interface UserRequests {

    @Multipart
    @POST("/users/{uid}/image")
    Single<UserResponse> uploadProfileImage(@Path("uid") String userId, @Part MultipartBody.Part image);

    @Multipart
    @POST("/users/register")
    Single<UserResponse> registerUser(@Part MultipartBody.Part image, @Part("json") RequestBody user);

    @Multipart
    @PUT("/users/{uid}")
    Single<UserResponse> updateUser(@Path("uid") String uid, @Part MultipartBody.Part image, @Part("json") RequestBody user);

    @GET("/users/{uid}")
    Maybe<UserResponse> getUserById(@Path("uid") String userId);

    @Headers("Content-Type: application/json")
    @POST("/users/login")
    Single<UserResponse> performLogin(@Body HashMap<String, String> body);

    @GET("/users/{uid}/threads")
    Call<ThreadsListResponse> getUsersThreads(@Path("uid") String userId);

    @DELETE("/users/{uid}")
    Completable deleteUserProfile(@Path("uid") String uid);
}
