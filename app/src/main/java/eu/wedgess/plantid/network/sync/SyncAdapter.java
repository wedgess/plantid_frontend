package eu.wedgess.plantid.network.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.local.CommentsLocalDataSource;
import eu.wedgess.plantid.data.source.local.HistoryLocalDataSource;
import eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource;
import eu.wedgess.plantid.data.source.local.dao.ThreadDao;
import eu.wedgess.plantid.data.source.remote.CommentsRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.HistoryRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.ThreadsRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.utils.PlantPreferenceManager;
import io.reactivex.schedulers.Schedulers;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 * <p>
 * Created by gar: 11/02/2018
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = SyncAdapter.class.getSimpleName();

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        Log.i(TAG, "In sync adapters onPerformSync()");

        AppDatabase database = AppDatabase.getInstance(getContext());

        ThreadsLocalDataSource threadsLocalDataSource = new ThreadsLocalDataSource(database.threadDao());
        ThreadsRemoteDataSource threadsRemoteDataSource = new ThreadsRemoteDataSource();

        //region Thread syncing
        ThreadsRepository threadsRepository = ThreadsRepository.getInstance(threadsLocalDataSource, threadsRemoteDataSource);

        threadsRepository.getDirtyThreads()
                .subscribeOn(Schedulers.io())
                .subscribe(dirtyThreads -> {
                    Log.i(TAG, "Dirty threads size: " + dirtyThreads.size());
                    if (!dirtyThreads.isEmpty()) {
                        for (Thread thread : dirtyThreads) {
                            setDirtyThreadAsClean(thread, threadsRepository);
                        }
                    }
                    syncThreadsRemoteWithLocal(threadsRemoteDataSource, threadsLocalDataSource, database.threadDao());
                });
        //endregion

        //region Comment Syncing
        CommentsRepository commentsRepository = CommentsRepository.getInstance(
                new CommentsLocalDataSource(database.commentDao()), new CommentsRemoteDataSource());

        commentsRepository.getDirtyComments()
                .subscribeOn(Schedulers.io())
                .subscribe(dirtyComments -> {
                    if (!dirtyComments.isEmpty()) {
                        for (Comment comment : dirtyComments) {
                            setDirtyCommentAsClean(comment, commentsRepository);
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "Error getting dirty items");
                });
        //endregion

        //region History Syncing
        HistoryRemoteDataSource historyRemoteDataSource = new HistoryRemoteDataSource();
        HistoryLocalDataSource historyLocalDataSource = new HistoryLocalDataSource(database.historyDao());

        HistoryRepository historyRepository = HistoryRepository.getInstance(
                historyLocalDataSource,
                historyRemoteDataSource);

        historyRepository.getDirtyHistory()
                .subscribeOn(Schedulers.io())
                .subscribe(dirtyHistory -> {
                    Log.i(TAG, "Dirty history size: " + dirtyHistory.size());
                    if (!dirtyHistory.isEmpty()) {
                        for (History history : dirtyHistory) {
                            setDirtyHistoryAsClean(history, historyRepository);
                        }
                    }
                    syncHistoryRemoteWithLocal(historyRemoteDataSource, historyLocalDataSource, historyRepository);
                });
        //endregion

    }

    private void setDirtyHistoryAsClean(History history, HistoryRepository historyRepository) {
        history.setDirty(false);
        historyRepository.updateDirtyHistory(history)
                .doOnComplete(() -> {
                    Log.i(TAG, "Updated history items dirty state");
                })
                .doOnError(throwable -> {
                    Log.e(TAG, "Error updating history dirty state");
//                                    throw Exceptions.propagate(throwable);
                })
                .subscribe(() -> {
                    Log.i(TAG, "Updated dirty history item succesfully");
                }, throwable -> {
                    Log.e(TAG, "error updating history dirty state");
                });
    }

    private void setDirtyCommentAsClean(Comment comment, CommentsRepository commentsRepository) {
        comment.setDirty(false);
        commentsRepository.updateDirtyComment(comment)
                .doOnComplete(() -> Log.i(TAG, "Updated comment item dirty state"))
                .doOnError(throwable -> {
                    Log.e(TAG, "Error updating comments dirty state");
//                                    throw Exceptions.propagate(throwable);
                })
                .subscribe(() -> {
                    Log.i(TAG, "Comment updated succesfully");
                });
    }

    private void setDirtyThreadAsClean(Thread thread, ThreadsRepository threadsRepository) {
        thread.setDirty(false);
        threadsRepository.updateDirtyThread(thread)
                .doOnComplete(() -> {
                    Log.i(TAG, "Updated thread items dirty state");
                })
                .doOnError(throwable -> {
                    Log.e(TAG, "Error updating threads dirty state");
//                                    throw Exceptions.propagate(throwable);
                })
                .subscribe(() -> {
                    Log.i(TAG, "Updated dirty thread item succesfully");
                }, throwable -> {
                    Log.e(TAG, "error updating thread dirty state");
                });
    }

    private void syncThreadsRemoteWithLocal(ThreadsRemoteDataSource threadsRemoteDataSource,
                                            ThreadsLocalDataSource threadsLocalDataSource,
                                            ThreadDao threadDao) {
        threadsRemoteDataSource.getThreads()
                .delay(2, TimeUnit.SECONDS)
                .subscribe(remoteThreads -> {
                            if (!remoteThreads.isEmpty()) {
                                threadsLocalDataSource
                                        .getThreads()
                                        .subscribe(localThreads -> {
                                            if (localThreads.size() != remoteThreads.size()) {
                                                localThreads.removeAll(remoteThreads);
                                                for (Thread thread : localThreads) {
                                                    // TODO: Might need to delete from repository so it is removed from cache
                                                    threadDao.deleteThread(thread);
                                                }
                                            }
                                        });
                            }
                        },
                        throwable -> Log.e(TAG, "Error syncing  threads with remote"));
    }

    private void syncHistoryRemoteWithLocal(HistoryRemoteDataSource historyRemoteDataSource,
                                            HistoryLocalDataSource historyLocalDataSource,
                                            HistoryRepository repository) {
        User user = PlantPreferenceManager.getUser(PreferenceManager.getDefaultSharedPreferences(getContext()));
        if (user != null) {
            historyRemoteDataSource.getHistory(user.getUid())
                    .delay(2, TimeUnit.SECONDS)
                    .subscribe(remoteHistory -> {
                                Log.i(TAG, "History list from remote size: " + remoteHistory.size());
                                if (!remoteHistory.isEmpty()) {
                                    historyLocalDataSource
                                            .getHistory(user.getUid())
                                            .subscribe(localHistory -> {
                                                Log.i(TAG, "History list from local size: " + localHistory.size());
                                                if (localHistory.size() != remoteHistory.size()) {
                                                    localHistory.removeAll(remoteHistory);
                                                    Log.i(TAG, "Deleting history items from local: " + localHistory.size());
                                                    for (History history : localHistory) {
                                                        repository.deleteHistoryItem(history);
                                                    }
                                                }
                                            });
                                }
                            },
                            throwable -> Log.e(TAG, "Error syncing  history with remote"));
        }
    }
}
