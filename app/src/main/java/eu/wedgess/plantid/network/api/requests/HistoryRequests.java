/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.requests;

import java.util.List;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.network.api.responses.HistoryItemResponse;
import eu.wedgess.plantid.network.api.responses.HistoryItemsResponse;
import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Retrofit class ot call the servers API endpoints for history items. {@link History}
 * Used by {@link eu.wedgess.plantid.data.source.remote.HistoryRemoteDataSource} &
 * {@link eu.wedgess.plantid.data.source.remote.ClassificationRemoteDataSource}
 * <p>
 * Created by gar on 12/12/17.
 */

public interface HistoryRequests {

    @Multipart
    @POST("/users/{uid}/history")
    Single<HistoryItemResponse> createHistoryItem(@Path("uid") String userId, @Part MultipartBody.Part image, @Part("location") RequestBody user);

    @Headers("Content-Type: application/json")
    @PUT("/users/{uid}/history/{hid}")
    Completable updateHistoryItem(@Path("uid") String userId, @Path("hid") String historyItemId, @Body History historyItem);

    @GET("/users/{uid}/history")
    Single<HistoryItemsResponse> getUsersHistory(@Path("uid") String userId);

    @GET("/users/{uid}/history/{hid}")
    Single<HistoryItemResponse> getHistoryItemById(@Path("uid") String userId, @Path("hid") String historyItemId);

    @Headers("Content-Type: application/json")
    @HTTP(method = "DELETE", path = "/users/{uid}/history", hasBody = true)
    Completable deleteHistoryItems(@Path("uid") String userId, @Body List<History> historyItems);
}
