/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.custom.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import eu.wedgess.plantid.R;

/**
 * Wrap the AlertDialog inside a {@link DialogFragment} to retain dialog on orientation change.
 * Activities who create this dialog must implement the below {@link HelpDialogCompleteListener}.
 * before showing the dialog.
 * This dialog is used to display an option to view the {@link eu.wedgess.plantid.features.help.HelpActivity}
 * <p>
 * Created by gar on 23/03/18.
 */

public class HelpAckDialog extends DialogFragment {

    // interface to listen to confirm button click
    public interface HelpDialogCompleteListener {
        void onHelpDialogShowHelpClicked();
    }

    public void setListener(HelpDialogCompleteListener mListener) {
        this.mListener = mListener;
    }

    private HelpDialogCompleteListener mListener;

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof HelpDialogCompleteListener) {
            mListener = (HelpDialogCompleteListener) activity;
        } else if (mListener == null) {
            throw new RuntimeException("Activity must implement HelpDialogCompleteListener!");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_title_getting_started)
                .setMessage(R.string.alert_msg_getting_started)
                .setPositiveButton(R.string.alert_btn_yes, (dialogInterface, i) -> mListener.onHelpDialogShowHelpClicked())
                .setNegativeButton(R.string.alert_btn_no, (dialogInterface, i) -> dialogInterface.cancel())
                .create();
    }
}
