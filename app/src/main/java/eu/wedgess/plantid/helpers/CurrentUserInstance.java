/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.helpers;

import eu.wedgess.plantid.data.User;

/**
 * Singleton to hold current app user. This saves us from having  to go to the UserRepository each time.
 * When user is signed in or app is loaded when user is signed in, the current user will be set here.
 * If user is signed out @{eu.wedgess/plantid.helpers.CurrentUserInstance#mCurrentUser} will be NULL.
 * Created by gar on 10/11/17.
 */
public class CurrentUserInstance {

    private static final String TAG = CurrentUserInstance.class.getSimpleName();

    private User mCurrentUser;

    private static volatile CurrentUserInstance sSoleInstance;

    public static CurrentUserInstance getInstance() {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (CurrentUserInstance.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new CurrentUserInstance();
            }
        }

        return sSoleInstance;
    }

    private CurrentUserInstance() {
    }

    public User getCurrentUser() {
        return mCurrentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.mCurrentUser = currentUser;
    }
}
