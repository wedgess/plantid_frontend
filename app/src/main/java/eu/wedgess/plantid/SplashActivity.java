/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import eu.wedgess.plantid.features.intro.IntroActivity;
import eu.wedgess.plantid.features.mainnavigation.MainNavigationActivity;
import eu.wedgess.plantid.utils.PlantPreferenceManager;

/**
 * An activity which display the applications splash screen, which this activity is showing we load
 * the user object from shared preferences (if it exists) and set the models user. This is to avoid
 * any white screen when the application first opens.
 * The activities BG is set through a theme within {@link /res/values/styles.xml}
 * <p>
 * Created by Gareth on 09/02/2018.
 */

public class SplashActivity extends AppCompatActivity {

    private final String TAG = SplashActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // if first start show user the intro activites, otherwise start man nav activity
        if (PlantPreferenceManager.showUserIntro(getApplicationContext())) {
            startActivity(new Intent(this, IntroActivity.class));
        } else {
            startActivity(new Intent(this, MainNavigationActivity.class));
        }
        finish();
    }
}
