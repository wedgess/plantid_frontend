/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plantdetails;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.source.local.PlantsLocalDataSource;
import eu.wedgess.plantid.data.source.remote.PlantsRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import eu.wedgess.plantid.helpers.GlideImageHelper;

/**
 * Used to display a specific plants details.
 * Called by: {@link eu.wedgess.plantid.features.plants.PlantsFragment} and
 * {@link eu.wedgess.plantid.features.history.HistoryActivity} when items are -
 * {@link eu.wedgess.plantid.features.history.HistoryActivity#TYPE_MATCHED}
 * and also by: {@link eu.wedgess.plantid.features.classification.ClassificationActivity}
 */
public class PlantDetailActivity extends BaseActivity implements PlantDetailContract.View {

    public static final String EXTRA_PLANT_ID = "EXTRA_PLANT_ID";

    //region View binding
    @BindView(R.id.plant_header_iv)
    ImageView mPlantHeaderIV;
    @BindView(R.id.tv_plant_common_name)
    TextView mPlantCommonNameTV;
    @BindView(R.id.tv_plant_latin_name)
    TextView mPlantBotanicalNameTV;
    @BindView(R.id.tv_plant_desc)
    TextView mPlantDescriptionTV;
    @BindView(R.id.tv_plant_height)
    TextView mPlantHeightTV;
    @BindView(R.id.tv_plant_width)
    TextView mPlantWidthTV;
    @BindView(R.id.plant_collapsing_tb)
    CollapsingToolbarLayout mCollapsingTBLayout;
    @BindView(R.id.grid_flower_time)
    GridView mFloweringTimeGV;
    @BindView(R.id.grid_flower_color)
    GridView mFlowerColorGV;
    @BindView(R.id.grid_foliage_color)
    GridView mFoliageColorGV;
    @BindView(R.id.grid_sow_time)
    GridView mSowTimeGV;
    @BindView(R.id.grid_plant_time)
    GridView mPlantTimeGV;
    @BindView(R.id.grid_prune_time)
    GridView mPruneTimeGV;
    @BindView(R.id.grid_collect_seed_time)
    GridView mCollectSeedTimeGV;
    @BindView(R.id.tv_plant_features)
    TextView mPlantFeaturesTV;
    @BindView(R.id.tv_plant_type)
    TextView mPlantTypeTV;
    @BindView(R.id.tv_plant_sunexposure)
    TextView mPlantSunExposureTV;
    @BindView(R.id.tv_plant_hardiness)
    TextView mPlantHardinessTV;
    @BindView(R.id.tv_plant_soiltype)
    TextView mPlantSoilTypeTV;
    @BindView(R.id.tv_plant_maintenance)
    TextView mPlantMaintenanceTV;
    @BindView(R.id.iv_attracts_bees)
    ImageView mAttractsBeesIV;
    @BindView(R.id.tv_attracts_bees_title)
    TextView mAttractsBeesTV;
    @BindView(R.id.iv_attracts_birds)
    ImageView mAttractsBirdsIV;
    @BindView(R.id.tv_attracts_birds_title)
    TextView mAttractsBirdsTV;
    @BindView(R.id.iv_attracts_insects)
    ImageView mAttractsInsectsIV;
    @BindView(R.id.tv_attracts_insects_title)
    TextView mAttractsInsectsTV;
    @BindView(R.id.iv_attracts_moths)
    ImageView mAttractsMothsIV;
    @BindView(R.id.tv_attracts_moths_title)
    TextView mAttractsMothsTV;
    @BindView(R.id.iv_attracts_other)
    ImageView mAttractsOtherIV;
    @BindView(R.id.tv_attracts_other_title)
    TextView mAttractsOtherTV;
    @BindView(R.id.attracts_layout)
    RelativeLayout mAttractsLayout;
    @BindView(R.id.toxicity_layout)
    RelativeLayout mToxicityLayout;
    @BindView(R.id.iv_toxicity_birds)
    ImageView mToxicityBirdsIV;
    @BindView(R.id.tv_toxicity_birds_title)
    TextView mToxicityBirdsTV;
    @BindView(R.id.iv_toxicity_cats)
    ImageView mToxicityCatsIV;
    @BindView(R.id.tv_toxicity_cats_title)
    TextView mToxicityCatsTV;
    @BindView(R.id.iv_toxicity_dogs)
    ImageView mToxicityDogsIV;
    @BindView(R.id.tv_toxicity_dogs_title)
    TextView mToxicityDogsTV;
    @BindView(R.id.iv_toxicity_horses)
    ImageView mToxicityHorsesIV;
    @BindView(R.id.tv_toxicity_horses_title)
    TextView mToxicityHorsesTV;
    @BindView(R.id.iv_toxicity_livestock)
    ImageView mToxicityLivestockIV;
    @BindView(R.id.tv_toxicity_livestock_title)
    TextView mToxicityLivestockTV;
    @BindView(R.id.iv_toxicity_people)
    ImageView mToxicityPeopleIV;
    @BindView(R.id.tv_toxicity_people_title)
    TextView mToxicityPeopleTV;
    @BindView(R.id.tv_plant_effects)
    TextView mEffectsValueTV;
    @BindView(R.id.tv_plant_wildlife_feat)
    TextView mWildlifeFeaturesValueTV;
    //endregion

    private PlantDetailContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_plant_details);
        ButterKnife.bind(this);
        setSupportActionBar(findViewById(R.id.toolbar_plant));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get the requested plant id
        String plantId = getIntent().getStringExtra(EXTRA_PLANT_ID);

        // init the presenter
        new PlantDetailPresenter(plantId, PlantsRepository.getInstance(
                new PlantsLocalDataSource(AppDatabase.getInstance(PlantDetailActivity.this).plantDao()),
                new PlantsRemoteDataSource()), this);

        setupAppbarOffsetChanges();
    }

    // offset listener on appbar for displaying toolbar title on collapse
    private void setupAppbarOffsetChanges() {
        AppBarLayout appBarLayout = findViewById(R.id.app_bar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset < 120) {
                    mCollapsingTBLayout.setTitle(mPlantCommonNameTV.getText().toString());
                    isShow = true;
                } else if (isShow) {
                    // should be space between quotes
                    mCollapsingTBLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull PlantDetailContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    @Override
    public void showMissingPlant() {

    }

    @Override
    public void setPlantHeaderImage(String url) {
        GlideImageHelper.glideLoadImage(mPlantHeaderIV, url.replace("localhost", "10.0.2.2"));
    }

    @Override
    public void setPlantNames(String botanicalName, String commonName) {
        mPlantBotanicalNameTV.setText(botanicalName);
        mPlantCommonNameTV.setText(commonName);
    }

    @Override
    public void setPlantDescription(String description) {
        mPlantDescriptionTV.setText(description);
    }

    @Override
    public String[] getMonthsList() {
        return getResources().getStringArray(R.array.months_abbrv);
    }

    @Override
    public void setFlowersGridView(LinkedHashMap<String, Boolean> flowerTimeMap) {
        mFloweringTimeGV.setAdapter(new CalendarMonthAdapter(flowerTimeMap, PlantDetailActivity.this));
    }

    @Override
    public void hideFlowersGridView() {
        final TextView titleTv = findViewById(R.id.tv_plant_flowering_title);
        final View divider = findViewById(R.id.flower_time_divider);
        titleTv.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        mFloweringTimeGV.setVisibility(View.GONE);
    }

    @Override
    public void setSowTimeGridView(LinkedHashMap<String, Boolean> sowTimeMap) {
        mSowTimeGV.setAdapter(new CalendarMonthAdapter(sowTimeMap, PlantDetailActivity.this));
    }

    @Override
    public void hideSowTimeGridView() {
        TextView titleTv = findViewById(R.id.tv_sow_time_title);
        View divider = findViewById(R.id.sow_time_divider);
        titleTv.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        mSowTimeGV.setVisibility(View.GONE);
    }

    @Override
    public void setPruneTimeGridView(LinkedHashMap<String, Boolean> pruneTimeMap) {
        mPruneTimeGV.setAdapter(new CalendarMonthAdapter(pruneTimeMap, PlantDetailActivity.this));
    }

    @Override
    public void hidePruneTimeGridView() {
        TextView titleTv = findViewById(R.id.tv_prune_time_title);
        View divider = findViewById(R.id.prune_time_divider);
        titleTv.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        mPruneTimeGV.setVisibility(View.GONE);
    }

    @Override
    public void setCollectSeedTimeGridView(LinkedHashMap<String, Boolean> collectSeedTimeMap) {
        mCollectSeedTimeGV.setAdapter(new CalendarMonthAdapter(collectSeedTimeMap, PlantDetailActivity.this));
    }

    @Override
    public void hideCollectSeedTimeGridView() {
        TextView titleTv = findViewById(R.id.tv_collect_seed_time_title);
        View divider = findViewById(R.id.collect_seed_time_divider);
        titleTv.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        mCollectSeedTimeGV.setVisibility(View.GONE);
    }

    // change colour of attracts icon when and text when not active
    private void setInactiveAttractsIV(ImageView iv, TextView tv) {
        if (tv != null) {
            tv.setTextColor(ContextCompat.getColor(this, R.color.dividers));
        }
        if (iv != null) {
            iv.setColorFilter(ContextCompat.getColor(this, R.color.dividers), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void setAttractsBirdsActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mAttractsBirdsIV, mAttractsBirdsTV);
        }
    }

    @Override
    public void setAttractsBeesActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mAttractsBeesIV, mAttractsBeesTV);
        }
    }

    @Override
    public void setAttractsOtherPollinatorsActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mAttractsOtherIV, mAttractsOtherTV);
        }
    }

    @Override
    public void setAttractsMothsButterfliesActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mAttractsMothsIV, mAttractsMothsTV);
        }
    }

    @Override
    public void setAttractsInsectsActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mAttractsInsectsIV, mAttractsInsectsTV);
        }
    }

    @Override
    public void hideAttractsLayout() {
        mAttractsLayout.setVisibility(View.GONE);
    }

    @Override
    public void setWildLifeFeatures(@NonNull String wildLifeFeatures) {
        mWildlifeFeaturesValueTV.setText(wildLifeFeatures);
    }

    @Override
    public void setToxicityPersonActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mToxicityPeopleIV, mToxicityPeopleTV);
        }
    }

    @Override
    public void setToxicityCatActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mToxicityCatsIV, mToxicityCatsTV);
        }
    }

    @Override
    public void setToxicityDogActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mToxicityDogsIV, mToxicityDogsTV);
        }
    }

    @Override
    public void setToxicityHorseActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mToxicityHorsesIV, mToxicityHorsesTV);
        }
    }

    @Override
    public void setToxicityBirdsActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mToxicityBirdsIV, mToxicityBirdsTV);
        }
    }

    @Override
    public void setToxicityLivestockActive(boolean active) {
        if (!active) {
            setInactiveAttractsIV(mToxicityLivestockIV, mToxicityLivestockTV);
        }
    }

    @Override
    public void hideToxicityLayout() {
        mToxicityLayout.setVisibility(View.GONE);
    }

    @Override
    public void setEffects(String effects) {
        mEffectsValueTV.setText(effects);
    }

    @Override
    public void setPlantTimeGridView(LinkedHashMap<String, Boolean> plantTimeMap) {
        mPlantTimeGV.setAdapter(new CalendarMonthAdapter(plantTimeMap, PlantDetailActivity.this));
    }

    @Override
    public void hidePlantTimeGridView() {
        setViewPairAsGone(findViewById(R.id.tv_plant_time_title),
                findViewById(R.id.plant_time_divider));
        mPlantTimeGV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantHeight(String height) {
        mPlantHeightTV.setText(height);
    }

    @Override
    public void hidePlantHeight() {
        final TextView titleTv = findViewById(R.id.tv_plant_height_title);
        titleTv.setVisibility(View.GONE);
        mPlantHeightTV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantWidth(String width) {
        mPlantWidthTV.setText(width);
    }

    @Override
    public void hidePlantWidth() {
        final TextView titleTv = findViewById(R.id.tv_plant_width_title);
        titleTv.setVisibility(View.GONE);
        mPlantWidthTV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantType(String type, @DrawableRes int drawable) {
        mPlantTypeTV.setText(type);
        mPlantTypeTV.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
    }

    @Override
    public void hidePlantType() {
        mPlantTypeTV.setVisibility(View.GONE);
    }

    @Override
    public void setFlowerColorGridView(String[] colors) {
        mFlowerColorGV.setNumColumns(colors.length);
        mFlowerColorGV.setAdapter(new PlantColorsAdapter(colors, PlantDetailActivity.this));
    }

    @Override
    public void hideFlowerColorGridView() {
        setViewPairAsGone(findViewById(R.id.tv_flower_color_title),
                findViewById(R.id.flower_color_divider));
        mFlowerColorGV.setVisibility(View.GONE);
    }

    @Override
    public void setFoliageColorGridView(String[] colors) {
        mFoliageColorGV.setNumColumns(colors.length);
        mFoliageColorGV.setAdapter(new PlantColorsAdapter(colors, PlantDetailActivity.this));
    }

    @Override
    public void hideFoliageColorGridView() {
        final TextView titleTv = findViewById(R.id.tv_foliage_color_title);
        titleTv.setVisibility(View.GONE);
        mFoliageColorGV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantFeatures(String features) {
        mPlantFeaturesTV.setText(features);
    }

    @Override
    public void hidePlantFeatures() {
        setViewPairAsGone(findViewById(R.id.tv_plant_features_title),
                findViewById(R.id.features_title_divider));
        mPlantFeaturesTV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantSunExposure(String sunExposure) {
        mPlantSunExposureTV.setText(sunExposure);
    }

    @Override
    public void hidePlantSunExposure() {
        setViewPairAsGone(findViewById(R.id.tv_plant_sunexposure_title),
                findViewById(R.id.sunexposure_title_divider));
        mPlantSunExposureTV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantHardiness(String hardiness) {
        mPlantHardinessTV.setText(hardiness);
    }

    @Override
    public void hidePlantHardiness() {
        setViewPairAsGone(findViewById(R.id.tv_plant_hardiness_title),
                findViewById(R.id.hardiness_title_divider));
        mPlantHardinessTV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantSoilType(String soiltype) {
        mPlantSoilTypeTV.setText(soiltype);
    }

    @Override
    public void hidePlantSoilType() {
        setViewPairAsGone(findViewById(R.id.tv_plant_soiltype_title),
                findViewById(R.id.soiltype_title_divider));
        mPlantSoilTypeTV.setVisibility(View.GONE);
    }

    @Override
    public void setPlantMaintenance(String maintenance) {
        mPlantMaintenanceTV.setText(maintenance);
    }

    @Override
    public void hidePlantMaintenance() {
        setViewPairAsGone(findViewById(R.id.tv_plant_maintenance_title),
                findViewById(R.id.maintenance_title_divider));
        mPlantMaintenanceTV.setVisibility(View.GONE);
    }

    // used to set title and divider visibility as GONE
    private void setViewPairAsGone(TextView titleTv, View divider) {
        titleTv.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
    }
}
