/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.createeditthread;

import android.support.annotation.NonNull;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;

/**
 * Contract between {@link CreateEditThreadActivity} and {@link CreateEditThreadPresenter}
 * <p>
 * Created by gar on 01/01/18.
 */

public class CreateEditThreadContract {

    interface View extends BaseView<CreateEditThreadPresenter> {
        // if not valid show error
        void showTitleErrorMessage();

        void showMessageErrorMessage();

        // activity will finish when called
        void threadCreationSuccess();

        void setThreadTitle(String title);

        void setThreadMessage(String message);

        void setThreadImage(String imgUrl);

    }

    interface Presenter extends BasePresenter {
        // submit thread for creation or update
        void submitThread(@NonNull String title, @NonNull String message, @NonNull final String imgUrl, @NonNull final String location, final boolean isOffline);

        // load just the profile image into its view
        void loadProfileImage(@NonNull String threadId);

        // check whether input fields are valid or not
        boolean validateThreadFields(@NonNull String title, @NonNull String message);
    }
}
