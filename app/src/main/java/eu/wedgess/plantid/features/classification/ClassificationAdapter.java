/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.classification;

import android.annotation.SuppressLint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.Classifier;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.helpers.GlideImageHelper;

/**
 * Displays the results from image classification in a list inside {@link ClassificationActivity}.
 * <p>
 * Created by gar on 31/11/17.
 */

public class ClassificationAdapter extends RecyclerView.Adapter<ClassificationAdapter.ViewHolder> {

    public interface RecyclerClickListener {
        void onRecyclerItemClick(int position);

        void onMatchButtonClicked(int position);
    }

    public static final String TAG = ClassificationAdapter.class.getSimpleName();
    private boolean mShowMatchButtons = true;

    void toggleMatchButtonVisibility(boolean visible) {
        mShowMatchButtons = visible;
        notifyDataSetChanged();
    }

    private List<Classifier.Recognition> mClassificationResults;
    private RecyclerClickListener mRecyclerClickListener;

    ClassificationAdapter(boolean showMatchButtons, List<Classifier.Recognition> plants, RecyclerClickListener recyclerClickListener) {
        mShowMatchButtons = showMatchButtons;
        this.mClassificationResults = plants;
        this.mRecyclerClickListener = recyclerClickListener;
    }

    // Create new views (invoked by the layout manager)
    @SuppressLint("InflateParams")
    @Override
    public ClassificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_classification_match, null), mRecyclerClickListener);
    }

    // Return the size of your mClassificationResults (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mClassificationResults.size();
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Classifier.Recognition classificationResult = mClassificationResults.get(position);
        viewHolder.setData(classificationResult, mShowMatchButtons);
    }

    public Classifier.Recognition getItemAtPos(int pos) {
        return this.mClassificationResults.get(pos);
    }

    ArrayList<Classifier.Recognition> toList() {
        return new ArrayList<>(this.mClassificationResults);
    }

    // inner class to hold a reference to each item of RecyclerView
    static class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView rivPlantImage;
        final ImageView ivPlantType;
        final TextView tvPlantTitle;
        final TextView tvPlantSubtitle;
        final TextView tvPlantType;
        final TextView tvConfidenceLevel;
        final Button btnMatchIdentification;

        ViewHolder(View itemLayoutView, final RecyclerClickListener listener) {
            super(itemLayoutView);
            tvPlantTitle = itemLayoutView.findViewById(R.id.tv_plant_title);
            tvPlantSubtitle = itemLayoutView.findViewById(R.id.tv_plant_sub_title);
            tvPlantType = itemLayoutView.findViewById(R.id.tv_plant_type);
            tvConfidenceLevel = itemLayoutView.findViewById(R.id.tv_confidence);
            ivPlantType = itemLayoutView.findViewById(R.id.iv_plant_type);
            rivPlantImage = itemLayoutView.findViewById(R.id.iv_plant_image);
            btnMatchIdentification = itemLayoutView.findViewById(R.id.btn_id_match);

            itemLayoutView.setOnClickListener(v -> listener.onRecyclerItemClick(getAdapterPosition()));
            btnMatchIdentification.setOnClickListener(view -> listener.onMatchButtonClicked(getAdapterPosition()));
        }

        public void setData(Classifier.Recognition data, boolean showMatchButtons) {
            Log.i(TAG, data.toString());

            if (!showMatchButtons) {
                btnMatchIdentification.setVisibility(View.GONE);
            }

            tvConfidenceLevel.setText(data.getDisplayableConfidence());

            Plant plant = data.getPlantInfo();
            if (plant != null) {
                tvPlantTitle.setText(plant.getCommonName());
                tvPlantSubtitle.setText(plant.getBotanicalName());
                tvPlantType.setText(plant.getType());

                //TODO: Can remove this when live on server
                GlideImageHelper.glideLoadCircleImage(rivPlantImage, plant.getImgUrl().replace("localhost", "10.0.2.2"));

                int iconId = -1;
                if (plant.getType().toLowerCase().contains("cactus")) {
                    iconId = R.drawable.ic_type_cactus;
                } else if (plant.getType().equalsIgnoreCase("tree")) {
                    iconId = R.drawable.ic_type_tree;
                } else if (plant.getType().equalsIgnoreCase("climber")) {
                    iconId = R.drawable.ic_type_creeper;
                } else {
                    iconId = R.drawable.ic_type_plant;
                }

                ivPlantType.setImageDrawable(ContextCompat.getDrawable(ivPlantType.getContext(), iconId));
            }

        }
    }
}
