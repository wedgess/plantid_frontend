/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.edituserprofile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.source.local.UsersLocalDataSource;
import eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.FileUtils;

import static eu.wedgess.plantid.features.mainnavigation.MainNavigationActivity.REQUEST_CODE_GALLERY_IMAGE;

/**
 * Activity to edit a users profile information.
 * Called from {@link eu.wedgess.plantid.features.userdetails.UserDetailsFragment}
 */
public class EditUserProfileActivity extends BaseActivity implements EditUserProfileContract.View {

    private static final String TAG = EditUserProfileActivity.class.getSimpleName();

    public static final String STATE_KEY_USERNAME = "STATE_KEY_USERNAME";

    //region View bindings
    @BindView(R.id.iv_blur_img)
    ImageView mBlurredIV;
    @BindView(R.id.til_username_edit)
    TextInputLayout mUsernameTIL;
    @BindView(R.id.iv_update_user_profile)
    ImageView mUserProfileIV;
    @BindView(R.id.fab_update_select_profile_img)
    FloatingActionButton mSelectProfileImgFAB;
    @BindView(R.id.btn_update_profile)
    Button mUpdateProfileBTN;
    //endregion

    private String mImagePath;
    private EditUserProfileContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);

        ButterKnife.bind(this);
        Toolbar tb = findViewById(R.id.toolbar_edit_profile);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // text listener to ensure there are no spaces in username
        mUsernameTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().contains(" ")) {
                    mUsernameTIL.setErrorEnabled(true);
                    mUsernameTIL.setError(getString(R.string.error_username_contains_spaces));
                } else if (!TextUtils.isEmpty(editable.toString()) && mUsernameTIL.isErrorEnabled()) {
                    mUsernameTIL.setErrorEnabled(false);
                }
            }
        });

        // open gallery image picker when user chooses to update profile image
        mSelectProfileImgFAB.setOnClickListener(view -> openGalleryImagePicker());

        mUpdateProfileBTN.setOnClickListener(view -> {
            // if username field has an error then don;t attempt to update user
            if (mUsernameTIL.isErrorEnabled()) {
                Toast.makeText(EditUserProfileActivity.this, R.string.error_must_fix_fields, Toast.LENGTH_LONG).show();
            } else {
                mPresenter.updateUser(mUsernameTIL.getEditText().getText().toString(), mImagePath);
            }
        });

        AppDatabase database = AppDatabase.getInstance(EditUserProfileActivity.this);
        new EditUserProfilePresenter(
                UserRepository.getInstance(
                        new UsersLocalDataSource(database.userDao(),
                                PreferenceManager.getDefaultSharedPreferences(EditUserProfileActivity.this)),
                        new UsersRemoteDataSource()),
                this);

        // handle which data to show on orientation change
        if (savedInstanceState == null) {
            mPresenter.subscribe();
        } else {
            setProfileUsername(savedInstanceState.getString(STATE_KEY_USERNAME, ""));
            // if image path is null load original image
            if (mImagePath == null) {
                mPresenter.getUserProfileImage();
            } else {
                // otherwise load new image from path
                setProfileImage(mImagePath);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save state on config change
        outState.putString(TAG, mImagePath);
        outState.putString(STATE_KEY_USERNAME, mUsernameTIL.getEditText().getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mImagePath = savedInstanceState.getString(TAG);
    }

    private void openGalleryImagePicker() {
        // open media picker chooser to select image
        Intent intent = new Intent();
        // accept and file type that is an image
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_select_image)), REQUEST_CODE_GALLERY_IMAGE);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @Override
    public void setPresenter(@NonNull EditUserProfileContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setProfileImage(String imgUrl) {
        GlideImageHelper.glideLoadBlurredImage(mBlurredIV, imgUrl);
        GlideImageHelper.glideLoadCircleImage(mUserProfileIV, imgUrl);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setProfileUsername(String username) {
        mUsernameTIL.getEditText().setText(username);
        mUsernameTIL.getEditText().setSelection(username.length());
    }

    @Override
    public void userUpdatedSuccessfully() {
        finish();
    }

    @Override
    public void failedToUpdateUser() {
        Toast.makeText(EditUserProfileActivity.this, R.string.toast_msg_failed_to_update_user, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setEmptyUsernameError() {
        mUsernameTIL.setErrorEnabled(true);
        mUsernameTIL.setError(getString(R.string.error_username_empty));
    }

    @Override
    public void setUsernameContainsSpacesError() {
        mUsernameTIL.setErrorEnabled(true);
        mUsernameTIL.setError(getString(R.string.error_username_contains_spaces));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // get image result back from gallery and load into views
        if (requestCode == REQUEST_CODE_GALLERY_IMAGE && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                String imgPath = FileUtils.getRealPathFromDocumentUri(getApplicationContext(), filePath);
                GlideImageHelper.glideLoadCircleImage(mUserProfileIV, imgPath);
                GlideImageHelper.glideLoadBlurredImage(mBlurredIV, imgPath);
                mImagePath = imgPath;
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }
}
