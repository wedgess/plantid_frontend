/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plantdetails;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Strings;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The presenter for {@link PlantDetailActivity}
 * <p>
 * Created by gar on 24/11/17.
 */
public class PlantDetailPresenter implements PlantDetailContract.Presenter {

    private static final String TAG = PlantDetailPresenter.class.getSimpleName();

    private final PlantsRepository mPlantsRepository;
    private final PlantDetailContract.View mPlantDetailView;
    private final CompositeDisposable mCompositeDisposable;

    @NonNull
    private String mPlantId;

    PlantDetailPresenter(@NonNull String plantId,
                         @NonNull PlantsRepository plantsRepository,
                         @NonNull PlantDetailContract.View taskDetailView) {
        mPlantId = checkNotNull(plantId, "Plant ID cannot be null, when viewing specfic plant");
        mPlantsRepository = checkNotNull(plantsRepository, "plantsRepository cannot be null!");
        mPlantDetailView = checkNotNull(taskDetailView, "plantDetailView cannot be null!");

        mPlantDetailView.setPresenter(this);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        loadPlantDetails();
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @SuppressWarnings("Guava")
    private void loadPlantDetails() {
        if (Strings.isNullOrEmpty(mPlantId)) {
            mPlantDetailView.showMissingPlant();
            return;
        }
        // get the lant by its id
        mCompositeDisposable.add(mPlantsRepository
                .getPlantById(mPlantId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(plant -> {
                    // ensure that the plant is present as it is an Optional
                    if (plant.isPresent()) {
                        setPlantDetailsUI(plant.get());
                    } else {
                        mPlantDetailView.showMissingPlant();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error getting plant by id");
                    mPlantDetailView.showMissingPlant();
                }));
    }

    // gets the plant icon based on Plant#type
    private int getPlantTypeIcon(String type) {
        int iconId = -1;
        if (type.toLowerCase().contains("cactus")) {
            iconId = R.drawable.ic_type_cactus;
        } else if (type.equalsIgnoreCase("tree")) {
            iconId = R.drawable.ic_type_tree;
        } else if (type.equalsIgnoreCase("climber")) {
            iconId = R.drawable.ic_type_creeper;
        } else {
            iconId = R.drawable.ic_type_plant;
        }
        return iconId;
    }

    // set all plant details in the UI
    private void setPlantDetailsUI(@NonNull Plant plant) {
        checkNotNull(plant, "Error plant is null");
        mPlantDetailView.setPlantHeaderImage(plant.getImgUrl());
        mPlantDetailView.setPlantNames(plant.getBotanicalName(), plant.getCommonName());
        mPlantDetailView.setPlantDescription(plant.getDescription());

        if (Strings.isNullOrEmpty(plant.getParsedHeight())) {
            mPlantDetailView.hidePlantHeight();
        } else {
            mPlantDetailView.setPlantHeight(plant.getParsedHeight());
        }
        if (Strings.isNullOrEmpty(plant.getParsedWidth())) {
            mPlantDetailView.hidePlantWidth();
        } else {
            mPlantDetailView.setPlantWidth(plant.getParsedWidth());
        }
        LinkedHashMap<String, Boolean> flowersMap = buildCalendarGrid(plant.getFlowerTime());
        if (flowersMap == null) {
            mPlantDetailView.hideFlowersGridView();
        } else {
            mPlantDetailView.setFlowersGridView(flowersMap);
        }
        LinkedHashMap<String, Boolean> sowingTimeMap = buildCalendarGrid(plant.getSowTime());
        if (sowingTimeMap == null) {
            mPlantDetailView.hideSowTimeGridView();
        } else {
            mPlantDetailView.setSowTimeGridView(sowingTimeMap);
        }
        LinkedHashMap<String, Boolean> collectSeedTimeMap = buildCalendarGrid(plant.getCollectSeedTime());
        if (collectSeedTimeMap == null) {
            mPlantDetailView.hideCollectSeedTimeGridView();
        } else {
            mPlantDetailView.setCollectSeedTimeGridView(collectSeedTimeMap);
        }
        LinkedHashMap<String, Boolean> plantTimeMap = buildCalendarGrid(plant.getPlantTime());
        if (plantTimeMap == null) {
            mPlantDetailView.hidePlantTimeGridView();
        } else {
            mPlantDetailView.setPlantTimeGridView(plantTimeMap);
        }
        LinkedHashMap<String, Boolean> pruneTimeMap = buildCalendarGrid(plant.getPruneTime());
        if (pruneTimeMap == null) {
            mPlantDetailView.hidePruneTimeGridView();
        } else {
            mPlantDetailView.setPruneTimeGridView(pruneTimeMap);
        }
        String[] flowerColors = plant.getFlowerColorsArray();
        if (flowerColors != null && flowerColors.length > 0) {
            mPlantDetailView.setFlowerColorGridView(flowerColors);
        }
        String[] foliageColors = plant.getFoliageColorsArray();
        if (foliageColors != null && foliageColors.length > 0) {
            mPlantDetailView.setFoliageColorGridView(foliageColors);
        }

        if (Strings.isNullOrEmpty(plant.getFeatures())) {
            mPlantDetailView.hidePlantFeatures();
        } else {
            mPlantDetailView.setPlantFeatures(plant.getFeatures());
        }
        if (Strings.isNullOrEmpty(plant.getSunExposure())) {
            mPlantDetailView.hidePlantSunExposure();
        } else {
            mPlantDetailView.setPlantSunExposure(plant.getSunExposure());
        }
        if (Strings.isNullOrEmpty(plant.getType())) {
            mPlantDetailView.hidePlantType();
        } else {
            mPlantDetailView.setPlantType(plant.getType(), getPlantTypeIcon(plant.getType()));
        }
        if (Strings.isNullOrEmpty(plant.getSoilType())) {
            mPlantDetailView.hidePlantSoilType();
        } else {
            mPlantDetailView.setPlantSoilType(plant.getSoilType());
        }
        if (Strings.isNullOrEmpty(plant.getHardiness())) {
            mPlantDetailView.hidePlantHardiness();
        } else {
            mPlantDetailView.setPlantHardiness(plant.getHardiness());
        }
        if (Strings.isNullOrEmpty(plant.getMaintenance())) {
            mPlantDetailView.hidePlantMaintenance();
        } else {
            mPlantDetailView.setPlantMaintenance(plant.getMaintenance());
        }

        List<String> attractsList = Arrays.asList(plant.getAttractsArray());
        if (attractsList.isEmpty()) {
            mPlantDetailView.hideAttractsLayout();
        } else {
            mPlantDetailView.setAttractsBirdsActive(attractsList.contains("Birds"));
            mPlantDetailView.setAttractsBeesActive(attractsList.contains("Bees"));
            mPlantDetailView.setAttractsInsectsActive(attractsList.contains("Beneficial insects"));
            mPlantDetailView.setAttractsMothsButterfliesActive(attractsList.contains("Butterflies / Moths"));
            mPlantDetailView.setAttractsOtherPollinatorsActive(attractsList.contains("Other pollinators"));
        }

        List<String> toxicityList = Arrays.asList(plant.getToxicityArray());
        if (toxicityList.isEmpty()) {
            mPlantDetailView.hideToxicityLayout();
        } else {
            mPlantDetailView.setToxicityBirdsActive(toxicityList.contains("Birds"));
            mPlantDetailView.setToxicityCatActive(toxicityList.contains("Cats"));
            mPlantDetailView.setToxicityDogActive(toxicityList.contains("Dogs"));
            mPlantDetailView.setToxicityPersonActive(toxicityList.contains("People"));
            mPlantDetailView.setToxicityLivestockActive(toxicityList.contains("Livestock"));
            mPlantDetailView.setToxicityHorseActive(toxicityList.contains("Horses"));
        }

        if (!Strings.isNullOrEmpty(plant.getEffects())) {
            mPlantDetailView.setEffects(plant.getEffects());
        }

        if (!Strings.isNullOrEmpty(plant.getWildlifeFeatures())) {
            mPlantDetailView.setWildLifeFeatures(plant.getWildlifeFeatures());
        }

    }

    private LinkedHashMap<String, Boolean> buildCalendarGrid(String selected) {
        if (Strings.isNullOrEmpty(selected)) {
            return null;
        }
        String[] monthsAbbrv = mPlantDetailView.getMonthsList();
        LinkedHashMap<String, Boolean> months = new LinkedHashMap<>();
        for (String month : monthsAbbrv) {
            months.put(month, selected.contains(month));
        }
        return months;
    }
}
