/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.register;

import android.support.annotation.NonNull;

import java.io.File;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;

/**
 * Contract between {@link RegisterActivity} and {@link RegisterPresenter}
 * <p>
 * Created by gar on 27/12/17.
 */

class RegisterContract {

    interface View extends BaseView<Presenter> {

        void setRegisteringIndicator(boolean active);

        void setRegistrationErrorText(boolean showError);

        void setUsernameEmptyError();

        void setPasswordEmptyError();

        void setPasswordConfirmationEmptyError();

        void setPasswordConfirmationNoMatchError();

        void setSuccessfulRegistration();

    }

    interface Presenter extends BasePresenter {

        void performRegistration(@NonNull String username, @NonNull String password, @NonNull String passwordConfirmation, File image);

        boolean validateRegistrationFields(@NonNull String username, @NonNull String password, @NonNull String passwordConfirmation);

    }
}
