package eu.wedgess.plantid.features.history;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;
import eu.wedgess.plantid.data.History;

/**
 * Contract between {@link HistoryActivity} & {@link HistoryPresenter}
 * <p>
 * Created by gar on 09/12/17.
 */

public class HistoryContract {

    interface View extends BaseView<Presenter> {

        void setHistoryData(List<History> plantsIds);

        void setLoadingIndicator(boolean active);

        void showNoHistoryData();

        void showLoadingHistoryError();

    }

    interface Presenter extends BasePresenter {

        void deleteHistoryItems(@NonNull List<History> historyItems);
    }
}
