/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threads;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;
import eu.wedgess.plantid.data.Thread;

/**
 * Contract for {@link ThreadsFragment} and {@link ThreadsPresenter}
 * <p>
 * Created by gar on 09/11/17.
 */

class ThreadsContract {

    interface View extends BaseView<Presenter> {

        void setThreadsData(@NonNull List<Thread> threads);

        void showThreadDetailsUi(@NonNull String threadId);

        void setLoadingIndicator(boolean active);

        void showNoThreadsData();

        void showLoadingThreadsError();

        boolean isActive();

        void setSwipeRefreshRefreshing(boolean refreshing);

        void showSolvedFilterLabel();

        void showUnsolvedFilterLabel();

        void showAllFilterLabel();

        void showNotCommentedFilterLabel();

        void showFilteringPopUpMenu();

        boolean adapterIsNotCreated();

        void updateThreadsUi(@NonNull List<Thread> threads);
    }

    interface Presenter extends BasePresenter {

        void loadThreads(boolean showLoadingUI);

        void openThreadDetailsActivity(@NonNull Thread thread);

        void refreshThreads();

        void setSearchQuery(String query);

        void setFiltering(@NonNull ThreadsFilterType requestType);

        ThreadsFilterType getFiltering();

        String getSearchFilterQuery();
    }
}
