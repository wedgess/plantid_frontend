/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plantdetails;

/**
 * This is an adapter for displaying the 12 months of the year.
 * Selected months will be coloured purple. while unselected months will be grey.
 * This is used to display, flowering, sowing, pruning etc times in:
 * {@link eu.wedgess.plantid.features.plantdetails.PlantDetailActivity}
 * <p>
 * Created by gar on 25/11/17.
 **/

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.LinkedHashMap;

import eu.wedgess.plantid.R;

public class CalendarMonthAdapter extends BaseAdapter {

    private LinkedHashMap<String, Boolean> monthsMap;
    private Context context;

    CalendarMonthAdapter(LinkedHashMap<String, Boolean> months, Context context) {
        this.monthsMap = months;
        this.context = context;
    }

    @Override
    public int getCount() {
        return monthsMap.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private Boolean getValueByIndex(LinkedHashMap<String, Boolean> hMap, int index) {
        return (Boolean) hMap.values().toArray()[index];
    }

    private String getKeyByIndex(LinkedHashMap<String, Boolean> hMap, int index) {
        return (String) hMap.keySet().toArray()[index];
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.grid_calendar_month, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvMonth.setText(getKeyByIndex(monthsMap, position));
        viewHolder.viewSlice.setBackgroundColor(
                getValueByIndex(monthsMap, position)
                        ? ContextCompat.getColor(context, R.color.purple)
                        : ContextCompat.getColor(context, R.color.dividers));

        return convertView;
    }

    private static class ViewHolder {
        TextView tvMonth;
        View viewSlice;

        ViewHolder(View view) {
            tvMonth = view.findViewById(R.id.monht_text_tv);
            viewSlice = view.findViewById(R.id.month_view);

        }
    }
}
