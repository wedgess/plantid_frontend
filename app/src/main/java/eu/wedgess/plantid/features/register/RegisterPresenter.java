/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.register;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Strings;

import java.io.File;

import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link RegisterActivity}
 * <p>
 * Created by gar on 09/11/17.
 */

public class RegisterPresenter implements RegisterContract.Presenter {

    private static final String TAG = RegisterPresenter.class.getSimpleName();


    private final UserRepository mUserRepository;
    private final RegisterContract.View mRegistrationView;
    private final CompositeDisposable mCompositeDisposable;

    RegisterPresenter(UserRepository userRepository, RegisterContract.View registerView) {
        this.mUserRepository = checkNotNull(userRepository);
        this.mRegistrationView = checkNotNull(registerView);
        mRegistrationView.setPresenter(this);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        // do nothing on subscribe
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void performRegistration(@NonNull String username, @NonNull String password,
                                    @NonNull String passwordConfirmation, File profileImg) {
        mRegistrationView.setRegisteringIndicator(true);

        mCompositeDisposable.add(mUserRepository.registerUser(new User(username, password), profileImg)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(user -> {
                    // save registered user in shared preferences
                    mCompositeDisposable.add(mUserRepository.insertCurrentUserToPreferences(user)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnComplete(() -> {
                                mRegistrationView.setRegisteringIndicator(false);
                                mRegistrationView.setSuccessfulRegistration();
                            })
                            .subscribe(() -> Log.i(TAG, "Current user inserted into preferences"),
                                    throwable -> Log.e(TAG, "erro inserting current user to preferences")));
                    Log.i(TAG, "Registered in succesfully");
                }, throwable -> {
                    Log.i(TAG, "Registration in failed");
                    mRegistrationView.setRegisteringIndicator(false);
                    mRegistrationView.setRegistrationErrorText(true);
                }));
    }

    @Override
    public boolean validateRegistrationFields(@NonNull String username, @NonNull String password, @NonNull String passwordConfirmation) {
        if (Strings.isNullOrEmpty(username)) {
            mRegistrationView.setUsernameEmptyError();
            mRegistrationView.setRegisteringIndicator(false);
            return false;
        }
        if (Strings.isNullOrEmpty(password)) {
            mRegistrationView.setPasswordEmptyError();
            mRegistrationView.setRegisteringIndicator(false);
            return false;
        }
        if (Strings.isNullOrEmpty(passwordConfirmation)) {
            mRegistrationView.setPasswordConfirmationEmptyError();
            mRegistrationView.setRegisteringIndicator(false);
            return false;
        }

        if (!passwordConfirmation.equals(password)) {
            mRegistrationView.setPasswordConfirmationNoMatchError();
            mRegistrationView.setRegisteringIndicator(false);
            return false;
        }

        return true;
    }
}
