/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plants;

/**
 * Filter types for filtering plants in {@link PlantsFragment} and used by
 * {@link PlantsPresenter}.
 * <p>
 * Created by gar on 28/01/18.
 */
public enum PlantsFilterType {

    ALL_PLANTS("All"),
    ALPINE_ONLY("Alpine"),
    ANNUAL_ONLY("Annual"),
    BULBS_ONLY("Bulb"),
    CLIMBER_ONLY("Climber"),
    CONSERVATORY_ONLY("Conservatory"),
    PERENNIAL_ONLY("Perennial"),
    SHRUBS_ONLY("Shrub"),
    TREES_ONLY("Tree");


    PlantsFilterType(String tag) {
        this.type = tag;
    }

    private String type;

    public String getStringType() {
        return type;
    }
}
