/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plants;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;
import eu.wedgess.plantid.data.Plant;

/**
 * Contract for {@link PlantsFragment} and {@link PlantsPresenter}
 * <p>
 * Created by gar on 09/11/17.
 */

class PlantsContract {

    interface View extends BaseView<Presenter> {

        void setPlantsData(@NonNull List<Plant> plants);

        void showPlantDetailsUi(@NonNull String plantId);

        void setLoadingIndicator(boolean active);

        void showNoPlantData();

        void showLoadingPlantsError();

        boolean isActive();

        void showAllFilterLabel();

        void showPerenialsFilterLabel();

        void showShrubsFilterLabel();

        void showTreesFilterLabel();

        void showBulbsFilterLabel();

        void showConservatoryFilterLabel();

        void showClimbersFilterLabel();

        void showAlpineFilterLabel();

        void showAnnualFilterLabel();

        void showFilteringPopUpMenu();

    }

    interface Presenter extends BasePresenter {

        void loadPlants(boolean showLoadingUI);

        void openPlantDetailsActivity(@NonNull Plant plant);

        void setFiltering(@NonNull PlantsFilterType requestType);

        PlantsFilterType getFiltering();

        void setSearchQuery(String query);

        String getSearchFilterQuery();
    }
}
