package eu.wedgess.plantid.features.threaddetails;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.base.Strings;

import java.text.ParseException;
import java.util.List;

import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.utils.DateUtility;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link ThreadDetailActivity}
 * <p>
 * Created by gar on 02/12/17.
 */

public class ThreadDetailPresenter implements ThreadDetailContract.Presenter {

    private static final String TAG = ThreadDetailPresenter.class.getSimpleName();

    private final String mThreadId;
    private final ThreadsRepository mThreadsRepository;
    private final CommentsRepository mCommentsRepository;
    private final UserRepository mUserRepository;
    private final ThreadDetailContract.View mThreadDetailView;
    private final CompositeDisposable mCompositeDisposable;
    private boolean mThreadIsSolved = false;

    ThreadDetailPresenter(@Nullable String threadId,
                          @NonNull UserRepository userRepository,
                          @NonNull CommentsRepository commentsRepository,
                          @NonNull ThreadsRepository threadsRepository,
                          @NonNull ThreadDetailContract.View threadDetailView) {
        mThreadId = checkNotNull(threadId);
        mUserRepository = checkNotNull(userRepository, "userRepository cannot be null!");
        mCommentsRepository = checkNotNull(commentsRepository, "commentsRepository cannot be null!");
        mThreadsRepository = checkNotNull(threadsRepository, "threadDetailView cannot be null!");
        mThreadDetailView = checkNotNull(threadDetailView, "threadDetailView cannot be null!");

        mCompositeDisposable = new CompositeDisposable();

        mThreadDetailView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        openThread();
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    /**
     * Load the thread, get thread author and thread comments and add them to the Thread object
     */
    private void openThread() {
        if (Strings.isNullOrEmpty(mThreadId)) {
            mThreadDetailView.showMissingThread();
            return;
        }

        mThreadDetailView.setThreadLoadingIndicator(true);

        mCompositeDisposable.add(mThreadsRepository.getThreadById(mThreadId).toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(thread ->
                        mUserRepository
                                .getUserById(thread.getAuthorId())
                                .toObservable()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .map(user -> {
                                    thread.setAuthor(user);
                                    return thread;
                                }))
                .flatMap(thread -> mCommentsRepository
                        .getCommentsByThreadId(thread.getId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(comments -> {
                            thread.setComments(comments);
                            return thread;
                        }))
                .subscribe(thread -> {
                    mThreadIsSolved = thread.isSolved();
                    mThreadDetailView.setThreadLoadingIndicator(false);
                    mThreadDetailView.setIsThreadAuthor(thread.getAuthorId());
                    showThread(thread);
                    showComments(thread.getComments());
                    if (thread.isSolved()) {
                        mThreadDetailView.lockThread();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error fetching thread with id: " + mThreadId);
                    mThreadDetailView.showMissingThread();
                }));
    }

    private void showThread(Thread thread) {
        if (thread == null) {
            mThreadDetailView.showMissingThread();
        } else {
            processThreadDetails(thread);
        }
    }

    private void processThreadDetails(Thread thread) {
        mThreadDetailView.setThreadTitle(thread.getTitle());
        mThreadDetailView.setThreadMessage(thread.getMessage());

        mThreadDetailView.setThreadImage(thread.getThreadImgUrl());

        if (thread.getAuthor() != null && !Strings.isNullOrEmpty(thread.getAuthor().getUserName())) {
            mThreadDetailView.setThreadAuthor(thread.getAuthor().getUserName());
        }

        if (Strings.isNullOrEmpty(thread.getLocation())) {
            mThreadDetailView.setThreadLocation("Unknown");
        } else {
            mThreadDetailView.setThreadLocation(thread.getLocation());
        }

        if (thread.getAuthor() != null && !Strings.isNullOrEmpty(thread.getAuthor().getImgUrl())) {
            mThreadDetailView.setThreadAuthorProfileImage(thread.getAuthor().getImgUrl());
        }

        if (Strings.isNullOrEmpty(thread.getDateCreated())) {
            mThreadDetailView.setThreadDate("Not specified");
        } else {
            // use normal date as default
            String prettyThreadDate = thread.getDateCreated();
            try {
                prettyThreadDate = DateUtility.getRelativeTime(thread.getDateCreated());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            mThreadDetailView.setThreadDate(prettyThreadDate);
        }
        mThreadDetailView.setThreadLoadingIndicator(false);
    }

    // get all threads comment and their authors and sort them by newest first
    private void showComments(List<Comment> comments) {
        mCompositeDisposable.add(Observable.just(comments)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(Observable::fromIterable) //map the list to an Observable that emits every item as an observable
                .flatMap(comment ->
                        mUserRepository
                                .getUserById(comment.getAuthorId())
                                .toObservable()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .map(user -> {
                                    comment.setAuthor(user);
                                    return comment;
                                }))
                .toSortedList((comment1, comment2) -> comment2.getDateCreated().compareTo(comment1.getDateCreated())) // sort list latest first
                .subscribe(commentList -> {
                    if (commentList.isEmpty()) {
                        mThreadDetailView.showNoComments();
                    } else {
                        mThreadDetailView.setCommentsLoadingIndicator(false);
                        if (mThreadDetailView.adapterIsNotCreated()) {
                            mThreadDetailView.setCommentsUi(commentList);
                        } else {
                            mThreadDetailView.updateCommentsUi(commentList);
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "error loading comments");
                }));

    }

    @Override
    public void editThread() {
        if (!Strings.isNullOrEmpty(mThreadId)) {
            mThreadDetailView.openEditThreadDetails(mThreadId);
        }
    }

    @Override
    public void deleteThread() {
        if (!Strings.isNullOrEmpty(mThreadId)) {
            mCompositeDisposable.add(mThreadsRepository.getThreadById(mThreadId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(thread -> {
                        mCompositeDisposable.add(mThreadsRepository.deleteThread(thread)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(() -> {
                                            mThreadDetailView.threadDeleteComplete();
                                        },
                                        throwable -> {
                                            Log.e(TAG, "Error deleting thread");
//                                            throw Exceptions.propagate(throwable);
                                        }));
                    }, throwable -> Log.e(TAG, "Error getting thread by id")));
        }
    }

    @Override
    public void setCommentAsAnswer(@NonNull Comment commentItem) {
        commentItem.setAnswer(true);
        mThreadIsSolved = true;
        mCompositeDisposable.add(mCommentsRepository.updateComment(commentItem)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                            mCompositeDisposable.add(mThreadsRepository.getThreadById(commentItem.getThreadId())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(thread -> {
                                        thread.setSolved(true);
                                        mThreadsRepository.updateThread(thread)
                                                .subscribeOn(Schedulers.io())
                                                .subscribe(() -> Log.i(TAG, "Thread updated as solved"),
                                                        throwable -> Log.e(TAG, "Error can;t update thread as solved"));
                                    }, throwable -> Log.e(TAG, "Error getting thread by id")));
                        },
                        throwable -> {
                            Log.e(TAG, "Error updating comment as answer");
//                            throw Exceptions.propagate(throwable);
                        }));
    }

    @Override
    public boolean isThreadSolved() {
        return mThreadIsSolved;
    }

    @Override
    public void deleteComment(@NonNull Comment comment, int position) {
        mCompositeDisposable.add(mCommentsRepository.deleteComment(comment)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    mThreadDetailView.commentDeleted(position);
                    mThreadsRepository.removeCachedComment(comment);
                }, throwable -> Log.e(TAG, "Error deleting comment")));
    }

    @SuppressWarnings("Guava")
    @Override
    public void canPostComment() {
        mCompositeDisposable.add(mUserRepository.getCurrentUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userOptional -> {
                    if (userOptional.isPresent()) {
                        mThreadDetailView.openCreateCommentActivity();
                    } else {
                        mThreadDetailView.showNotLoggedInError();
                    }
                }));
    }
}
