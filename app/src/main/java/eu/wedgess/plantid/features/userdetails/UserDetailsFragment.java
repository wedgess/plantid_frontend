/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.userdetails;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.custom.dialogs.ConfirmationDialog;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.local.CommentsLocalDataSource;
import eu.wedgess.plantid.data.source.local.HistoryLocalDataSource;
import eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource;
import eu.wedgess.plantid.data.source.local.UsersLocalDataSource;
import eu.wedgess.plantid.data.source.remote.CommentsRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.HistoryRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.ThreadsRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.features.edituserprofile.EditUserProfileActivity;
import eu.wedgess.plantid.features.history.HistoryActivity;
import eu.wedgess.plantid.features.login.LoginActivity;
import eu.wedgess.plantid.features.mainnavigation.MainNavigationActivity;
import eu.wedgess.plantid.features.threaddetails.ThreadDetailActivity;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.LocationUtils;
import eu.wedgess.plantid.utils.PlantPreferenceManager;
import eu.wedgess.plantid.utils.UiUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link Fragment} subclass which displas all user information. Profile, history and threads.
 */
public class UserDetailsFragment extends Fragment implements UserDetailContract.View,
        View.OnClickListener,
        ConfirmationDialog.ConfirmationDialogCompleteListener {

    private static final String TAG = UserDetailsFragment.class.getSimpleName();

    public static final String EXTRA_KEY_HISTORY_TYPE = "EXTRA_KEY_HISTORY_TYPE";
    public static final int REQUEST_CODE_LOGIN = 10;
    public static final int REQUEST_CODE_HISTORY = 11;
    private static final int REQUEST_CODE_EDIT_USER_PROFILE = 12;

    //region View binding
    @BindView(R.id.iv_profile_img)
    ImageView mProfileIV;
    @BindView(R.id.tv_username_title)
    TextView mUsernameTV;
    @BindView(R.id.rl_view_sign_in)
    RelativeLayout mSignInRL;
    @BindView(R.id.rl_view_matched)
    RelativeLayout mMatchedHistoryRL;
    @BindView(R.id.tv_matched_value)
    TextView mMatchedValueTV;
    @BindView(R.id.tv_unmatched_value)
    TextView mUnmatchedValueTV;
    @BindView(R.id.tv_date_created)
    TextView mDateCreatedTV;
    @BindView(R.id.rl_view_unmatched)
    RelativeLayout mUnmatchedHistoryRL;
    @BindView(R.id.tv_sign_in_txt)
    TextView mLoginBtnTV;
    @BindView(R.id.tv_user_location)
    TextView mUserLocationTV;
    @BindView(R.id.tv_users_empty_threads_msg)
    TextView mUserNoThreadsTV;
    @BindView(R.id.rv_users_threads)
    RecyclerView mUsersThreadsRV;
    @BindView(R.id.ib_delete_user)
    ImageButton mDeleteUserProfileIB;
    @BindView(R.id.ib_edit_user)
    ImageButton mEditUserProfileIB;
    @BindView(R.id.profile_actions_divider)
    View mProfileActionsDivider;
    //endregion

    private UserDetailContract.Presenter mPresenter;
    private Unbinder mUnbinder;
    private boolean mShouldExecuteOnResume;

    public UserDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UserDetailsFragment.
     */
    public static UserDetailsFragment newInstance() {
        return new UserDetailsFragment();
    }

    @Override
    public void setPresenter(@NonNull UserDetailContract.Presenter presenter) {
        this.mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mShouldExecuteOnResume = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);
        if (isAdded()) {
            // set up presenter
            AppDatabase database = AppDatabase.getInstance(getActivity());
            HistoryRepository historyRepository = HistoryRepository.getInstance(
                    new HistoryLocalDataSource(database.historyDao()),
                    new HistoryRemoteDataSource());
            ThreadsRepository threadsRepository = ThreadsRepository.getInstance(
                    new ThreadsLocalDataSource(database.threadDao()),
                    new ThreadsRemoteDataSource());
            CommentsRepository commentsRepository = CommentsRepository.getInstance(
                    new CommentsLocalDataSource(database.commentDao()),
                    new CommentsRemoteDataSource());
            new UserDetailsPresenter(
                    UserRepository.getInstance(
                            new UsersLocalDataSource(database.userDao(),
                                    PreferenceManager.getDefaultSharedPreferences(getActivity())),
                            new UsersRemoteDataSource()),
                    this,
                    threadsRepository,
                    commentsRepository,
                    historyRepository,
                    PlantPreferenceManager.hideUserLocation(getActivity()));
        }
        mSignInRL.setOnClickListener(this);
        mMatchedHistoryRL.setOnClickListener(this);
        mUnmatchedHistoryRL.setOnClickListener(this);
        mDeleteUserProfileIB.setOnClickListener(this);
        mEditUserProfileIB.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.subscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unSubscribe();
        mUnbinder.unbind();
    }

    @Override
    public void showUserLocation() {
        String location = LocationUtils.getLocationName(getActivity());
        mUserLocationTV.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(location)) {
            mUserLocationTV.setText(R.string.home_location_unknown);
        } else {
            mUserLocationTV.setText(location);
        }
    }

    @Override
    public void hideUserLocation() {
        mUserLocationTV.setVisibility(View.GONE);
    }

    @Override
    public void setUserProfileImage(@NonNull String imgUrl) {
        GlideImageHelper.glideLoadCircleImage(mProfileIV, imgUrl);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void setUserProfileUsername(@NonNull String username) {
        mUsernameTV.setText(username);
    }

    @Override
    public void setUserProfileDateCreated(@NonNull String dateCreated) {
        mDateCreatedTV.setText(dateCreated);
    }

    @Override
    public void setSignedInButtonText() {
        mLoginBtnTV.setText(R.string.home_sign_in);
    }

    @Override
    public void setSignedOutButtonText() {
        mLoginBtnTV.setText(R.string.home_sign_out);
    }

    @Override
    public void setUserNotLoggedIn() {
        mUsernameTV.setText(R.string.home_not_signed_in);
        mDateCreatedTV.setText(R.string.home_sign_in_or_register_below);
        mProfileIV.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_placeholder_profile_img));
        setSignedInButtonText();
    }

    private void setProfileActionControlsVisibility(boolean visisble) {
        if (visisble) {
            mProfileActionsDivider.setVisibility(View.VISIBLE);
            mDeleteUserProfileIB.setVisibility(View.VISIBLE);
            mEditUserProfileIB.setVisibility(View.VISIBLE);
        } else {
            mProfileActionsDivider.setVisibility(View.GONE);
            mDeleteUserProfileIB.setVisibility(View.GONE);
            mEditUserProfileIB.setVisibility(View.GONE);
        }
    }

    @Override
    public void setMatchedHistoryCount(int count) {
        UiUtils.startCountAnimation(count, mMatchedValueTV);
    }

    @Override
    public void setUnmatchedHistoryCount(int count) {
        UiUtils.startCountAnimation(count, mUnmatchedValueTV);
    }

    @Override
    public void setMatchedHistoryCountNoAnim(int count) {
        mMatchedValueTV.setText(String.valueOf(count));
    }

    @Override
    public void setUnmatchedHistoryCountNoAnim(int count) {
        mUnmatchedValueTV.setText(String.valueOf(count));

    }

    @Override
    public void showNoHistoryItemsToast() {
        Toast.makeText(getActivity(), R.string.home_toast_no_history, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setUsersThreads(@NonNull List<Thread> usersThreads) {
        Log.i(TAG, "Setting users threads, found: " + usersThreads.size());
        if (usersThreads.isEmpty()) {
            Log.i(TAG, "Setting users empty layout visisble");
            hideUsersThreadsUi();
        } else {
            Log.i(TAG, "Setting users empty layout gone and recycler as visisbile");
            // add item decoration (divider) to recycler view and set its adapter and click callback
            mUsersThreadsRV.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            mUsersThreadsRV.setAdapter(new UsersThreadsAdapter(usersThreads, PlantPreferenceManager.filterProfanity(getActivity()),
                    position -> {
                        Thread thread = ((UsersThreadsAdapter) mUsersThreadsRV.getAdapter()).getItemAtPos(position);
                        mPresenter.openThreadDetailsActivity(thread);
                    }));
            showUsersThreadsUi();
        }
    }

    @Override
    public void showThreadDetailsUi(@NonNull String threadId) {
        Intent intent = new Intent(getActivity(), ThreadDetailActivity.class);
        intent.putExtra(ThreadDetailActivity.EXTRA_THREAD_ID, threadId);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    private void showUsersThreadsUi() {
        mUserNoThreadsTV.setVisibility(View.GONE);
        mUsersThreadsRV.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingThreadsError() {
        mUserNoThreadsTV.setText(R.string.error_loading_threads_user_profile);
        mUserNoThreadsTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideUsersThreadsUi() {
        Log.i(TAG, "*** Hididng threads UI");
        mUsersThreadsRV.setVisibility(View.GONE);
        mUserNoThreadsTV.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean adapterIsNotCreated() {
        return mUsersThreadsRV.getAdapter() == null;
    }

    @Override
    public void updateThreadsUi(@NonNull List<Thread> threads) {
        UsersThreadsAdapter adapter = ((UsersThreadsAdapter) mUsersThreadsRV.getAdapter());
        adapter.updateThreadsList(threads);
        showUsersThreadsUi();
    }

    @Override
    public void showUserActionsPanel() {
        setProfileActionControlsVisibility(true);
    }

    @Override
    public void hideUserActionsPanel() {
        setProfileActionControlsVisibility(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mShouldExecuteOnResume) {
            mPresenter.getUsersThreads();
        } else {
            mShouldExecuteOnResume = true;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_view_sign_in: // sign in button clicked
                if (mLoginBtnTV.getText().toString().equalsIgnoreCase(getString(R.string.home_sign_in))) {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class), REQUEST_CODE_LOGIN);
                } else {
                    mPresenter.signUserOut();
                }
                break;
            case R.id.rl_view_matched: // matched history button clicked
                if (mPresenter.canOpenHistory(mMatchedValueTV.getText().toString())) {
                    Intent matchedIntent = new Intent(getActivity(), HistoryActivity.class);
                    matchedIntent.putExtra(EXTRA_KEY_HISTORY_TYPE, HistoryActivity.TYPE_MATCHED);
                    startActivityForResult(matchedIntent, REQUEST_CODE_HISTORY);
                }
                break;
            case R.id.rl_view_unmatched: // unmatched history button clicked
                if (mPresenter.canOpenHistory(mUnmatchedValueTV.getText().toString())) {
                    Intent unmatchedIntent = new Intent(getActivity(), HistoryActivity.class);
                    unmatchedIntent.putExtra(EXTRA_KEY_HISTORY_TYPE, HistoryActivity.TYPE_UNMATCHED);
                    startActivityForResult(unmatchedIntent, REQUEST_CODE_HISTORY);
                }
                break;
            case R.id.ib_delete_user: // delete user profile button clicked
                if (PlantPreferenceManager.showDeleteConfirmation(getActivity())) {
                    ConfirmationDialog dialog = ConfirmationDialog.newInstance(getString(R.string.dialog_title_delete_profile), getString(R.string.dialog_msg_delete_profile));
                    dialog.setListener(this);
                    dialog.show(getActivity().getSupportFragmentManager(), "DELETE_PROFILE");
                } else {
                    mPresenter.deleteUser();
                }
                break;
            case R.id.ib_edit_user: // edit user profile button clicked
                startActivityForResult(new Intent(getActivity(), EditUserProfileActivity.class), REQUEST_CODE_EDIT_USER_PROFILE);
                break;
        }
        getActivity().overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_LOGIN || requestCode == REQUEST_CODE_EDIT_USER_PROFILE) {
            mPresenter.getUserDetails();
        } else if (requestCode == REQUEST_CODE_HISTORY
                || requestCode == MainNavigationActivity.REQUEST_CODE_GALLERY_IMAGE
                || requestCode == MainNavigationActivity.REQUEST_CODE_CAMERA_IMAGE) {
            // only update user history if history items have been deleted
            if (data.hasExtra(HistoryActivity.EXTRA_ITEMS_DELETED)
                    && data.getBooleanExtra(HistoryActivity.EXTRA_ITEMS_DELETED, false)) {
                mPresenter.updateHistoryData();
            }
        }
    }

    @Override
    public void onConfirmationDialogConfirmClicked() {
        mPresenter.deleteUser();
    }
}
