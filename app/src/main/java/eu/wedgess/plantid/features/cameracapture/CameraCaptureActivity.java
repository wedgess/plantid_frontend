/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.cameracapture;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.common.base.Strings;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.utils.FileUtils;
import eu.wedgess.plantid.utils.OnRevealAnimationListener;
import eu.wedgess.plantid.utils.UiUtils;

public class CameraCaptureActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = CameraCaptureActivity.class.getSimpleName();
    public static final String EXTRA_RESULT_IMG_DATA = "EXTRA_RESULT_IMG_DATA";

    //region View binding
    @BindView(R.id.rl_camera_container)
    protected LinearLayout mRlContainer;
    @BindView(R.id.rl_controls_container)
    protected RelativeLayout mRlControlsContainer;
    @BindView(R.id.ll_camera_container)
    protected LinearLayout mLlContainer;
    @BindView(R.id.cv_camera_preview)
    protected CameraView mCameraView;
    @BindView(R.id.fab_capture_img)
    protected FloatingActionButton mCaptureImageBTN;
    @BindView(R.id.fab_cancel_capture)
    protected FloatingActionButton mCancelCaptureBTN;
    @BindView(R.id.fab_exit)
    protected FloatingActionButton mExitCaptureBTN;
    private String mCapturedImgLocation;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_capture);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            setupEnterAnimation();
            setupExitAnimation();
        } else {
            initViews();
        }

        mCaptureImageBTN.setOnClickListener(this);
        mCancelCaptureBTN.setOnClickListener(this);
        mExitCaptureBTN.setOnClickListener(this);

        // when picture is taken, freeze camera view and and display done icon
        mCameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] picture) {
                mCameraView.stop();
                changeFabIcon(true);
                // Create a bitmap or a file...
                // CameraUtils will read EXIF orientation for you, in a worker thread.
                CameraUtils.decodeBitmap(picture, 300, 300, new CameraUtils.BitmapCallback() {
                    @Override
                    public void onBitmapReady(Bitmap bitmap) {
                        mCapturedImgLocation = FileUtils.storeImage(bitmap);
                    }
                });
            }
        });


    }

    private void initViews() {
        new Handler(Looper.getMainLooper()).post(() -> {
            Animation animation = AnimationUtils.loadAnimation(CameraCaptureActivity.this, android.R.anim.fade_in);
            animation.setDuration(300);
            mLlContainer.startAnimation(animation);
            //mIvClose.startAnimation(animation);
            mLlContainer.setVisibility(View.VISIBLE);
            mLlContainer.setBackgroundColor(Color.BLACK);
            mRlControlsContainer.setBackgroundColor(Color.BLACK);
            mExitCaptureBTN.setVisibility(View.VISIBLE);
        });
    }

    private void changeFabIcon(final boolean doneIconVisible) {
        final int[] ids = new int[2];
        if (doneIconVisible) {
            ids[0] = R.drawable.ic_photo_camera;
            ids[1] = R.drawable.ic_done_white;
        } else {
            ids[0] = R.drawable.ic_done_white;
            ids[1] = R.drawable.ic_photo_camera;
        }
        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, ids.length - 1).setDuration(0);
        valueAnimator.setInterpolator(new LinearInterpolator() /*your TimeInterpolator*/);
        valueAnimator.addUpdateListener(animation -> {
            int animatedValue = (int) animation.getAnimatedValue();
            if (animatedValue == 1) {
                CameraCaptureActivity.this.runOnUiThread(() -> {
                    int animatedValue1 = (int) animation.getAnimatedValue();
                    mCaptureImageBTN.setImageDrawable(getResources().getDrawable(ids[animatedValue1]));
                    mCancelCaptureBTN.setVisibility(doneIconVisible ? View.VISIBLE : View.INVISIBLE);
                });
            }
        });
        valueAnimator.start();
    }

    @Override
    public void onBackPressed() {
        UiUtils.animateRevealHide(this, mCaptureImageBTN, mRlContainer, R.color.colorAccent, mCaptureImageBTN.getWidth() / 2,
                new OnRevealAnimationListener() {
                    @Override
                    public void onRevealHide() {
                        backPressed();
                        // set status bar color
                        getWindow().setStatusBarColor(
                                ContextCompat.getColor(CameraCaptureActivity.this, R.color.colorPrimaryDark));
                    }

                    @Override
                    public void onRevealShow() {

                    }
                });
    }

    private void backPressed() {
        super.onBackPressed();
    }

    private void setupExitAnimation() {
        Fade fade = new Fade();
        getWindow().setReturnTransition(fade);
        fade.setDuration(100);
    }


    private void setupEnterAnimation() {
        Transition transition = TransitionInflater.from(this).inflateTransition(R.transition.change_bound_with_arc);
        transition.setDuration(300);
        getWindow().setSharedElementEnterTransition(transition);
        transition.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {
            }

            @Override
            public void onTransitionEnd(Transition transition) {
                animateRevealShow(mRlContainer);
            }

            @Override
            public void onTransitionCancel(Transition transition) {
            }

            @Override
            public void onTransitionPause(Transition transition) {
            }

            @Override
            public void onTransitionResume(Transition transition) {
            }
        });
    }

    private void animateRevealShow(final View viewRoot) {
        int cx = (viewRoot.getLeft() + viewRoot.getRight()) / 2;
        int cy = (viewRoot.getTop() + viewRoot.getBottom()) - (int) mCaptureImageBTN.getY();
        UiUtils.animateRevealShow(this, mRlContainer, mCaptureImageBTN.getWidth() / 2, R.color.colorAccent,
                cx, cy, new OnRevealAnimationListener() {
                    @Override
                    public void onRevealHide() {

                    }

                    @Override
                    public void onRevealShow() {
                        initViews();
                    }
                });
        new Handler().postDelayed(() -> {
            // set status bar color
            getWindow().setStatusBarColor(
                    ContextCompat.getColor(CameraCaptureActivity.this, android.R.color.black));
        }, 600);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab_capture_img) {
            // when the cancel capture button is visible a image has been captured, otherwise capture image
            if (mCancelCaptureBTN.getVisibility() == View.VISIBLE) {
                // delete image from the devices storage on capture as not to block users gallery
                if (!Strings.isNullOrEmpty(mCapturedImgLocation)) {
                    removeImageFromStorage();
                    sendImagePathResult();
                } else {
                    finish();
                }
            } else {
                mCameraView.capturePicture();
            }
        } else if (view.getId() == R.id.fab_cancel_capture) {
            if (!Strings.isNullOrEmpty(mCapturedImgLocation)) {
                File capturedImg = new File(mCapturedImgLocation);
                if (capturedImg.exists()) {
                    // delete image and from media scanner
                    removeImageFromStorage();
                }
                mCapturedImgLocation = null;
            }
            mCancelCaptureBTN.setVisibility(View.INVISIBLE);
            mCameraView.start();
            changeFabIcon(false);
        } else if (view.getId() == R.id.fab_exit) {
            onBackPressed();
        }
    }

    private void sendImagePathResult() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_RESULT_IMG_DATA, mCapturedImgLocation);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void removeImageFromStorage() {
        if (!Strings.isNullOrEmpty(mCapturedImgLocation)) {
            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{mCapturedImgLocation},
                    null, new MediaScannerConnection.MediaScannerConnectionClient() {
                        @Override
                        public void onMediaScannerConnected() {
                            // Do nothing
                        }

                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            getContentResolver().delete(uri, null, null);
                        }
                    });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraView.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCameraView.destroy();
    }
}
