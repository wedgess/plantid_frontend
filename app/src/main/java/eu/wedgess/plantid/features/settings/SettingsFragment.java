/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.settings;

import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.utils.PlantPreferenceManager;

/**
 * Settings fragment
 */
public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.settings);
//        Preference locationPreference = findPreference(PlantPreferenceManager.PREF_HIDE_LOCATION);
//        if (locationPreference != null) {
//            locationPreference.setOnPreferenceChangeListener(this);
//        }
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // set BG color of preferences back to default - as app overrides default BG colour in styles
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(getResources().getColor(android.R.color.background_light));

        return view;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
//        if (preference.getKey().equals(PlantPreferenceManager.PREF_HIDE_LOCATION)) {
//            preference.setSummary(((boolean) newValue) ? "Location is hidden from other users when posting threads." : "Location will be shown to other users when posting threads.");
//            return true;
//        }
        return false;
    }
}
