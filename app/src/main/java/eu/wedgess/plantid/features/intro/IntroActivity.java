package eu.wedgess.plantid.features.intro;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntro2Fragment;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.features.mainnavigation.MainNavigationActivity;
import eu.wedgess.plantid.utils.PlantPreferenceManager;

/**
 * Intro activity, which displays four slides.
 * This is using the {@link AppIntro2} library (https://github.com/apl-devs/AppIntro)
 * <p>
 * Created by gar on 07/12/17.
 */

public class IntroActivity extends AppIntro2 {

    public static final String EXTRA_SHOW_HELP_ALERT = "EXTRA_SHOW_HELP_ALERT";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // add all the slides
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.slide_welcome_title), getString(R.string.slide_welcome_message), R.drawable.plant_slide, ContextCompat.getColor(this, R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.slide_community_title), getString(R.string.slide_community_message), R.drawable.community_slide, ContextCompat.getColor(this, R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.slide_permissions_title), getString(R.string.slide_permissions_message), R.drawable.slide_camera, ContextCompat.getColor(this, R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.slide_account_title), getString(R.string.slide_account_message), R.drawable.app_icon_slide, ContextCompat.getColor(this, R.color.colorPrimary)));

        // ask for permissions on slide 3
        askForPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 3);

        // set status and nav bar colour
        setBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        showSkipButton(false);
        setProgressButtonEnabled(true);
        setVibrate(false);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // When done set preference to false so intro isn't shown again, start main activity and finish this one
        PlantPreferenceManager.setShowUserIntro(false, this);
        Intent intnent = new Intent(this, MainNavigationActivity.class);
        // set extra to show Help dialog
        intnent.putExtra(EXTRA_SHOW_HELP_ALERT, true);
        startActivity(intnent);
        finish();
        overridePendingTransition(R.anim.activity_stay, R.anim.activity_slide_down);
    }
}
