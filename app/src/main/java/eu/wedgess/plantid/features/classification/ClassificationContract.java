/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.classification;

import java.util.List;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;
import eu.wedgess.plantid.data.Classifier;
import eu.wedgess.plantid.data.Plant;

/**
 * The contract between {@link ClassificationActivity} which is the view and
 * {@link ClassificationPresenter} which handles the presentation for the view.
 * <p>
 * Created by gar on 07/12/17.
 */
class ClassificationContract {

    interface View extends BaseView<ClassificationPresenter> {
        // void setPotentialMatchedPlantsData(List<Plant> plants);
        void setClassificationData(List<Classifier.Recognition> classifiers);

        void showPlantDetailsUi(String plantId);

        void setLoadingIndicator(boolean active);

        void showNoMatchesView();

        void showLoadingMatchesError(String error);

        boolean isActive();

        void hideMatchAndCreateThreadButtons();
    }

    interface Presenter extends BasePresenter {

        void openPlantDetailsActivity(Plant plant);

        void startClassification(String location, String imgPath);

        void updateHistoryItem(String plantId);

        String getHistoryItemImgUrl();
    }

}
