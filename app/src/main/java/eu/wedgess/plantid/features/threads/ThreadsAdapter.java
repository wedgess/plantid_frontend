/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threads;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.DateUtility;
import eu.wedgess.plantid.utils.ProfanityFilter;
import eu.wedgess.plantid.utils.ThreadsDiffCallback;

/**
 * Adapter for displaying thread items inside {@link ThreadsFragment}
 * <p>
 * Created by gar on 31/10/17.
 */

public class ThreadsAdapter extends RecyclerView.Adapter<ThreadsAdapter.ViewHolder> {

    public static final String TAG = ThreadsAdapter.class.getSimpleName();

    public interface RecyclerClickListener {
        void onRecyclerItemClick(int position);
    }

    private List<Thread> mThreadsList;
    private RecyclerClickListener mRecyclerClickListener;
    private boolean mFilterProfanity;

    ThreadsAdapter(List<Thread> threadList, boolean filterProfanity, RecyclerClickListener recyclerClickListener) {
        this.mThreadsList = threadList;
        this.mRecyclerClickListener = recyclerClickListener;
        this.mFilterProfanity = filterProfanity;
    }


    // Create new views (invoked by the layout manager)
    @SuppressLint("InflateParams")
    @Override
    public ThreadsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_thread, null), mRecyclerClickListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ThreadsAdapter.ViewHolder viewHolder, int position) {
        Thread thread = mThreadsList.get(position);
        viewHolder.setData(thread, mFilterProfanity);
    }

    @Override
    public void onBindViewHolder(ThreadsAdapter.ViewHolder holder, int position, List<Object> payloads) {
        // Diff util uses this to change indiviual views rather than redrawing the whole view
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            Bundle o = (Bundle) payloads.get(0);
            for (String key : o.keySet()) {
                if (key.equals(ThreadsDiffCallback.BUNDLE_THREAD_TITLE)) {
                    holder.tvThreadTitle.setText(mThreadsList.get(position).getTitle());
                }
                if (key.equals(ThreadsDiffCallback.BUNDLE_THREAD_MSG)) {
                    holder.tvThreadMessage.setText(mThreadsList.get(position).getMessage());
                }
                if (key.equals(ThreadsDiffCallback.BUNDLE_THREAD_SOLVED)) {
                    holder.setSolvedUi(mThreadsList.get(position));
                }
                if (key.equals(ThreadsDiffCallback.BUNDLE_THREAD_COMMENT_COUNT)) {
                    holder.tvThreadCommentCount.setText(String.valueOf(mThreadsList.get(position).getCommentCount()));
                }
            }
        }
    }

    void updateThreadsList(List<Thread> newThreadList) {
        // TODO: Possibly do this in presenter to run this operation on computational thread
        List<Thread> copy = new ArrayList(this.mThreadsList);
        final ThreadsDiffCallback diffCallback = new ThreadsDiffCallback(copy, newThreadList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.mThreadsList.clear();
        this.mThreadsList.addAll(newThreadList);
        diffResult.dispatchUpdatesTo(this);
    }

    public Thread getItemAtPos(int pos) {
        return this.mThreadsList.get(pos);
    }

    ArrayList<Thread> toList() {
        return new ArrayList<>(this.mThreadsList);
    }

    // Return the size of your mThreadsList (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mThreadsList.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView threadImage;
        TextView tvThreadTitle, tvThreadAuthor, tvThreadAuthorLocation,
                tvThreadMessage, tvThreadDate, tvThreadCommentCount, tvThreadSolved;

        ViewHolder(View itemLayoutView, final RecyclerClickListener listener) {
            super(itemLayoutView);
            tvThreadAuthor = itemLayoutView.findViewById(R.id.tv_thread_author);
            tvThreadTitle = itemLayoutView.findViewById(R.id.tv_thread_title);
            tvThreadAuthorLocation = itemLayoutView.findViewById(R.id.tv_thread_location);
            tvThreadMessage = itemLayoutView.findViewById(R.id.tv_thread_message);
            tvThreadDate = itemLayoutView.findViewById(R.id.tv_thread_date);
            tvThreadCommentCount = itemLayoutView.findViewById(R.id.tv_thread_comments);
            tvThreadSolved = itemLayoutView.findViewById(R.id.tv_thread_solved);
            threadImage = itemLayoutView.findViewById(R.id.iv_thread_img);
            itemLayoutView.setOnClickListener(v -> listener.onRecyclerItemClick(getAdapterPosition()));
        }

        // set the items data to viewholder views
        void setData(Thread data, boolean filterProfanity) {
            Log.i(TAG, data.toString());
            tvThreadAuthor.setText(data.getAuthor().getUserName());
            tvThreadTitle.setText(filterProfanity ? ProfanityFilter.filter(data.getTitle()) : data.getTitle());
            tvThreadMessage.setText(filterProfanity ? ProfanityFilter.filter(data.getMessage()) : data.getMessage());
            tvThreadAuthorLocation.setText(Strings.isNullOrEmpty(data.getLocation()) ? "Unknown" : data.getLocation());
            tvThreadCommentCount.setText(String.valueOf(data.getCommentCount()));
            String prettyDate = data.getDateCreated();
            if (!Strings.isNullOrEmpty(prettyDate)) {
                try {
                    prettyDate = DateUtility.getRelativeTime(data.getDateCreated());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                tvThreadDate.setText(prettyDate);
            }

            setSolvedUi(data);

            // TODO: Can remove replace(..) in production code
            GlideImageHelper.glideLoadCircleImage(threadImage, data.getThreadImgUrl().replace("localhost", "10.0.2.2"));
        }

        private void setSolvedUi(Thread data) {
            Drawable drawable;
            if (data.isSolved()) {
                tvThreadSolved.setText(R.string.thread_solved);
                drawable = ContextCompat.getDrawable(tvThreadSolved.getContext(), R.drawable.ic_matched);
            } else {
                tvThreadSolved.setText(R.string.thread_unsolved);
                drawable = ContextCompat.getDrawable(tvThreadSolved.getContext(), R.drawable.ic_unmatched);

            }
            tvThreadSolved.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        }
    }
}
