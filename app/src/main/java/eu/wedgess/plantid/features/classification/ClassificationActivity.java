/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.classification;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Classifier;
import eu.wedgess.plantid.data.source.local.ClassificationLocalDataSource;
import eu.wedgess.plantid.data.source.local.HistoryLocalDataSource;
import eu.wedgess.plantid.data.source.local.PlantsLocalDataSource;
import eu.wedgess.plantid.data.source.remote.ClassificationRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.HistoryRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.PlantsRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.ClassificationRepository;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import eu.wedgess.plantid.features.createeditthread.CreateEditThreadActivity;
import eu.wedgess.plantid.features.plantdetails.PlantDetailActivity;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.LocationUtils;

/**
 * Classification activity is displayed when an image is captured or chosen from the gallery via
 * {@link eu.wedgess.plantid.features.mainnavigation.MainNavigationActivity}.
 */

public class ClassificationActivity extends BaseActivity implements ClassificationContract.View, View.OnClickListener {

    public static final String TAG = ClassificationActivity.class.getSimpleName();

    public static final String EXTRA_KEY_IMG_URL = "EXTRA_KEY_IMG_URL";
    public static final String EXTRA_KEY_IMG_FILE_PATH = "EXTRA_KEY_IMG_FILE_PATH";
    private static final String STATE_KEY_IS_MATCHED = "STATE_KEY_IS_MATCHED";
    private static final String STATE_KEY_LIST = "STATE_KEY_LIST";
    private static final int RESULT_CODE_CREATE_THREAD = 212;

    //region View binding
    @BindView(R.id.iv_identification_image)
    ImageView mPlantIdImageIV;
    @BindView(R.id.rv_identification_matches)
    RecyclerView mClassificatioMatchesRV;
    @BindView(R.id.pb_loading_identification)
    protected ProgressBar mClassifyingPB;
    @BindView(R.id.tv_no_identification_matches)
    protected TextView mNoClassificationMatchesTV;
    @BindView(R.id.fab_create_thread)
    FloatingActionButton mCreateThreadFAB;
    //endregion

    private ClassificationContract.Presenter mPresenter;
    private boolean mShowMatchButtons = true;


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // saved show match button and adapter list state on config change
        if (mClassificatioMatchesRV.getAdapter() != null) {
            outState.putBoolean(STATE_KEY_IS_MATCHED, mShowMatchButtons);
            outState.putSerializable(STATE_KEY_LIST, ((ClassificationAdapter) mClassificatioMatchesRV.getAdapter()).toList());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // restore show match button state, adapter list is handles in #onCreate
        if (savedInstanceState.containsKey(STATE_KEY_IS_MATCHED)) {
            mShowMatchButtons = savedInstanceState.getBoolean(STATE_KEY_IS_MATCHED);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_classification);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // initialize the presenter
        AppDatabase appDatabase = AppDatabase.getInstance(ClassificationActivity.this);
        HistoryRepository historyRepository = HistoryRepository.getInstance(
                new HistoryLocalDataSource(appDatabase.historyDao()),
                new HistoryRemoteDataSource());
        PlantsRepository plantsRepository = PlantsRepository.getInstance(
                new PlantsLocalDataSource(appDatabase.plantDao()),
                new PlantsRemoteDataSource());
        new ClassificationPresenter(this, ClassificationRepository.getInstance(
                new ClassificationLocalDataSource(getAssets()),
                new ClassificationRemoteDataSource()), plantsRepository, historyRepository);

        mCreateThreadFAB.setOnClickListener(this);

        // get image path from intent
        if (getIntent().hasExtra(EXTRA_KEY_IMG_FILE_PATH)) {
            // always load the image into its view, even on config change
            GlideImageHelper.glideLoadImage(mPlantIdImageIV, getIntent().getStringExtra(EXTRA_KEY_IMG_FILE_PATH));
            // start classification if state is not null
            if (savedInstanceState == null) {
                mPresenter.startClassification(LocationUtils.getLocationName(ClassificationActivity.this),
                        getIntent().getStringExtra(EXTRA_KEY_IMG_FILE_PATH));
            } else {
                // otherwise check that the list is in the state bundle, if so load the list back into its view
                if (savedInstanceState.containsKey(STATE_KEY_LIST)) {
                    ArrayList<Classifier.Recognition> list = (ArrayList<Classifier.Recognition>) savedInstanceState.getSerializable(STATE_KEY_LIST);
                    setClassificationData(list);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull ClassificationPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setClassificationData(List<Classifier.Recognition> classifiers) {
        Log.i(TAG, "Called activity set Classifier Data: " + classifiers.size());
        // if already matched don;t show create thread FAb on orientation change
        if (!mShowMatchButtons) {
            mCreateThreadFAB.setVisibility(View.GONE);
        }
        if (classifiers.isEmpty()) {
            mClassificatioMatchesRV.setVisibility(View.GONE);
            mNoClassificationMatchesTV.setVisibility(View.VISIBLE);
        } else {
            mClassificatioMatchesRV.setVisibility(View.VISIBLE);
            mNoClassificationMatchesTV.setVisibility(View.GONE);
            mClassificatioMatchesRV.setAdapter(new ClassificationAdapter(mShowMatchButtons, classifiers, new ClassificationAdapter.RecyclerClickListener() {
                @Override
                public void onRecyclerItemClick(int position) {
                    mPresenter.openPlantDetailsActivity((((ClassificationAdapter) mClassificatioMatchesRV.getAdapter()).getItemAtPos(position).getPlantInfo()));
                }

                @Override
                public void onMatchButtonClicked(int position) {
                    ClassificationAdapter adapter = ((ClassificationAdapter) mClassificatioMatchesRV.getAdapter());
                    Classifier.Recognition classificationResult = adapter.getItemAtPos(position);
                    mPresenter.updateHistoryItem(classificationResult.getPlantId());
                    mShowMatchButtons = false;
                    adapter.toggleMatchButtonVisibility(mShowMatchButtons);
                    mCreateThreadFAB.setVisibility(View.GONE);
                }
            }));
        }
    }

    @Override
    public void showPlantDetailsUi(String plantId) {
        Intent intent = new Intent(ClassificationActivity.this, PlantDetailActivity.class);
        intent.putExtra(PlantDetailActivity.EXTRA_PLANT_ID, plantId);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }


    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mClassifyingPB.setVisibility(View.VISIBLE);
        } else {
            mClassifyingPB.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNoMatchesView() {
        mClassifyingPB.setVisibility(View.GONE);
        mNoClassificationMatchesTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingMatchesError(String error) {
        mClassifyingPB.setVisibility(View.GONE);
        mNoClassificationMatchesTV.setVisibility(View.VISIBLE);
        mNoClassificationMatchesTV.setText(error);
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void hideMatchAndCreateThreadButtons() {
        mCreateThreadFAB.setVisibility(View.GONE);
        ClassificationAdapter adapter = ((ClassificationAdapter) mClassificatioMatchesRV.getAdapter());
        if (adapter != null) {
            adapter.toggleMatchButtonVisibility(false);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab_create_thread) {
            // start created thread with the image to be classified's path
            startCreateThreadActivity();
        }
    }

    private void startCreateThreadActivity() {
        Intent intent = new Intent(ClassificationActivity.this, CreateEditThreadActivity.class);
        if (getIntent().hasExtra(EXTRA_KEY_IMG_FILE_PATH)) {
            if (mPresenter.getHistoryItemImgUrl() == null) {
                intent.putExtra(EXTRA_KEY_IMG_FILE_PATH, getIntent().getStringExtra(EXTRA_KEY_IMG_FILE_PATH));
            } else {
                intent.putExtra(EXTRA_KEY_IMG_URL, mPresenter.getHistoryItemImgUrl());
            }
        }
        startActivityForResult(intent, RESULT_CODE_CREATE_THREAD);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_CODE_CREATE_THREAD) {
            if (resultCode == RESULT_OK) {
                finish();
            } else {
                Log.e(TAG, "Error receiving result from creating thread");
            }
        }
    }
}
