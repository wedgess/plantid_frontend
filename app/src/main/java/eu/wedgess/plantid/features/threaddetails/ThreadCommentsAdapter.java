/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threaddetails;

import android.annotation.SuppressLint;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.CommentsDiffCallback;
import eu.wedgess.plantid.utils.DateUtility;
import eu.wedgess.plantid.utils.ProfanityFilter;

/**
 * Adapter class for displaying comment items in {@link ThreadDetailActivity}
 * <p>
 * Created by gar on 31/10/17.
 */
public class ThreadCommentsAdapter extends RecyclerView.Adapter<ThreadCommentsAdapter.ViewHolder> {

    // recycler item callback
    public interface RecyclerClickListener {
        void onCommentAnswerAccepted(int position);

        void onEditCommentClicked(int position);

        void onDeleteCommentClicked(int position);
    }

    public static final String TAG = ThreadCommentsAdapter.class.getSimpleName();

    private List<Comment> mCommentsList;
    private RecyclerClickListener mRecyclerClickListener;
    private boolean mFilterProfanity;
    private boolean isThreadAuthor;
    private boolean mThreadSolved;

    ThreadCommentsAdapter(List<Comment> commentList, boolean isThreadAuthor, boolean isSolved,
                          boolean filterProfanity, RecyclerClickListener recyclerClickListener) {
        this.mCommentsList = commentList;
        this.isThreadAuthor = isThreadAuthor;
        this.mThreadSolved = isSolved;
        this.mFilterProfanity = filterProfanity;
        this.mRecyclerClickListener = recyclerClickListener;
    }

    // Create new views (invoked by the layout manager)
    @SuppressLint("InflateParams")
    @Override
    public ThreadCommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_comment, null), mRecyclerClickListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Comment comment = mCommentsList.get(position);
        viewHolder.setData(comment, isThreadAuthor, mThreadSolved, mFilterProfanity);
    }

    public Comment getItemAtPos(int pos) {
        return this.mCommentsList.get(pos);
    }

    void updateCommentsList(List<Comment> commentsList) {
        // TODO: Possibly do this in presenter to run this operation on computational thread
        final CommentsDiffCallback diffCallback = new CommentsDiffCallback(this.mCommentsList, commentsList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.mCommentsList.clear();
        this.mCommentsList.addAll(commentsList);
        diffResult.dispatchUpdatesTo(this);
    }

    void removeCommentItem(int position) {
        if (mCommentsList != null && !mCommentsList.isEmpty()) {
            removeAt(position);
        }
    }

    private void removeAt(int position) {
        mCommentsList.remove(position);
        notifyItemRemoved(position);
        // update all items below
        notifyItemRangeChanged(position, mCommentsList.size());
    }

    public void updateComment(int position, String updatedMsg) {
        if (mCommentsList != null && !mCommentsList.isEmpty()) {
            Comment comment = mCommentsList.get(position);
            if (comment != null) {
                comment.setMessage(updatedMsg);
                mCommentsList.set(position, comment);
                notifyItemChanged(position);
            }
        }
    }

    Comment getCommentItem(int position) {
        return mCommentsList.get(position);
    }

    // Return the size of your mCommentsList (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mCommentsList.size();
    }

    // used when a comment has been accepted as an answer
    public void setThreadSolved(boolean threadSolved) {
        this.mThreadSolved = threadSolved;
    }

    // inner class to hold a reference to each item of RecyclerView
    static class ViewHolder extends RecyclerView.ViewHolder {

        final RelativeLayout rlAuthorPanel;
        final ImageView ivAuthorProfileImage;
        final ImageButton ibAcceptAnswerTicks, ibEditComment, ibDeleteComment;
        final View answerDivider, commentAuthorDivider;
        final TextView tvCommentAuthor, tvThreadMessage, tvThreadDate, tvCommentIsAnswer;

        ViewHolder(View itemLayoutView, final RecyclerClickListener listener) {
            super(itemLayoutView);
            rlAuthorPanel = itemLayoutView.findViewById(R.id.rl_comment_author_panel);
            tvCommentIsAnswer = itemLayoutView.findViewById(R.id.tv_comment_answer);
            ibAcceptAnswerTicks = itemLayoutView.findViewById(R.id.ib_author_accept_answer);
            ibEditComment = itemLayoutView.findViewById(R.id.ib_edit_comment);
            ibDeleteComment = itemLayoutView.findViewById(R.id.ib_delete_comment);
            answerDivider = itemLayoutView.findViewById(R.id.answer_divider_vertical);
            commentAuthorDivider = itemLayoutView.findViewById(R.id.author_panel_divider);
            tvCommentAuthor = itemLayoutView.findViewById(R.id.tv_comment_author);
            tvThreadMessage = itemLayoutView.findViewById(R.id.tv_comment_message);
            tvThreadDate = itemLayoutView.findViewById(R.id.tv_comment_date);
            ivAuthorProfileImage = itemLayoutView.findViewById(R.id.iv_author_img);
            ibAcceptAnswerTicks.setOnClickListener(view -> {
                answerDivider.setVisibility(View.VISIBLE);
                tvCommentIsAnswer.setVisibility(View.VISIBLE);
                ibAcceptAnswerTicks.setVisibility(View.GONE);
                listener.onCommentAnswerAccepted(getAdapterPosition());
            });

            ibDeleteComment.setOnClickListener(view -> listener.onDeleteCommentClicked(getAdapterPosition()));
            ibEditComment.setOnClickListener(view -> listener.onEditCommentClicked(getAdapterPosition()));
        }

        public void setData(Comment data, boolean isThreadAuthor, boolean threadSolved, boolean filterProfanity) {
            Log.i(TAG, data.toString());
            if (data.isAnswer()) {
                tvCommentIsAnswer.setVisibility(View.VISIBLE);
                answerDivider.setVisibility(View.VISIBLE);
            } else {
                tvCommentIsAnswer.setVisibility(View.GONE);
                answerDivider.setVisibility(View.GONE);
            }

            User currentuser = CurrentUserInstance.getInstance().getCurrentUser();
            if (!threadSolved && currentuser != null && data.getAuthorId().equals(currentuser.getUid())) {
                commentAuthorDivider.setVisibility(View.VISIBLE);
                rlAuthorPanel.setVisibility(View.VISIBLE);
            } else {
                commentAuthorDivider.setVisibility(View.GONE);
                rlAuthorPanel.setVisibility(View.GONE);
            }

            // show option to select answer if user is thread author and thread is not already marked as solved
            ibAcceptAnswerTicks.setVisibility(isThreadAuthor && !threadSolved ? View.VISIBLE : View.GONE);

            tvCommentAuthor.setText(data.getAuthor().getUserName());
            tvThreadMessage.setText(filterProfanity ? ProfanityFilter.filter(data.getMessage()) : data.getMessage());
            String prettyDate = data.getDateCreated();
            try {
                prettyDate = DateUtility.getRelativeTime(data.getDateCreated());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tvThreadDate.setText(prettyDate);
            GlideImageHelper.glideLoadCircleImage(ivAuthorProfileImage, data.getAuthor().getImgUrl());
        }

    }
}
