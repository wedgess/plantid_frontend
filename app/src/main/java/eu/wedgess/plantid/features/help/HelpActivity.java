/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.help;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;

/**
 * A help activity which is launched from {@link eu.wedgess.plantid.features.settings.SettingsFragment}
 * The action must be set in res/xml/settings.xml and must match the action as defined in
 * AndroidManifest.xml.
 * <p>
 * Created: 02/03/2018
 */
public class HelpActivity extends BaseActivity implements View.OnClickListener {

    private static final String STATE_IDENTIFY_VISIBILITY = "STATE_IDENTIFY_VISIBILITY";
    private static final String STATE_RESULTS_VISIBILITY = "STATE_RESULTS_VISIBILITY";
    private static final String STATE_CREATE_THREAD_VISIBILITY = "STATE_CREATE_THREAD_VISIBILITY";
    private static final String STATE_SOLVED_THREAD_VISIBILITY = "STATE_SOLVED_THREAD_VISIBILITY";
    private static final String STATE_MY_DATA_VISIBILITY = "STATE_MY_DATA_VISIBILITY";

    //region View Binding
    @BindView(R.id.rl_identify_help_holder)
    RelativeLayout mIdentifyInstructionsRL;
    @BindView(R.id.ib_identify_expand)
    ImageButton mIdentifyExpandIB;
    @BindView(R.id.rl_classification_results_help_holder)
    RelativeLayout mResultsInstructionsRL;
    @BindView(R.id.ib_classification_results_expand)
    ImageButton mResultsExpandIB;
    @BindView(R.id.rl_solved_thread_help_holder)
    RelativeLayout mSolvedInstructionsRL;
    @BindView(R.id.ib_solved_thread_expand)
    ImageButton mSolvedExpandIB;
    @BindView(R.id.rl_creating_thread_help_holder)
    RelativeLayout mCreatingThreadInstructionsRL;
    @BindView(R.id.ib_creating_thread_expand)
    ImageButton mCreatingThreadExpandIB;
    @BindView(R.id.rl_my_data_help_holder)
    RelativeLayout mAccessDataInstructionsRL;
    @BindView(R.id.ib_my_data_expand)
    ImageButton mAccessDataExpandIB;
    //endregion

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_IDENTIFY_VISIBILITY, mIdentifyInstructionsRL.getVisibility());
        outState.putInt(STATE_RESULTS_VISIBILITY, mResultsInstructionsRL.getVisibility());
        outState.putInt(STATE_CREATE_THREAD_VISIBILITY, mCreatingThreadInstructionsRL.getVisibility());
        outState.putInt(STATE_SOLVED_THREAD_VISIBILITY, mSolvedInstructionsRL.getVisibility());
        outState.putInt(STATE_MY_DATA_VISIBILITY, mAccessDataInstructionsRL.getVisibility());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        setSupportActionBar(findViewById(R.id.toolbar_help));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(HelpActivity.this);

        if (savedInstanceState != null) {
            if (savedInstanceState.getInt(STATE_IDENTIFY_VISIBILITY) == View.VISIBLE) {
                mIdentifyInstructionsRL.setVisibility(View.VISIBLE);
                mIdentifyExpandIB.setRotation(-180f);
            }
            if (savedInstanceState.getInt(STATE_RESULTS_VISIBILITY) == View.VISIBLE) {
                mResultsInstructionsRL.setVisibility(View.VISIBLE);
                mResultsExpandIB.setRotation(-180f);
            }
            if (savedInstanceState.getInt(STATE_CREATE_THREAD_VISIBILITY) == View.VISIBLE) {
                mCreatingThreadInstructionsRL.setVisibility(View.VISIBLE);
                mCreatingThreadExpandIB.setRotation(-180f);
            }
            if (savedInstanceState.getInt(STATE_SOLVED_THREAD_VISIBILITY) == View.VISIBLE) {
                mSolvedInstructionsRL.setVisibility(View.VISIBLE);
                mSolvedExpandIB.setRotation(-180f);
            }
            if (savedInstanceState.getInt(STATE_MY_DATA_VISIBILITY) == View.VISIBLE) {
                mAccessDataInstructionsRL.setVisibility(View.VISIBLE);
                mAccessDataExpandIB.setRotation(-180f);
            }
        }

        mIdentifyExpandIB.setOnClickListener(this);
        mResultsExpandIB.setOnClickListener(this);
        mSolvedExpandIB.setOnClickListener(this);
        mCreatingThreadExpandIB.setOnClickListener(this);
        mAccessDataExpandIB.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_identify_expand:
                toggleExpandView(mIdentifyInstructionsRL, mIdentifyExpandIB);
                break;
            case R.id.ib_classification_results_expand:
                toggleExpandView(mResultsInstructionsRL, mResultsExpandIB);
                break;
            case R.id.ib_solved_thread_expand:
                toggleExpandView(mSolvedInstructionsRL, mSolvedExpandIB);
                break;
            case R.id.ib_creating_thread_expand:
                toggleExpandView(mCreatingThreadInstructionsRL, mCreatingThreadExpandIB);
                break;
            case R.id.ib_my_data_expand:
                toggleExpandView(mAccessDataInstructionsRL, mAccessDataExpandIB);
                break;
        }
    }

    private void toggleExpandView(RelativeLayout instructionLayout, ImageButton expandButton) {
        if (instructionLayout.getVisibility() == View.VISIBLE) {
            animateExpandButton(expandButton, View.GONE);
            instructionLayout.setVisibility(View.GONE);
        } else {
            animateExpandButton(expandButton, View.VISIBLE);
            instructionLayout.setVisibility(View.VISIBLE);
        }
    }

    private void animateExpandButton(ImageButton imageButton, int visibility) {
        imageButton.clearAnimation();
        imageButton.animate().rotation(visibility == View.GONE ? 0f : -180f).setDuration(200);
    }


}
