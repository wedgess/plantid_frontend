/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plantdetails;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

import java.util.LinkedHashMap;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;

/**
 * Contract between {@link PlantDetailActivity} and {@link PlantDetailPresenter}
 * <p>
 * Created by gar on 24/11/17.
 */

class PlantDetailContract {

    interface View extends BaseView<Presenter> {

        void showMissingPlant();

        void setPlantHeaderImage(String url);

        void setPlantNames(String botanicalName, String commonName);

        void setPlantDescription(String description);

        String[] getMonthsList();

        void setFlowersGridView(LinkedHashMap<String, Boolean> flowersTimeMap);

        void hideFlowersGridView();

        void setSowTimeGridView(LinkedHashMap<String, Boolean> sowTimeMap);

        void hideSowTimeGridView();

        void setPlantHeight(String height);

        void hidePlantHeight();

        void setPlantWidth(String width);

        void hidePlantWidth();

        void setPlantType(String type, @IdRes int drawable);

        void hidePlantType();

        void setFlowerColorGridView(String[] colors);

        void hideFlowerColorGridView();

        void setFoliageColorGridView(String[] colors);

        void hideFoliageColorGridView();

        void setPlantFeatures(String features);

        void hidePlantFeatures();

        void setPlantSunExposure(String sunExposure);

        void hidePlantSunExposure();

        void setPlantHardiness(String hardiness);

        void hidePlantHardiness();

        void setPlantSoilType(String hardiness);

        void hidePlantSoilType();

        void setPlantMaintenance(String maintenance);

        void hidePlantMaintenance();

        void setPlantTimeGridView(LinkedHashMap<String, Boolean> plantTimeMap);

        void hidePlantTimeGridView();

        void setPruneTimeGridView(LinkedHashMap<String, Boolean> pruneTimeMap);

        void hidePruneTimeGridView();

        void setCollectSeedTimeGridView(LinkedHashMap<String, Boolean> collectSeedTimeMap);

        void hideCollectSeedTimeGridView();

        void setAttractsBirdsActive(boolean active);

        void setAttractsBeesActive(boolean active);

        void setAttractsOtherPollinatorsActive(boolean active);

        void setAttractsMothsButterfliesActive(boolean active);

        void setAttractsInsectsActive(boolean active);

        void hideAttractsLayout();

        void setWildLifeFeatures(@NonNull String wildLifeFeatures);

        void setToxicityPersonActive(boolean active);

        void setToxicityCatActive(boolean active);

        void setToxicityDogActive(boolean active);

        void setToxicityHorseActive(boolean active);

        void setToxicityBirdsActive(boolean active);

        void setToxicityLivestockActive(boolean active);

        void hideToxicityLayout();

        void setEffects(String effects);
    }

    interface Presenter extends BasePresenter {

    }
}
