/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threads;

import android.animation.ValueAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.custom.ScrollChildSwipeRefreshLayout;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.local.CommentsLocalDataSource;
import eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource;
import eu.wedgess.plantid.data.source.local.UsersLocalDataSource;
import eu.wedgess.plantid.data.source.remote.CommentsRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.ThreadsRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.features.threaddetails.ThreadDetailActivity;
import eu.wedgess.plantid.utils.PlantPreferenceManager;
import eu.wedgess.plantid.utils.UiUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link Fragment} subclass which displays a list of threads Threads that can be filtered
 * or searched.
 */
public class ThreadsFragment extends Fragment implements ThreadsContract.View {

    private static final String TAG = ThreadsFragment.class.getSimpleName();

    private static final String STATE_KEY_CURRENT_FILTERING = "STATE_KEY_CURRENT_FILTERING";
    private static final String STATE_KEY_LIST = "STATE_KEY_LIST";
    private static final String STATE_KEY_CURRENT_SEARCH = "STATE_KEY_CURRENT_SEARCH";

    //region View binding
    @BindView(R.id.rv_community_threads)
    protected RecyclerView mThreadsRV;
    @BindView(R.id.pb_loading_community_threads)
    protected ProgressBar mLoadingPB;
    @BindView(R.id.tv_no_community_data)
    protected TextView mNoThreadDataTV;
    @BindView(R.id.sr_layout)
    protected ScrollChildSwipeRefreshLayout mSwipeRefreshLayout;
    //endregion

    private ThreadsContract.Presenter mPresenter;
    private Unbinder mUnbinder;
    private boolean mShouldExecuteOnResume;

    public ThreadsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static ThreadsFragment newInstance() {
        return new ThreadsFragment();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // save filtering, list and search on configuration change
        outState.putSerializable(STATE_KEY_CURRENT_FILTERING, mPresenter.getFiltering());
        if (!adapterIsNotCreated()) {
            outState.putSerializable(STATE_KEY_LIST, ((ThreadsAdapter) mThreadsRV.getAdapter()).toList());
        }
        outState.putString(STATE_KEY_CURRENT_SEARCH, mPresenter.getSearchFilterQuery());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mShouldExecuteOnResume = false;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_community, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);
        if (isAdded()) {
            // init presenter
            AppDatabase database = AppDatabase.getInstance(getActivity());
            new ThreadsPresenter(
                    ThreadsRepository.getInstance(
                            new ThreadsLocalDataSource(database.threadDao()),
                            new ThreadsRemoteDataSource()),
                    CommentsRepository.getInstance(
                            new CommentsLocalDataSource(database.commentDao()),
                            new CommentsRemoteDataSource()),
                    UserRepository.getInstance(
                            new UsersLocalDataSource(database.userDao(),
                                    PreferenceManager.getDefaultSharedPreferences(getActivity())),
                            new UsersRemoteDataSource()),
                    this);

            // set up swipe refresh layout
            mSwipeRefreshLayout.setColorSchemeColors(
                    ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                    ContextCompat.getColor(getActivity(), R.color.colorAccent),
                    ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
            );
            // Set the recycler view in the custom SwipeRefreshLayout.
            mSwipeRefreshLayout.setScrollUpChild(mThreadsRV);
            // refresh threads
            mSwipeRefreshLayout.setOnRefreshListener(() -> mPresenter.refreshThreads());
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // on first load subscribe to presenter to load threads
        if (savedInstanceState == null) {
            mPresenter.subscribe();
        } else {
            // on config changes reload the state
            if (savedInstanceState.containsKey(STATE_KEY_LIST)) {
                ArrayList<Thread> list = savedInstanceState.getParcelableArrayList(STATE_KEY_LIST);
                setThreadsData(list);
            }
            ThreadsFilterType currentFiltering = (ThreadsFilterType) savedInstanceState.getSerializable(STATE_KEY_CURRENT_FILTERING);
            mPresenter.setFiltering(currentFiltering);
            mPresenter.setSearchQuery(savedInstanceState.getString(STATE_KEY_CURRENT_SEARCH));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // clear menus from previous fragments
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.community, menu);
        initSearchView(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_filter_threads) {
            showFilteringPopUpMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showFilteringPopUpMenu() {
        PopupMenu popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.overflow));
        popup.getMenuInflater().inflate(R.menu.filter_threads, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.all_threads:
                    mPresenter.setFiltering(ThreadsFilterType.ALL_THREADS);
                    break;
                case R.id.unsolved_threads:
                    mPresenter.setFiltering(ThreadsFilterType.UNSOLVED_THREADS);
                    break;
                case R.id.solved_threads:
                    mPresenter.setFiltering(ThreadsFilterType.SOLVED_THREADS);
                    break;
                case R.id.no_commented_threads:
                    mPresenter.setFiltering(ThreadsFilterType.NO_COMMENTED_THREADS);
                    break;
                default:
                    mPresenter.setFiltering(ThreadsFilterType.ALL_THREADS);
                    break;
            }
            mPresenter.loadThreads(false);
            return true;
        });

        popup.show();
    }

    @Override
    public boolean adapterIsNotCreated() {
        return mThreadsRV.getAdapter() == null;
    }

    @Override
    public void updateThreadsUi(@NonNull List<Thread> threads) {
        ThreadsAdapter adapter = ((ThreadsAdapter) mThreadsRV.getAdapter());
        adapter.updateThreadsList(threads);
        if (mNoThreadDataTV.getVisibility() == View.VISIBLE) {
            mNoThreadDataTV.setVisibility(View.GONE);
            mThreadsRV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setPresenter(@NonNull ThreadsContract.Presenter presenter) {
        this.mPresenter = checkNotNull(presenter);
    }

    @Override
    public void setThreadsData(@NonNull final List<Thread> threads) {
        setLoadingIndicator(false);
        // just ensure its a new thread and not an update
        if (adapterIsNotCreated()) {
            // adapter is not created so set it ip, with anonymous recycler item click listener
            mThreadsRV.setAdapter(new ThreadsAdapter(threads, PlantPreferenceManager.filterProfanity(getActivity()),
                    position -> mPresenter.openThreadDetailsActivity((((ThreadsAdapter) mThreadsRV.getAdapter()).getItemAtPos(position)))));
        } else {
            ((ThreadsAdapter) mThreadsRV.getAdapter()).updateThreadsList(threads);
        }
        mThreadsRV.setVisibility(View.VISIBLE);
    }

    @Override
    public void showThreadDetailsUi(@NonNull String threadId) {
        // start the ThreadDetailsActivity with the thread to views id
        Intent intent = new Intent(getActivity(), ThreadDetailActivity.class);
        intent.putExtra(ThreadDetailActivity.EXTRA_THREAD_ID, threadId);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mThreadsRV.setVisibility(View.GONE);
            mNoThreadDataTV.setVisibility(View.GONE);
            mLoadingPB.setVisibility(View.VISIBLE);
        } else {
            mLoadingPB.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNoThreadsData() {
        mLoadingPB.setVisibility(View.GONE);
        mThreadsRV.setVisibility(View.GONE);
        mNoThreadDataTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingThreadsError() {
        mLoadingPB.setVisibility(View.GONE);
        mThreadsRV.setVisibility(View.GONE);
        mNoThreadDataTV.setVisibility(View.VISIBLE);
        mNoThreadDataTV.setText(R.string.error_fetching_threads);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setSwipeRefreshRefreshing(boolean refreshing) {
        // Make sure setRefreshing() is called after the layout is done with everything else.
        mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(refreshing));
    }

    @Override
    public void showSolvedFilterLabel() {
        setActionbarTitle(getString(R.string.filter_threads_solved));
    }

    @Override
    public void showUnsolvedFilterLabel() {
        setActionbarTitle(getString(R.string.filter_threads_unsolved));

    }

    @Override
    public void showAllFilterLabel() {
        setActionbarTitle(getString(R.string.filter_threads_all));

    }

    @Override
    public void showNotCommentedFilterLabel() {
        setActionbarTitle(getString(R.string.filter_threads_unanswered));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unSubscribe();
        mUnbinder.unbind();
    }

    // material design like search animation with circular reveal
    private void initSearchView(final Menu menu) {
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search_threads);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expandToolbar it by default
        final Toolbar toolbar = getActivity().findViewById(R.id.toolbar);

        searchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // animate circle reveal and color change of toolbar
                UiUtils.animateSearchToolbar(toolbar, true, getActivity());
                toggleMenuItemVisibility(menu, false);
                ValueAnimator colorChangeAnimation = UiUtils.colorChangeAnimator(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                        ContextCompat.getColor(getActivity(), android.R.color.background_light), 260);
                colorChangeAnimation.addUpdateListener(animator -> toolbar.setBackgroundColor((int) animator.getAnimatedValue()));
                colorChangeAnimation.start();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mPresenter.setSearchQuery(null);
                mPresenter.loadThreads(false);
                UiUtils.animateSearchToolbar(toolbar, false, getActivity());
                toggleMenuItemVisibility(menu, true);
                return true;
            }
        });

        // restore search state on orientation change
        if (mPresenter.getSearchFilterQuery() != null) {
            searchMenuItem.expandActionView();
            EditText searchET = searchView.findViewById(R.id.search_src_text);
            searchET.setText(mPresenter.getSearchFilterQuery());
            searchET.setSelection(mPresenter.getSearchFilterQuery().length());
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String constraint) {
                mPresenter.setSearchQuery(constraint);
                mPresenter.loadThreads(false);
                return false;
            }
        });
    }

    // toggle menu items visibility when showing/hiding the search view
    private void toggleMenuItemVisibility(Menu menu, boolean visible) {
        MenuItem filterMenuItem = menu.findItem(R.id.action_filter_threads);
        if (filterMenuItem != null) {
            filterMenuItem.setVisible(visible);
        }
    }

    private void setActionbarTitle(String title) {
        ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setTitle(title);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mShouldExecuteOnResume) {
            Log.i(TAG, "Onreume load Threads called");
            mPresenter.loadThreads(false);
        } else {
            mShouldExecuteOnResume = true;
        }
    }
}
