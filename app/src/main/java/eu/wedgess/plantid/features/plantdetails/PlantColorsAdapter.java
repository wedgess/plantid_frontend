/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plantdetails;

/**
 * Adapter for displaying the foliage and flower colour of plants in:
 * {@link eu.wedgess.plantid.features.plantdetails.PlantDetailActivity}
 * <p>
 * Created by gar on 25/11/17.
 */

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedHashMap;

import eu.wedgess.plantid.R;

public class PlantColorsAdapter extends BaseAdapter {

    private String[] mColours;
    private Context mContext;
    private final static LinkedHashMap<String, Integer> colorMap = createMap();

    private static LinkedHashMap<String, Integer> createMap() {
        LinkedHashMap<String, Integer> myMap = new LinkedHashMap<>();
        myMap.put("purple", R.color.purple);
        myMap.put("green", R.color.green);
        myMap.put("yellow", R.color.yellow);
        myMap.put("red", R.color.red);
        myMap.put("white", R.color.white);
        myMap.put("pink", R.color.pink);
        myMap.put("silver", R.color.silver);
        myMap.put("bronze", R.color.bronze);
        myMap.put("peach", R.color.peach);
        myMap.put("blue", R.color.blue);
        myMap.put("black", R.color.black);
        return myMap;
    }

    PlantColorsAdapter(String[] colors, Context context) {
        this.mColours = colors;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return this.mColours.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PlantColorsAdapter.ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.grid_plant_color, parent, false);
            viewHolder = new PlantColorsAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (PlantColorsAdapter.ViewHolder) convertView.getTag();
        }

        viewHolder.colourTV.setText(mColours[position]);
        GradientDrawable bgShape = (GradientDrawable) viewHolder.colourIV.getBackground();
        bgShape.setColor(ContextCompat.getColor(mContext, colorMap.get(mColours[position].toLowerCase())));
        bgShape.setStroke(2, mContext.getResources().getColor(R.color.dividers));

        return convertView;
    }

    private static class ViewHolder {
        TextView colourTV;
        ImageView colourIV;

        ViewHolder(View view) {
            colourTV = view.findViewById(R.id.color_text_tv);
            colourIV = view.findViewById(R.id.color_iv);
        }
    }

}
