/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.createeditthread;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource;
import eu.wedgess.plantid.data.source.remote.ThreadsRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.features.classification.ClassificationActivity;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.LocationUtils;
import eu.wedgess.plantid.utils.NetworkUtils;
import eu.wedgess.plantid.utils.PlantPreferenceManager;

/**
 * Activity to create or edit a thread.
 * Used by: {@link eu.wedgess.plantid.features.threaddetails.ThreadDetailActivity} and
 * {@link ClassificationActivity}.
 * <p>
 * Created by Gareth
 */
public class CreateEditThreadActivity extends BaseActivity implements CreateEditThreadContract.View, View.OnClickListener {

    private static final String TAG = CreateEditThreadActivity.class.getSimpleName();

    public static final String STATE_KEY_TITLE = "STATE_KEY_TITLE";
    public static final String STATE_KEY_MSG = "STATE_KEY_MSG";
    public static final String EXTRA_KEY_EDIT_THREAD_ID = "EXTRA_KEY_EDIT_THREAD_ID";

    //region View bindings
    @BindView(R.id.iv_thread_image)
    ImageView mThreadImageIV;
    @BindView(R.id.tv_message_limit)
    TextView mMessageLimitTV;
    @BindView(R.id.til_thread_msg)
    TextInputLayout mMessageTIL;
    @BindView(R.id.tv_title_limit)
    TextView mTitleLimitTV;
    @BindView(R.id.til_thread_title)
    TextInputLayout mTitleTIL;
    @BindView(R.id.fab_submit_thread)
    FloatingActionButton mSubmitThreadFAB;
    //endregion

    private CreateEditThreadContract.Presenter mPresenter;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_KEY_TITLE, mTitleTIL.getEditText().getText().toString());
        outState.putString(STATE_KEY_MSG, mMessageTIL.getEditText().getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_thread);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // set title, create or edit, if thread id is null its a new thread
        String threadId = getIntent().getStringExtra(EXTRA_KEY_EDIT_THREAD_ID);
        setActionbarTitle(
                threadId == null ?
                        getString(R.string.activity_title_create_edit_thread_create)
                        : getString(R.string.activity_title_create_edit_thread_edit));

        AppDatabase database = AppDatabase.getInstance(CreateEditThreadActivity.this);
        setPresenter(new CreateEditThreadPresenter(threadId, this,
                ThreadsRepository.getInstance(
                        new ThreadsLocalDataSource(database.threadDao()),
                        new ThreadsRemoteDataSource())));

        mTitleTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mTitleLimitTV.setText(String.format("%s / %s", editable.length(), getResources().getInteger(R.integer.max_chars_title)));
                if (mTitleTIL.isErrorEnabled()) {
                    mTitleTIL.setErrorEnabled(false);
                }
            }
        });
        mMessageTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mMessageLimitTV.setText(String.format("%s / %s", editable.length(), getResources().getInteger(R.integer.max_chars_msg)));
                if (mMessageTIL.isErrorEnabled()) {
                    mMessageTIL.setErrorEnabled(false);
                }
            }
        });
        // we want new line key to be the done key
        mMessageTIL.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        mMessageTIL.getEditText().setRawInputType(InputType.TYPE_CLASS_TEXT);
        mSubmitThreadFAB.setOnClickListener(this);

        // load image from path/URL from intent

        if (savedInstanceState == null) {
            GlideImageHelper.glideLoadImage(mThreadImageIV, getImagePathFromExtras());
            mPresenter.subscribe();
        } else {
            // restore input fields on config change
            setThreadTitle(savedInstanceState.getString(STATE_KEY_TITLE));
            setThreadMessage(savedInstanceState.getString(STATE_KEY_MSG));
            if (!Strings.isNullOrEmpty(threadId)) {
                mPresenter.loadProfileImage(threadId);
            } else {
                // new thread which has not been saved needs to be loaded from intent
                GlideImageHelper.glideLoadImage(mThreadImageIV, getImagePathFromExtras());
            }
        }
    }

    private String getImagePathFromExtras() {
        String imgUrl = "";
        // get img url path from extras, this is only used for a new thread
        if (getIntent().hasExtra(ClassificationActivity.EXTRA_KEY_IMG_URL)) {
            imgUrl = getIntent().getStringExtra(ClassificationActivity.EXTRA_KEY_IMG_URL);
        } else {
            imgUrl = getIntent().getStringExtra(ClassificationActivity.EXTRA_KEY_IMG_FILE_PATH);
        }
        return imgUrl;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull CreateEditThreadPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showTitleErrorMessage() {
        mTitleTIL.setErrorEnabled(true);
        mTitleTIL.setError(getString(R.string.error_thread_title_empty));
    }

    @Override
    public void showMessageErrorMessage() {
        mMessageTIL.setErrorEnabled(true);
        mMessageTIL.setError(getString(R.string.error_thread_msg_empty));
    }

    @Override
    public void threadCreationSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void setThreadTitle(String title) {
        mTitleTIL.getEditText().setText(title);
        mTitleTIL.getEditText().setSelection(title.length());
    }

    @Override
    public void setThreadMessage(String message) {
        mMessageTIL.getEditText().setText(message);
    }

    @Override
    public void setThreadImage(String imgUrl) {
        GlideImageHelper.glideLoadImage(mThreadImageIV, imgUrl);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab_submit_thread) {
            String titleField = mTitleTIL.getEditText().getText().toString();
            String messageField = mMessageTIL.getEditText().getText().toString();

            if (mPresenter.validateThreadFields(titleField, messageField)) {
                // remove any error messages
                mTitleTIL.setErrorEnabled(false);
                mMessageTIL.setErrorEnabled(false);
                mPresenter.submitThread(titleField, messageField, getImagePathFromExtras(),
                        PlantPreferenceManager.hideUserLocation(CreateEditThreadActivity.this)
                                ? "" : LocationUtils.getLocationName(CreateEditThreadActivity.this),
                        !NetworkUtils.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }
}
