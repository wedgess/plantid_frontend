/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.userdetails;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

import java.net.ConnectException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import eu.wedgess.plantid.utils.DateUtility;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter class for {@link UserDetailsFragment}
 * <p>
 * Created by gar on 09/11/17.
 */

public class UserDetailsPresenter implements UserDetailContract.Presenter {

    private static final String TAG = UserDetailsPresenter.class.getSimpleName();

    @NonNull
    private CompositeDisposable mCompositeDisposable;
    private boolean mHideUserLocation = false;

    private final UserRepository mUserRepository;
    private final HistoryRepository mHistoryRepository;
    private final ThreadsRepository mThreadsRepository;
    private final CommentsRepository mCommentsRepository;
    private final UserDetailContract.View mHomeView;

    UserDetailsPresenter(@NonNull UserRepository mUserRepository,
                         @NonNull UserDetailContract.View mHomeView,
                         @NonNull ThreadsRepository threadsRepository,
                         @NonNull CommentsRepository commentsRepository,
                         @NonNull HistoryRepository historyRepository,
                         boolean hideUserLocation) {
        this.mThreadsRepository = checkNotNull(threadsRepository);
        this.mCommentsRepository = checkNotNull(commentsRepository);
        this.mUserRepository = checkNotNull(mUserRepository);
        this.mHistoryRepository = checkNotNull(historyRepository);
        this.mHomeView = checkNotNull(mHomeView);
        this.mHomeView.setPresenter(this);

        mHideUserLocation = hideUserLocation;
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void getUserDetails() {
        // get the current user, make sure it is present as User is an Optional.
        // Then show user details ui and set current user
        mCompositeDisposable.add(mUserRepository.getCurrentUser()
                .subscribeOn(Schedulers.io())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                            CurrentUserInstance.getInstance().setCurrentUser(user);
                            showUserDetails(user);
                            checkLocalUserIsLatest(user.getUid());
                        },
                        throwable -> {
                            // handle error event
                            Log.e(TAG, "Error getting current user");
//                            throw Exceptions.propagate(throwable);
                        }));
    }

    private void checkLocalUserIsLatest(@NonNull String uid) {
        mCompositeDisposable.add(
                mUserRepository.getCurrentUserFromRemoteRepository(uid)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(user -> {
                                    // get the current user and check if the current user and remote user are not equal, if not update local user
                                    User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
                                    if (currentUser != null && !currentUser.equals(user)) {
                                        mUserRepository.updateCachedUser(user);
                                        getUserDetails();
                                    }
                                },
                                throwable -> Log.e(TAG, "Error getting user with id: " + uid + " from remote.")));
    }

    @Override
    public void updateHistoryData() {
        getAllHistoryItems(CurrentUserInstance.getInstance().getCurrentUser().getUid(), true);
    }

    @Override
    public void getUsersThreads() {
        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
        if (currentUser != null) {
            getUsersThreads(currentUser.getUid());
        }
    }

    @Override
    public void deleteUser() {
        // delete user when they choose to delet profile
        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
        // after delete sign the user out
        Disposable deleteUser = mUserRepository
                .deleteUserProfile(currentUser.getUid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::signUserOut);
        mCompositeDisposable.add(deleteUser);
    }

    /**
     * Get the threads created by the current user (uid) and get the threads comment (authors not needed).
     * Then on complete display the users threads in the view.
     *
     * @param uid - id of user whose threads to fetch
     */
    private void getUsersThreads(String uid) {
        mCompositeDisposable.add(mThreadsRepository.getThreadsByAuthorId(uid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(Observable::fromIterable) //map the list to an Observable that emits every item as an observable
                .flatMap(thread ->
                        mCommentsRepository
                                .getCommentsByThreadId(thread.getId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .map(comments -> {
                                    thread.setComments(comments);
                                    return thread;
                                }))
                .distinct()
                .toSortedList((thread, t1) -> Objects.requireNonNull(t1.getDateCreated())
                        .compareTo(Objects.requireNonNull(thread.getDateCreated()))) // sort list latest first
                .subscribe(
                        threads -> {
                            if (mHomeView.adapterIsNotCreated()) {
                                mHomeView.setUsersThreads(threads);
                            } else {
                                mHomeView.updateThreadsUi(threads);
                            }
                        },
                        error -> {
                            if (error instanceof ConnectException) {
                                Log.e(TAG, "Error fetching users threads, could not connect to remote...");
                                return;
                            }
                            if (!mHomeView.isActive()) {
                                return;
                            }

                            mHomeView.showLoadingThreadsError();
//                            throw Exceptions.propagate(error);
                        }));
    }

    private void showUserDetails(User user) {
        if (user == null || Strings.isNullOrEmpty(user.getUserName())) {
            setUserLoggedOutUi();
        } else {
            // cannot handle UI updates anymore
            if (!mHomeView.isActive()) {
                return;
            }
            mHomeView.showUserActionsPanel();
            if (!Strings.isNullOrEmpty(user.getImgUrl())) {
                mHomeView.setUserProfileImage(user.getImgUrl());
            }
            if (!Strings.isNullOrEmpty(user.getUserName())) {
                mHomeView.setUserProfileUsername(user.getUserName());
            }
            if (!Strings.isNullOrEmpty(user.getDateCreated())) {
                String parsedDate = user.getDateCreated();
                try {
                    parsedDate = DateUtility.dateToProfileDateString(user.getDateCreated());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                mHomeView.setUserProfileDateCreated(parsedDate);
            }
            mHomeView.setSignedOutButtonText();
            if (mHideUserLocation) {
                mHomeView.hideUserLocation();
            } else {
                mHomeView.showUserLocation();
            }
            getAllHistoryItems(user.getUid(), false);
            getUsersThreads(user.getUid());
        }
    }

    private void setUserLoggedOutUi() {
        // reset all user views to logged out state
        mHomeView.setUserNotLoggedIn();
        mHomeView.setSignedOutButtonText();
        mHomeView.setMatchedHistoryCount(0);
        mHomeView.setUnmatchedHistoryCount(0);
        mHistoryRepository.clearCache();
        mHomeView.hideUsersThreadsUi();
        mHomeView.hideUserActionsPanel();
    }

    @Override
    public boolean canOpenHistory(String historyCount) {
        // user can only open history items if there are any present
        if (Strings.isNullOrEmpty(historyCount) || historyCount.equals("0")) {
            mHomeView.showNoHistoryItemsToast();
            return false;
        }
        return true;
    }

    @Override
    public void signUserOut() {
        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
        mCompositeDisposable.add(mUserRepository
                .signUserOut(currentUser)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                            CurrentUserInstance.getInstance().setCurrentUser(null);
                            showUserDetails(null);
                            mHomeView.setUserNotLoggedIn();
                        },
                        throwable -> Log.e(TAG, "Error signing user out")));
    }

    private void getAllHistoryItems(@NonNull String uid, boolean isUpdate) {
        Log.i(TAG, "*** getting history items for user: " + uid);
        mCompositeDisposable.add(
                mHistoryRepository
                        .getHistory(uid)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(historyList -> {
                            if (!mHomeView.isActive()) return;
                            processHistoryItems(historyList, isUpdate);
                        }, throwable -> {
                            Log.e(TAG, "Error getting history items");
                            mHomeView.setMatchedHistoryCountNoAnim(0);
                            mHomeView.setUnmatchedHistoryCountNoAnim(0);
                            if (throwable instanceof ConnectException) {
                                Log.e(TAG, "Failed to connect to server, it is possibly offline");
                                return;
                            }
//                            throw Exceptions.propagate(throwable);
                        }));
    }

    /**
     * Handle splitting all history items into two seperate lists.
     * One for matched history and the other for unmatched history.
     *
     * @param historyList - list of history items to split
     * @param isUpdate    - is an update to existing list or new
     */
    private void processHistoryItems(List<History> historyList, boolean isUpdate) {
        ArrayList<History> matchedItems = new ArrayList<>();
        for (History item : historyList) {
            Log.i(TAG, item.toString());
            if (item.isMatch()) {
                matchedItems.add(item);
            }
        }
        historyList.removeAll(matchedItems);

        // cannot handle UI updates anymore
        if (!mHomeView.isActive()) {
            return;
        }

        if (isUpdate) {
            mHomeView.setMatchedHistoryCountNoAnim(matchedItems.size());
            mHomeView.setUnmatchedHistoryCountNoAnim(historyList.size());
        } else {
            mHomeView.setMatchedHistoryCount(matchedItems.size());
            mHomeView.setUnmatchedHistoryCount(historyList.size());
        }
    }

    @Override
    public void openThreadDetailsActivity(@NonNull Thread requestedThread) {
        checkNotNull(requestedThread, "requestedThread cannot be null!");
        mHomeView.showThreadDetailsUi(requestedThread.getId());
    }

    @Override
    public void subscribe() {
        getUserDetails();
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }
}
