/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plants;

import android.animation.ValueAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.source.local.PlantsLocalDataSource;
import eu.wedgess.plantid.data.source.remote.PlantsRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import eu.wedgess.plantid.features.plantdetails.PlantDetailActivity;
import eu.wedgess.plantid.utils.UiUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link PlantsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlantsFragment extends Fragment implements PlantsContract.View {

    private static final String TAG = PlantsFragment.class.getSimpleName();

    private static final String STATE_KEY_LIST = "STATE_KEY_LIST";
    private static final String STATE_KEY_CURRENT_FILTERING = "STATE_KEY_CURRENT_FILTERING";
    private static final String STATE_KEY_CURRENT_SEARCH = "STATE_KEY_CURRENT_SEARCH";

    //region View binding
    @BindView(R.id.rv_plant_encyclopedia)
    protected RecyclerView mPlantListRV;
    @BindView(R.id.pb_loading_plants)
    protected ProgressBar mLoadingPB;
    @BindView(R.id.tv_no_plant_data)
    protected TextView mNoPlantDataTV;
    //endregion

    private PlantsContract.Presenter mPresenter;
    private Unbinder mUnbinder;

    public PlantsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static PlantsFragment newInstance() {
        return new PlantsFragment();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // save state of plant list, filter and search query on config change
        if (mPlantListRV.getAdapter() != null) {
            outState.putParcelableArrayList(STATE_KEY_LIST, ((PlantsAdapter) mPlantListRV.getAdapter()).getList());
        }
        outState.putSerializable(STATE_KEY_CURRENT_FILTERING, mPresenter.getFiltering());
        outState.putString(STATE_KEY_CURRENT_SEARCH, mPresenter.getSearchFilterQuery());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_plants, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);
        if (isAdded()) {
            new PlantsPresenter(PlantsRepository.getInstance(
                    new PlantsLocalDataSource(AppDatabase.getInstance(getActivity()).plantDao()),
                    new PlantsRemoteDataSource()), this);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // if fragment is first displayed savedInstanceState is null so create and fetch list
        if (savedInstanceState == null) {
            mPresenter.subscribe();
        } else {
            // on configuration change (screen orientation) restore saved list,  filtering type and query string
            if (savedInstanceState.containsKey(STATE_KEY_LIST)) {
                ArrayList<Plant> list = savedInstanceState.getParcelableArrayList(STATE_KEY_LIST);
                setPlantsData(list);
            }
            PlantsFilterType currentFiltering = (PlantsFilterType) savedInstanceState.getSerializable(STATE_KEY_CURRENT_FILTERING);
            mPresenter.setFiltering(currentFiltering);
            mPresenter.setSearchQuery(savedInstanceState.getString(STATE_KEY_CURRENT_SEARCH));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.encyclopedia, menu);
        initSearchView(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_filter_plants) {
            showFilteringPopUpMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showFilteringPopUpMenu() {
        PopupMenu popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.overflow));
        popup.getMenuInflater().inflate(R.menu.filter_plants, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.all_plants:
                    mPresenter.setFiltering(PlantsFilterType.ALL_PLANTS);
                    break;
                case R.id.annual_plants:
                    mPresenter.setFiltering(PlantsFilterType.ANNUAL_ONLY);
                    break;
                case R.id.alpine:
                    mPresenter.setFiltering(PlantsFilterType.ALPINE_ONLY);
                    break;
                case R.id.bulbs:
                    mPresenter.setFiltering(PlantsFilterType.BULBS_ONLY);
                    break;
                case R.id.conservatory:
                    mPresenter.setFiltering(PlantsFilterType.CONSERVATORY_ONLY);
                    break;
                case R.id.climbers:
                    mPresenter.setFiltering(PlantsFilterType.CLIMBER_ONLY);
                    break;
                case R.id.perennials:
                    mPresenter.setFiltering(PlantsFilterType.PERENNIAL_ONLY);
                    break;
                case R.id.shrubs:
                    mPresenter.setFiltering(PlantsFilterType.SHRUBS_ONLY);
                    break;
                case R.id.trees:
                    mPresenter.setFiltering(PlantsFilterType.TREES_ONLY);
                    break;
                default:
                    mPresenter.setFiltering(PlantsFilterType.ALL_PLANTS);
                    break;
            }
            mPresenter.loadPlants(false);
            return true;
        });

        popup.show();
    }

    @Override
    public void setPresenter(@NonNull PlantsContract.Presenter presenter) {
        this.mPresenter = checkNotNull(presenter);
    }

    @Override
    public void setPlantsData(@NonNull final List<Plant> plants) {
        setLoadingIndicator(false);
        mPlantListRV.setVisibility(View.VISIBLE);
        // set adapter and set anonymous click listener for when plant item is clicked
        mPlantListRV.setAdapter(new PlantsAdapter(plants,
                position -> mPresenter.openPlantDetailsActivity((((PlantsAdapter) mPlantListRV.getAdapter()).getItemAtPos(position)))));
    }

    @Override
    public void showPlantDetailsUi(@NonNull String plantId) {
        Intent intent = new Intent(getActivity(), PlantDetailActivity.class);
        intent.putExtra(PlantDetailActivity.EXTRA_PLANT_ID, plantId);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mPlantListRV.setVisibility(View.GONE);
            mNoPlantDataTV.setVisibility(View.GONE);
            mLoadingPB.setVisibility(View.VISIBLE);
        } else {
            mLoadingPB.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNoPlantData() {
        mLoadingPB.setVisibility(View.GONE);
        mPlantListRV.setVisibility(View.GONE);
        mNoPlantDataTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingPlantsError() {

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showAllFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_all));
    }

    @Override
    public void showPerenialsFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_perennials));
    }

    @Override
    public void showShrubsFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_shrubs));
    }

    @Override
    public void showTreesFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_trees));
    }

    @Override
    public void showBulbsFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_bulbs));
    }

    @Override
    public void showConservatoryFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_conservatory));
    }

    @Override
    public void showClimbersFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_climbers));
    }

    @Override
    public void showAlpineFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_alpine));
    }

    @Override
    public void showAnnualFilterLabel() {
        setActionbarTitle(getString(R.string.plants_filter_annual));
    }

    private void setActionbarTitle(String title) {
        ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setTitle(title);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unSubscribe();
        mUnbinder.unbind();
    }


    // setup search view and animation and actions performed by the search view
    private void initSearchView(final Menu menu) {
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search_plants);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expandToolbar it by default
        final Toolbar toolbar = getActivity().findViewById(R.id.toolbar);

        searchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // animate circle reveal and color change of toolbar
                UiUtils.animateSearchToolbar(toolbar, true, getActivity());
                toggleMenuItemVisibility(menu, false);
                ValueAnimator colorChangeAnimation = UiUtils.colorChangeAnimator(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                        ContextCompat.getColor(getActivity(), android.R.color.background_light), 260);
                colorChangeAnimation.addUpdateListener(animator -> toolbar.setBackgroundColor((int) animator.getAnimatedValue()));
                colorChangeAnimation.start();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mPresenter.setSearchQuery(null);
                mPresenter.loadPlants(false);
                UiUtils.animateSearchToolbar(toolbar, false, getActivity());
                toggleMenuItemVisibility(menu, true);
                return true;
            }
        });

        // restore search state on orientation change
        if (mPresenter.getSearchFilterQuery() != null) {
            searchMenuItem.expandActionView();
            EditText searchET = searchView.findViewById(R.id.search_src_text);
            searchET.setText(mPresenter.getSearchFilterQuery());
            searchET.setSelection(mPresenter.getSearchFilterQuery().length());
        }

        // must set query listener after restoring search state or query is wiped
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String constraint) {
                mPresenter.setSearchQuery(constraint);
                mPresenter.loadPlants(false);
                return false;
            }
        });
    }

    // toggle the other menu items visibility when searchview is shown
    private void toggleMenuItemVisibility(Menu menu, boolean visible) {
        MenuItem filterMenuItem = menu.findItem(R.id.action_filter_plants);
        if (filterMenuItem != null) {
            filterMenuItem.setVisible(visible);
        }
    }
}
