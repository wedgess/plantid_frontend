/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threads;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Optional;

import java.util.List;

import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link ThreadsFragment}
 * <p>
 * Created by gar on 09/11/17.
 */

public class ThreadsPresenter implements ThreadsContract.Presenter {

    private static final String TAG = ThreadsPresenter.class.getSimpleName();

    private final ThreadsRepository mThreadsRepository;
    private final CommentsRepository mCommentsRepository;
    private final UserRepository mUserRepository;
    private final ThreadsContract.View mThreadsView;
    private final CompositeDisposable mCompositeDisposable;
    private String mSearchQueryFilter = null;

    @NonNull
    private ThreadsFilterType mCurrentFiltering = ThreadsFilterType.ALL_THREADS;

    ThreadsPresenter(@NonNull ThreadsRepository threadsRepository,
                     @NonNull CommentsRepository commentsRepository,
                     @NonNull UserRepository userRepository,
                     @NonNull ThreadsContract.View communityView) {
        this.mThreadsRepository = checkNotNull(threadsRepository);
        this.mCommentsRepository = checkNotNull(commentsRepository);
        this.mUserRepository = checkNotNull(userRepository);
        this.mThreadsView = checkNotNull(communityView);
        this.mThreadsView.setPresenter(this);
        mCompositeDisposable = new CompositeDisposable();
    }

    @SuppressWarnings("Guava")
    @Override
    public void loadThreads(boolean showLoadingUI) {
        if (showLoadingUI) {
            mThreadsView.setLoadingIndicator(true);
        }

        /*
         * Join together getting the thread author, and comments and add them all to each thread.
         * Apply search filters to search for specific threads by title.
         * Apply another filter to get threads by no comments, solved, unsolved & all
         * Sort the list showing the latest first.
         */
        mCompositeDisposable.add(mThreadsRepository.getThreads()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(Observable::fromIterable) //map the list to an Flowable that emits every item as an observable
                .filter(thread -> mSearchQueryFilter == null || (thread.getTitle().toLowerCase().contains(mSearchQueryFilter)))
                .flatMap(thread ->
                        mUserRepository
                                .getUserById(thread.getAuthorId())
                                .toObservable()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .filter(user -> Optional.of(user).isPresent())
                                .map(user -> Optional.of(user).get())
                                .map(user -> {
                                    thread.setAuthor(user);
                                    return thread;
                                }))
                .flatMap(thread -> mCommentsRepository
                        .getCommentsByThreadId(thread.getId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(comments -> {
                            thread.setComments(comments);
                            return thread;
                        }))
                .filter(thread -> {
                    switch (mCurrentFiltering) {
                        case NO_COMMENTED_THREADS:
                            return thread.getComments().isEmpty();
                        case SOLVED_THREADS:
                            return thread.isSolved();
                        case UNSOLVED_THREADS:
                            return !thread.isSolved();
                        case ALL_THREADS:
                        default:
                            return true;
                    }
                })
                .distinct()
                .toSortedList((thread, t1) -> t1.getDateCreated().compareTo(thread.getDateCreated())) // sort list latest first
                .subscribe(
                        threads -> {
                            showThreads(threads, showLoadingUI);
                            showFilterLabel();
                        },
                        error -> {
                            if (!mThreadsView.isActive()) {
                                return;
                            }
                            Log.e(TAG, "Got threads error");
                            mThreadsView.showLoadingThreadsError();
//                            throw Exceptions.propagate(error);
                        }));

    }

    // sets the thread filter for type of threads
    private void showFilterLabel() {
        switch (mCurrentFiltering) {
            case ALL_THREADS:
                mThreadsView.showAllFilterLabel();
                break;
            case UNSOLVED_THREADS:
                mThreadsView.showUnsolvedFilterLabel();
                break;
            case SOLVED_THREADS:
                mThreadsView.showSolvedFilterLabel();
                break;
            case NO_COMMENTED_THREADS:
                mThreadsView.showNotCommentedFilterLabel();
                break;
            default:
                mThreadsView.showAllFilterLabel();
                break;
        }
    }

    // handles showing all the thread info in the UI
    private void showThreads(List<Thread> threads, boolean showLoadingUI) {
        if (!mThreadsView.isActive()) {
            Log.e(TAG, "Community view is not active");
            return;
        }

        if (showLoadingUI) {
            mThreadsView.setLoadingIndicator(false);
        } else {
            mThreadsView.setSwipeRefreshRefreshing(false);
        }

        if (threads == null || threads.isEmpty()) {
            mThreadsView.showNoThreadsData();
        } else {
            mThreadsView.setLoadingIndicator(false);

            if (mThreadsView.adapterIsNotCreated()) {
                mThreadsView.setThreadsData(threads);
            } else {
                mThreadsView.updateThreadsUi(threads);
            }
        }
    }


    @Override
    public void openThreadDetailsActivity(@NonNull Thread requestedThread) {
        checkNotNull(requestedThread, "requestedThread cannot be null!");
        mThreadsView.showThreadDetailsUi(requestedThread.getId());
    }

    @Override
    public void refreshThreads() {
        mCommentsRepository.refreshComments();
        mThreadsRepository.refreshThreads();
        loadThreads(false);
    }

    @Override
    public void setSearchQuery(String query) {
        this.mSearchQueryFilter = query != null ? query.toLowerCase() : query;
    }

    @Override
    public void setFiltering(@NonNull ThreadsFilterType requestType) {
        mCurrentFiltering = requestType;
        showFilterLabel();
    }

    @Override
    public ThreadsFilterType getFiltering() {
        return mCurrentFiltering;
    }

    @Override
    public String getSearchFilterQuery() {
        return this.mSearchQueryFilter;
    }

    @Override
    public void subscribe() {
        this.loadThreads(true);
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }
}
