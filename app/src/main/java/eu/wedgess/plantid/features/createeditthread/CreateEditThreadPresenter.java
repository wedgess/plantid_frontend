/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.createeditthread;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.base.Strings;

import java.net.UnknownHostException;
import java.util.UUID;

import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import eu.wedgess.plantid.utils.DateUtility;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The presenter for {@link CreateEditThreadActivity}.
 * <p>
 * Created by gar on 01/01/18.
 */

public class CreateEditThreadPresenter implements CreateEditThreadContract.Presenter {

    public static final String TAG = CreateEditThreadPresenter.class.getSimpleName();

    private final CreateEditThreadContract.View mCreateEditThreadView;
    private final ThreadsRepository mThreadRepository;
    private final CompositeDisposable mCompositeDisposable;

    @Nullable
    private String threadId;


    CreateEditThreadPresenter(@Nullable String threadId, @NonNull CreateEditThreadContract.View mCreateEditThreadView,
                              @NonNull ThreadsRepository threadsRepository) {
        this.threadId = threadId;
        this.mCreateEditThreadView = checkNotNull(mCreateEditThreadView);
        this.mThreadRepository = checkNotNull(threadsRepository);
        this.mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        if (!isNewThread()) {
            checkNotNull(threadId);
            getThreadInfoById(threadId, false);
        }
    }

    /**
     * Gets a thread object by id, then populates the fields in the view,
     * on config changes of an edit we just need to reload the image, input fields
     * are restored from state.
     *
     * @param threadId      - thread id to get
     * @param loadImageOnly - whether to load only the image or input field values as well
     */
    private void getThreadInfoById(String threadId, boolean loadImageOnly) {
        mCompositeDisposable.add(mThreadRepository.getThreadById(threadId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(thread -> populateThreadInfo(thread, loadImageOnly),
                        throwable -> Log.e(TAG, "Error loading thread by id")));
    }

    // populates thread info, title, message and thread image
    private void populateThreadInfo(Thread thread, boolean loadImageOnly) {
        if (!Strings.isNullOrEmpty(thread.getThreadImgUrl())) {
            mCreateEditThreadView.setThreadImage(thread.getThreadImgUrl());
        }
        if (loadImageOnly) {
            return;
        }
        if (!Strings.isNullOrEmpty(thread.getTitle())) {
            mCreateEditThreadView.setThreadTitle(thread.getTitle());
        }
        if (!Strings.isNullOrEmpty(thread.getMessage())) {
            mCreateEditThreadView.setThreadMessage(thread.getMessage());
        }
    }

    private boolean isNewThread() {
        return threadId == null;
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void submitThread(@NonNull final String title,
                             @NonNull final String message,
                             @NonNull final String imgUrl,
                             @NonNull final String location,
                             final boolean isOffline) {
        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
        // if current user if null do nothing, (Should never be null but just a check)
        if (currentUser != null) {
            if (isNewThread()) {
                final Thread thread = new Thread(UUID.randomUUID().toString(), title, message, currentUser.getUid(),
                        DateUtility.getDBDateFormat(), imgUrl, location, false, isOffline);
                createThread(thread);
            } else {
                checkNotNull(threadId, "Cannot update thread as threadId is not set");
                mCompositeDisposable.add(mThreadRepository.getThreadById(threadId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(thread -> {
                            thread.setTitle(title);
                            thread.setMessage(message);
                            thread.setDirty(isOffline);
                            updateThread(thread);
                        }, throwable -> Log.e(TAG, "Error loading thread by id")));
            }
        }
    }

    @Override
    public void loadProfileImage(@NonNull String threadId) {
        checkNotNull(threadId);
        getThreadInfoById(threadId, true);
    }

    @Override
    public boolean validateThreadFields(@NonNull String title, @NonNull String message) {
        if (Strings.isNullOrEmpty(title)) {
            mCreateEditThreadView.showTitleErrorMessage();
            return false;
        }

        if (Strings.isNullOrEmpty(message)) {
            mCreateEditThreadView.showMessageErrorMessage();
            return false;
        }
        return true;
    }

    private void createThread(Thread thread) {
        mCompositeDisposable.add(mThreadRepository
                .insertThread(thread)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.i(TAG, "Thread inserted successfully");
                    mCreateEditThreadView.threadCreationSuccess();
                }, throwable -> {
                    Log.i(TAG, "Error inserting thread to remote: " + thread.toString());
                    // on offline usage the thread is still created locally so close activity
                    mCreateEditThreadView.threadCreationSuccess();
                    //Exceptions.propagate(throwable);
                }));
    }

    private void updateThread(Thread thread) {
        mCompositeDisposable.add(mThreadRepository
                .updateThread(thread)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.i(TAG, "Thread  updated successfully");
                    mCreateEditThreadView.threadCreationSuccess();
                }, throwable -> {
                    if (throwable instanceof UndeliverableException || throwable instanceof UnknownHostException) {
                        Log.e(TAG, "User possibly offline");
                        mCreateEditThreadView.threadCreationSuccess();
                        return;
                    }
//                    throw Exceptions.propagate(throwable);
                }));
    }
}
