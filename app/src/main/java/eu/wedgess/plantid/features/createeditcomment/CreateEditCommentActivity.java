/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.createeditcomment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.source.local.CommentsLocalDataSource;
import eu.wedgess.plantid.data.source.remote.CommentsRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.features.threaddetails.ThreadDetailActivity;
import eu.wedgess.plantid.utils.NetworkUtils;

/**
 * Activity to create and edit a comment.
 * Called from {@link ThreadDetailActivity}
 * <p>
 * Created by gar on 03/01/18.
 */

public class CreateEditCommentActivity extends BaseActivity implements CreateEditCommentContract.View {

    public static final String TAG = CreateEditCommentActivity.class.getSimpleName();

    public static final String STATE_KEY_COMMENT_MESSAGE = "STATE_KEY_COMMENT_MESSAGE";
    public static final String EXTRA_KEY_COMMENT_BUNDLE = "EXTRA_KEY_COMMENT_BUNDLE";
    public static final String EXTRA_KEY_THREAD_ID = "EXTRA_KEY_THREAD_ID";
    public static final String EXTRA_KEY_COMMENT_ID = "EXTRA_KEY_COMMENT_ID";
    public static final String EXTRA_KEY_COMMENT_POSITION = "EXTRA_KEY_COMMENT_POSITION";

    //region View binding
    @BindView(R.id.til_comment_msg)
    TextInputLayout mCommentMessageTIL;
    @BindView(R.id.tv_comment_limit)
    TextView mCommentTextLimitTV;
    @BindView(R.id.fab_submit_comment)
    FloatingActionButton mFabSubmitComment;
    //endregion

    private int mEditedCommentPosition = -1;
    private CreateEditCommentContract.Presenter mPresenter;

    private TextWatcher mCommentTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            mCommentTextLimitTV.setText(String.format("%s / %s", editable.length(), 800));
            if (mCommentMessageTIL.isErrorEnabled()) {
                mCommentMessageTIL.setErrorEnabled(false);
            }
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save comment message on config change
        outState.putString(STATE_KEY_COMMENT_MESSAGE, mCommentMessageTIL.getEditText().getText().toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_comment);
        ButterKnife.bind(this);

        // show back arrow and set the toolbar as this activities actionbar
        setSupportActionBar(findViewById(R.id.toolbar_comment));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get the bundle from intent and get all keys passed
        Bundle arguments = getIntent().getBundleExtra(EXTRA_KEY_COMMENT_BUNDLE);
        String threadId = arguments.getString(EXTRA_KEY_THREAD_ID);
        String commentId = arguments.getString(EXTRA_KEY_COMMENT_ID);
        mEditedCommentPosition = arguments.getInt(EXTRA_KEY_COMMENT_POSITION);
        setActionbarTitle(
                commentId == null ?
                        getString(R.string.activity_title_create_comment)
                        : getString(R.string.activity_title_edit_comment));

        // set up presenter
        AppDatabase appDatabase = AppDatabase.getInstance(CreateEditCommentActivity.this);
        CommentsRepository commentsRepository = CommentsRepository.getInstance(
                new CommentsLocalDataSource(appDatabase.commentDao()),
                new CommentsRemoteDataSource());
        setPresenter(new CreateEditCommentPresenter(threadId, commentId, commentsRepository, this));

        // show done button on comment instead of new line & add text watcher to update word count
        mCommentMessageTIL.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        mCommentMessageTIL.getEditText().setRawInputType(InputType.TYPE_CLASS_TEXT);
        mCommentMessageTIL.getEditText().addTextChangedListener(mCommentTextWatcher);

        mFabSubmitComment.setOnClickListener(view -> {
            // if valid comment - not empty, then submit thread to be updated or added
            String message = mCommentMessageTIL.getEditText().getText().toString();
            if (mPresenter.validateCommentMessage(message)) {
                mPresenter.submitComment(message,
                        !NetworkUtils.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)));
            }
        });

        // on first load of activity subscribe to presenter, otherwise load comment from saved state
        if (savedInstanceState == null) {
            mPresenter.subscribe();
        } else {
            String comment = savedInstanceState.getString(STATE_KEY_COMMENT_MESSAGE);
            if (comment != null) {
                setCommentMessage(comment);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull CreateEditCommentPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setCommentEmptyError() {
        mCommentMessageTIL.setError(getString(R.string.error_comment_msg_empty));
        mCommentMessageTIL.setErrorEnabled(true);
    }

    @Override
    public void setCommentMessage(@NonNull String message) {
        mCommentMessageTIL.getEditText().setText(message);
        // set cursor at end of text
        mCommentMessageTIL.getEditText().setSelection(message.length());
    }

    @Override
    public void commentUpdateSuccessful(@NonNull Comment comment) {
        Intent resultIntent = new Intent();
        Bundle resultBundle = new Bundle();
        resultBundle.putInt(ThreadDetailActivity.EXTRA_COMMENT_POSITION, mEditedCommentPosition);
        resultBundle.putString(ThreadDetailActivity.EXTRA_COMMENT_MSG, comment.getMessage());
        resultIntent.putExtra(ThreadDetailActivity.EXTRA_COMMENT_RESULT, resultBundle);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void commentInsertSuccessful() {
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }
}
