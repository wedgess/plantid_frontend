/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.history;

import android.annotation.SuppressLint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.DateUtility;
import eu.wedgess.plantid.utils.SelectableAdapter;
import eu.wedgess.plantid.utils.UiUtils;

/**
 * Used for loading each history item which is displayed in {@link HistoryActivity}
 * <p>
 * Created by gar on 31/10/17.
 */

public class HistoryAdapter extends SelectableAdapter<HistoryAdapter.HistoryViewHolder> {

    public static final String TAG = HistoryAdapter.class.getSimpleName();

    // callback for item click and long click
    public interface RecyclerClickListener {
        void onRecyclerItemClick(int position);

        boolean onRecyclerItemLongClick(int position);
    }

    private List<History> mHistoryItems;
    private RecyclerClickListener mRecyclerClickListener;

    HistoryAdapter(List<History> history, RecyclerClickListener recyclerClickListener) {
        super();
        this.mHistoryItems = history;
        this.mRecyclerClickListener = recyclerClickListener;
    }

    // if all items are already selected then clear selection, otherwise select all items
    void selectAllHistoryItems() {
        if (getSelectedItemCount() == mHistoryItems.size()) {
            clearSelection();
        } else {
            selectAll();
        }
    }

    // TODO: Use diff util
    void deleteItems(List<History> itemsToDelete) {
        for (History item : itemsToDelete) {
            removeAt(mHistoryItems.indexOf(item));
        }
    }

    private void removeAt(int position) {
        mHistoryItems.remove(position);
        notifyItemRemoved(position);
        // update all items below
        notifyItemRangeChanged(position, mHistoryItems.size());
    }

    public ArrayList<History> getList() {
        return new ArrayList<>(this.mHistoryItems);
    }

    // Create new views (invoked by the layout manager)
    @SuppressLint("InflateParams")
    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_history, null), mRecyclerClickListener);
    }

    // get hostory item at clicked position
    public History getItemAtPos(int pos) {
        return this.mHistoryItems.get(pos);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(HistoryViewHolder viewHolder, int position) {
        History historyItem = mHistoryItems.get(position);
        viewHolder.setData(historyItem, isSelected(position));
    }

    // Return the size of your mHistoryItems (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mHistoryItems.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    static class HistoryViewHolder extends RecyclerView.ViewHolder {

        final FrameLayout contentHolder;
        final ImageView ivHistoryImage;
        final ImageView ivSelectedIcon;
        final TextView tvPlantHistoryTitle;
        final TextView tvHistorySubtitle;
        final TextView tvHistoryDate;
        final TextView tvHistoryLocation;

        HistoryViewHolder(View itemLayoutView, final RecyclerClickListener listener) {
            super(itemLayoutView);
            tvPlantHistoryTitle = itemLayoutView.findViewById(R.id.tv_history_item_title);
            tvHistorySubtitle = itemLayoutView.findViewById(R.id.tv_history_item_subtitle);
            tvHistoryDate = itemLayoutView.findViewById(R.id.tv_history_date);
            ivHistoryImage = itemLayoutView.findViewById(R.id.iv_history_img);
            tvHistoryLocation = itemLayoutView.findViewById(R.id.tv_history_location);
            ivSelectedIcon = itemLayoutView.findViewById(R.id.iv_item_selected);
            contentHolder = itemLayoutView.findViewById(R.id.fl_holder);
            itemLayoutView.setOnClickListener(v -> listener.onRecyclerItemClick(getAdapterPosition()));
            itemLayoutView.setOnLongClickListener(view -> listener.onRecyclerItemLongClick(getAdapterPosition()));
        }

        void setData(History data, boolean isSelected) {
            // if item is selected change view BG color and play animation to show selected icon
            // otherwise set as normal
            if (isSelected) {
                if (ivSelectedIcon.getVisibility() == View.GONE) {
                    UiUtils.playSelectAnimation(ivSelectedIcon, 0f, 1f);
                }
                contentHolder.setBackgroundColor(ContextCompat.getColor(tvHistoryDate.getContext(), R.color.selected_item_bg));
            } else {
                contentHolder.setBackgroundResource(UiUtils.getSelectedItemBgAttr(tvHistoryDate.getContext()));
                if (ivSelectedIcon.getVisibility() == View.VISIBLE) {
                    UiUtils.playSelectAnimation(ivSelectedIcon, 1f, 0f);
                }
            }

            // if is match then then show the proper plant name and botanical name
            if (data.isMatch()) {
                Plant plant = data.getPlantInfo();
                if (plant != null) {
                    tvPlantHistoryTitle.setText(plant.getCommonName());
                    tvHistorySubtitle.setText(plant.getBotanicalName());
                }
            } else {
                // otherwise show title and subtitle as unidentified
                tvPlantHistoryTitle.setText(R.string.history_unidentified);
                tvHistorySubtitle.setText(R.string.history_no_matches);
            }

            // show formatted date, which can throw exception, if exception just display default format
            try {
                tvHistoryDate.setText(DateUtility.dateToHistoryDateString(data.getIdDate()));
            } catch (ParseException e) {
                tvHistoryDate.setText(data.getIdDate());
                e.printStackTrace();
            }

            if (!Strings.isNullOrEmpty(data.getLocation())) {
                tvHistoryLocation.setText(data.getLocation());
            }

            // TODO: Can remove replace(..) for production
            GlideImageHelper.glideLoadCircleImage(ivHistoryImage, data.getImgUrl().replace("localhost", "10.0.2.2"));
        }
    }
}
