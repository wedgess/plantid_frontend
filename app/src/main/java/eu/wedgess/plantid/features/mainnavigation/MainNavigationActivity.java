/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.mainnavigation;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.custom.arcmenu.ArcMenu;
import eu.wedgess.plantid.custom.dialogs.HelpAckDialog;
import eu.wedgess.plantid.features.cameracapture.CameraCaptureActivity;
import eu.wedgess.plantid.features.classification.ClassificationActivity;
import eu.wedgess.plantid.features.help.HelpActivity;
import eu.wedgess.plantid.features.intro.IntroActivity;
import eu.wedgess.plantid.features.plants.PlantsFragment;
import eu.wedgess.plantid.features.settings.SettingsFragment;
import eu.wedgess.plantid.features.threads.ThreadsFragment;
import eu.wedgess.plantid.features.userdetails.UserDetailsFragment;
import eu.wedgess.plantid.helpers.BottomNavigationViewHelper;
import eu.wedgess.plantid.network.sync.StubProvider;
import eu.wedgess.plantid.utils.FileUtils;
import eu.wedgess.plantid.utils.NetworkUtils;

/**
 * This is the root activity which displays the {@link BottomNavigationView} and is the container
 * for fragments. ({@link UserDetailsFragment}, {@link ThreadsFragment}, {@link PlantsFragment}
 * and {@link SettingsFragment}.
 * <p>
 * This class also handles showing the no internet connection Snackbar.
 * <p>
 * Created by: Gareth Williams
 */
public class MainNavigationActivity extends BaseActivity implements MainNavigationContract.View,
        NetworkUtils.ConnectionReceiverListener, HelpAckDialog.HelpDialogCompleteListener {

    private static final String TAG = MainNavigationActivity.class.getSimpleName();

    private static final String STATE_KEY_TB_TITLE = "STATE_KEY_TB_TITLE";
    public static final String FRAG_TAG_HOME = "FRAG_TAG_HOME";
    public static final String FRAG_TAG_COMMUNITY = "FRAG_TAG_COMMUNITY";
    public static final String FRAG_TAG_PLANTS = "FRAG_TAG_PLANTS";
    public static final String FRAG_TAG_SETTINGS = "FRAG_TAG_SETTINGS";

    private static final int REQUEST_CODE_PERMISSIONS_MULTIPLE = 112;
    public static final int REQUEST_CODE_PERMISSION_STORAGE = 113;
    public static final int REQUEST_CODE_GALLERY_IMAGE = 114;
    public static final int REQUEST_CODE_CAMERA_IMAGE = 115;
    private static final int REQUEST_CODE_PERMISSION_LOCATION = 116;

    //region View binding
    @BindView(R.id.navigation)
    protected BottomNavigationView mBottomNavigation;
    @BindView(R.id.arcMenu)
    protected ArcMenu mArcMenu;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.main_coordinator)
    protected CoordinatorLayout mMainCoordinatorLayout;
    //endregion

    private FloatingActionButton[] mArmMenuItems;
    private Snackbar mNoInternetSnackbar;
    private Fragment[] mFragments;
    private boolean mSnackbarDismissed = false;
    private final String[] mFragmentTags = {FRAG_TAG_HOME, FRAG_TAG_COMMUNITY, FRAG_TAG_PLANTS, FRAG_TAG_SETTINGS};
    private NetworkUtils.ConnectionReceiver mNetworkConnectionReceiver;

    // initializer for setting up fragments
    {
        mFragments = new Fragment[]{
                UserDetailsFragment.newInstance(),
                ThreadsFragment.newInstance(),
                PlantsFragment.newInstance(),
                SettingsFragment.newInstance()
        };
    }

    // listener for bottom navigation menu, which handles changing fragments
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            String title = getString(R.string.app_name);
            String tag = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectedFragment = mFragments[0];
                    title = getString(R.string.fragment_title_home);
                    tag = mFragmentTags[0];
                    break;
                case R.id.navigation_community:
                    selectedFragment = mFragments[1];
                    title = getString(R.string.fragment_title_threads);
                    tag = mFragmentTags[1];
                    break;
                case R.id.navigation_plants:
                    selectedFragment = mFragments[2];
                    title = getString(R.string.fragment_title_plants);
                    tag = mFragmentTags[2];
                    break;
                case R.id.navigation_settings:
                    selectedFragment = mFragments[3];
                    title = getString(R.string.fragment_title_settings);
                    tag = mFragmentTags[3];
                    break;
            }
            if (selectedFragment != null) {
                // get the current fragment & check it is not already visible, if visible do nothing
                Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(tag);
                if (currentFragment != null && currentFragment.isVisible()) {
                    return false;
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.content, selectedFragment, tag).commit();
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(title);
                }
                return true;
            } else {
                return false;
            }
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save actionbar title on orientation change
        if (getSupportActionBar() != null) {
            outState.putString(STATE_KEY_TB_TITLE, getSupportActionBar().getTitle().toString());
        }
        // save if no internet connection snackbar was dismissed or not
        outState.putBoolean(TAG, mSnackbarDismissed);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // restore state
        if (savedInstanceState.containsKey(STATE_KEY_TB_TITLE)) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(savedInstanceState.getString(STATE_KEY_TB_TITLE));
            }
        }
        mSnackbarDismissed = savedInstanceState.getBoolean(TAG);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestLocationPermissions();
        }

        // if first start load the home fragment
        if (savedInstanceState == null) {
            getSupportActionBar().setTitle(getString(R.string.fragment_title_home));
            getSupportFragmentManager().beginTransaction().replace(R.id.content, mFragments[0], FRAG_TAG_HOME).commit();
        } else {
            Fragment frag;
            for (String tag : mFragmentTags) {
                if (getSupportFragmentManager().findFragmentByTag(tag) != null) {
                    frag = getSupportFragmentManager().findFragmentByTag(tag);
                    getSupportFragmentManager().beginTransaction().replace(R.id.content, frag, tag).commit();
                    break;
                }
            }
        }

        // if extra contains key to show help dialog then show it
        if (getIntent().hasExtra(IntroActivity.EXTRA_SHOW_HELP_ALERT)
                && getIntent().getBooleanExtra(IntroActivity.EXTRA_SHOW_HELP_ALERT, false)) {
            // remove extra so dialog won;t be created again - only should ever be shown once
            getIntent().removeExtra(IntroActivity.EXTRA_SHOW_HELP_ALERT);
            new HelpAckDialog().show(getSupportFragmentManager(), "DIALOG_TAG");
        }

        mBottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(mBottomNavigation);
        mArcMenu.setContentLayout(findViewById(R.id.fl_dimmer_view));
        mArmMenuItems = new FloatingActionButton[2];
        buildArcMenu(mArcMenu, new int[]{R.drawable.ic_photo_camera, R.drawable.ic_image_gallery},
                new String[]{getString(R.string.action_camera), getString(R.string.action_gallery)});

        // setup network connection receiver and related snackbar
        mNetworkConnectionReceiver = new NetworkUtils.ConnectionReceiver(this);
        mNoInternetSnackbar = Snackbar.make(mArcMenu,
                R.string.snackbar_msg_no_internet_connection, Snackbar.LENGTH_INDEFINITE);
        mNoInternetSnackbar.setAction(R.string.snackbar_action_dismiss, view -> {
            showNoInternetSnackbar(false);
            mSnackbarDismissed = true;
        });
        mNoInternetSnackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                //see Snackbar.Callback docs for event details
                showNoInternetSnackbar(false);
                mSnackbarDismissed = true;
            }

            @Override
            public void onShown(Snackbar snackbar) {
            }
        });
    }

    /**
     * Builds the arc menu from the drawables and labels passed.
     *
     * @param menu          - arm menu to attach menu items to.
     * @param itemDrawables - the icons in the buttons
     * @param itemLabels    - labels for buttons
     */
    public void buildArcMenu(ArcMenu menu, int[] itemDrawables, final String[] itemLabels) {
        for (int i = 0; i < itemDrawables.length; i++) {
            View base = getLayoutInflater().inflate(R.layout.arc_menu_item, null);
            final FloatingActionButton fab = base.findViewById(R.id.fab_action);
            fab.setImageResource(itemDrawables[i]);
            mArmMenuItems[i] = fab;
            ((TextView) base.findViewById(R.id.tv_tooltip)).setText(itemLabels[i]);
            final String itemLabel = itemLabels[i];
            menu.addItem(base, v -> {
                if (itemLabel.equals(getString(R.string.action_camera))) {
                    // Android M and above requires runtime permissions
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (hasCameraPermissions()) {
                            startCameraActivity();
                        } else {
                            requestCameraPermissions();
                        }
                    } else {
                        startCameraActivity();
                    }
                } else { // Gallery
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (hasStoragePermissions()) {
                            // start Gallery Picker
                            openMediaPicker();
                        } else {
                            requestStoragePermissions();
                        }
                    } else {
                        openMediaPicker();
                    }
                }
            });
        }
    }

    private void openMediaPicker() {
        // open media picker chooser to select image
        Intent intent = new Intent();
        // the type which we will be picking to select an image of any format
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_select_image)), REQUEST_CODE_GALLERY_IMAGE);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    private void startCameraActivity() {
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(this, mArmMenuItems[0], mArmMenuItems[0].getTransitionName());
        ActivityCompat.startActivityForResult(this, new Intent(MainNavigationActivity.this, CameraCaptureActivity.class),
                REQUEST_CODE_CAMERA_IMAGE, options.toBundle());
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showCameraPermissionSnackbar() {
        Snackbar.make(findViewById(android.R.id.content), R.string.permission_grant_camera,
                Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.btn_enable),
                v -> requestCameraPermissions()).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showStoragePermissionSnackbar() {
        Snackbar.make(findViewById(android.R.id.content), R.string.permission_grant_storage,
                Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.btn_enable),
                v -> requestStoragePermissions()).show();
    }

    private void startPlantClassificationActivity(String filePath) {
        Intent intent = new Intent(MainNavigationActivity.this, ClassificationActivity.class);
        intent.putExtra(ClassificationActivity.EXTRA_KEY_IMG_FILE_PATH, filePath);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestStoragePermissions() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_STORAGE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestLocationPermissions() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSION_LOCATION);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestCameraPermissions() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_CODE_PERMISSIONS_MULTIPLE);
    }

    private boolean hasStoragePermissions() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean hasCameraPermissions() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && hasStoragePermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // The user should already have granted permissions during the intro activity
        // incase they didn't we also check that permissions were granted and if not allow them to here
        switch (requestCode) {
            case MainNavigationActivity.REQUEST_CODE_PERMISSIONS_MULTIPLE:
                if (grantResults.length > 0) {
                    boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraPermission && readExternalFile) {
                        // call arcs menu click for first child item in menu which is camera
                        startCameraActivity();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            showCameraPermissionSnackbar();
                        }
                    }
                }
                break;
            case MainNavigationActivity.REQUEST_CODE_PERMISSION_STORAGE:
                if (grantResults.length > 0) {
                    boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (readExternalFile) {
                        openMediaPicker();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            showStoragePermissionSnackbar();
                        }
                    }
                }
        }
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String imgPath = null;
        // if result is from gallery
        if (requestCode == MainNavigationActivity.REQUEST_CODE_GALLERY_IMAGE && data != null
                && data.getData() != null) {
            // get the images real path
            Uri filePath = data.getData();
            try {
                imgPath = FileUtils.getRealPathFromDocumentUri(getApplicationContext(), filePath);
            } catch (URISyntaxException | NumberFormatException e) {
                e.printStackTrace();
                imgPath = FileUtils.getRealPathFromDocumentUriLegacy(getApplicationContext(), filePath);
            }
        } else if (requestCode == MainNavigationActivity.REQUEST_CODE_CAMERA_IMAGE && data != null
                && data.hasExtra(CameraCaptureActivity.EXTRA_RESULT_IMG_DATA)) {
            // otherwise get the image path from the camera capture activity
            imgPath = data.getStringExtra(CameraCaptureActivity.EXTRA_RESULT_IMG_DATA);
        }
        // so long as the image path is not empty or null, start the classification activity
        if (!Strings.isNullOrEmpty(imgPath)) {
            startPlantClassificationActivity(imgPath);
        }
    }

    @Override
    public void setPresenter(@NonNull MainNavigationContract.Presenter presenter) {
//        mMainPresenter = checkNotNull(presenter);
    }

    @Override
    public void onBackPressed() {
        if (!mArcMenu.arcMenuIsExpanded()) {
            super.onBackPressed();
        } else {
            mArcMenu.toggleMenuVisibility();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register network change broadcast receiver to show snackbar
        registerReceiver(mNetworkConnectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mNetworkConnectionReceiver);
    }

    private void showNoInternetSnackbar(boolean show) {
        if (mNoInternetSnackbar != null) {
            // show snackbar is no internet
            if (show) {
                // if not dismissed show snackbar, otherwise ignore
                if (!mSnackbarDismissed) {
                    mArcMenu.setVisibility(View.GONE);
                    if (!mNoInternetSnackbar.isShownOrQueued()) {
                        mNoInternetSnackbar.show();
                    }
                }
            } else {
                // Sync between local and remote for offline changes when connection is established
                ContentResolver.requestSync(NetworkUtils.createDummyAccount(this),
                        StubProvider.AUTHORITY, Bundle.EMPTY);
                // hide snackbar and reshow arc menu button
                mArcMenu.setVisibility(View.VISIBLE);
                mNoInternetSnackbar.dismiss();
                mSnackbarDismissed = false; // reset flag so it can be shown again
            }
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternetSnackbar(!isConnected);
    }

    @Override
    public void onHelpDialogShowHelpClicked() {
        // show help when dialog positive button is clicked
        startActivity(new Intent(MainNavigationActivity.this, HelpActivity.class));
    }
}
