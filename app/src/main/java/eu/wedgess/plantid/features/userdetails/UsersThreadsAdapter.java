/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.userdetails;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.text.ParseException;
import java.util.List;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.DateUtility;
import eu.wedgess.plantid.utils.ProfanityFilter;
import eu.wedgess.plantid.utils.ThreadsDiffCallback;

/**
 * Adapter to diplay list of user threads.
 * Similar to {@link eu.wedgess.plantid.features.threads.ThreadsAdapter}.
 * Used by: {@link UserDetailsFragment}
 * <p>
 * Created by gar on 31/10/17.
 */

public class UsersThreadsAdapter extends RecyclerView.Adapter<UsersThreadsAdapter.ViewHolder> {

    public static final String TAG = UsersThreadsAdapter.class.getSimpleName();

    // recycler click listener
    public interface RecyclerClickListener {
        void onRecyclerItemClick(int position);
    }

    private List<Thread> mThreadsList;
    private boolean mFilterProfanity;
    private RecyclerClickListener mRecyclerClickListener;

    UsersThreadsAdapter(List<Thread> threadList, boolean filterProfanity, RecyclerClickListener recyclerClickListener) {
        this.mThreadsList = threadList;
        this.mFilterProfanity = filterProfanity;
        this.mRecyclerClickListener = recyclerClickListener;
    }

    // Create new views (invoked by the layout manager)
    @SuppressLint("InflateParams")
    @Override
    public UsersThreadsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_my_threads, null), mRecyclerClickListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Thread thread = mThreadsList.get(position);
        viewHolder.setData(thread, mFilterProfanity);
    }

    // Return the size of your mThreadsList (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mThreadsList.size();
    }

    void updateThreadsList(List<Thread> threadList) {
        final ThreadsDiffCallback diffCallback = new ThreadsDiffCallback(this.mThreadsList, threadList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.mThreadsList.clear();
        this.mThreadsList.addAll(threadList);
        diffResult.dispatchUpdatesTo(this);
    }

    public Thread getItemAtPos(int pos) {
        return this.mThreadsList.get(pos);
    }


    // inner class to hold a reference to each item of RecyclerView
    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView threadImage;
        TextView tvThreadTitle, tvThreadMessage, tvThreadDate, tvThreadCommentCount, tvThreadSolved;

        ViewHolder(View itemLayoutView, final RecyclerClickListener listener) {
            super(itemLayoutView);
            tvThreadTitle = itemLayoutView.findViewById(R.id.tv_thread_preview_title);
            tvThreadMessage = itemLayoutView.findViewById(R.id.tv_thread_preview_msg);
            tvThreadDate = itemLayoutView.findViewById(R.id.tv_thread_preview_date);
            tvThreadCommentCount = itemLayoutView.findViewById(R.id.tv_thread_preview_comment_count);
            tvThreadSolved = itemLayoutView.findViewById(R.id.tv_thread_preview_solved);
            threadImage = itemLayoutView.findViewById(R.id.iv_thread_preview_img);
            itemLayoutView.setOnClickListener(v -> listener.onRecyclerItemClick(getAdapterPosition()));
        }

        // set the UI for the data passed in, also the option to filter profanity from threads
        void setData(Thread data, boolean filterProfanity) {
            Log.i(TAG, data.toString());
            tvThreadTitle.setText(filterProfanity ? ProfanityFilter.filter(data.getTitle()) : data.getTitle());
            tvThreadMessage.setText(filterProfanity ? ProfanityFilter.filter(data.getMessage()) : data.getMessage());
            tvThreadCommentCount.setText(String.valueOf(data.getCommentCount()));
            String prettyDate = data.getDateCreated();
            if (!Strings.isNullOrEmpty(prettyDate)) {
                try {
                    prettyDate = DateUtility.getRelativeTime(data.getDateCreated());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                tvThreadDate.setText(prettyDate);
            }

            Drawable drawable;
            if (data.isSolved()) {
                tvThreadSolved.setText(R.string.thread_solved);
                drawable = ContextCompat.getDrawable(tvThreadSolved.getContext(), R.drawable.ic_matched);
            } else {
                tvThreadSolved.setText(R.string.thread_unsolved);
                drawable = ContextCompat.getDrawable(tvThreadSolved.getContext(), R.drawable.ic_unmatched);
            }
            tvThreadSolved.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

            // TODO: Remove for production
            GlideImageHelper.glideLoadCircleImage(threadImage, data.getThreadImgUrl().replace("localhost", "10.0.2.2"));
        }
    }
}
