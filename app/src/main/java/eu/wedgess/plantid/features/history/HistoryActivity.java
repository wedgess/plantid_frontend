/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.history;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.custom.dialogs.ConfirmationDialog;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.source.local.HistoryLocalDataSource;
import eu.wedgess.plantid.data.source.local.PlantsLocalDataSource;
import eu.wedgess.plantid.data.source.remote.HistoryRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.PlantsRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import eu.wedgess.plantid.features.plantdetails.PlantDetailActivity;
import eu.wedgess.plantid.features.userdetails.UserDetailsFragment;
import eu.wedgess.plantid.utils.PlantPreferenceManager;
import eu.wedgess.plantid.utils.SelectableAdapter;
import eu.wedgess.plantid.utils.UiUtils;

/**
 * Activity for displaying history item either matched or unmatched history.
 * When calling this activity either {@link #TYPE_MATCHED} or {@link #TYPE_UNMATCHED}
 * must be passed with the intent.
 * History items are displayed via {@link HistoryAdapter}
 */
public class HistoryActivity extends BaseActivity implements HistoryContract.View,
        HistoryAdapter.RecyclerClickListener,
        ActionMode.Callback,
        ConfirmationDialog.ConfirmationDialogCompleteListener {

    private static final String TAG = HistoryActivity.class.getSimpleName();

    public static final int TYPE_MATCHED = 0;
    public static final int TYPE_UNMATCHED = 1;
    public static final String EXTRA_ITEMS_DELETED = "EXTRA_ITEMS_DELETED";
    private static final String STATE_LIST = "STATE_LIST";
    private static final String STATE_ITEMS_DELETED = "STATE_ITEMS_DELETED";

    //region View binding
    @BindView(R.id.rv_history)
    protected RecyclerView mHistoryListRV;
    @BindView(R.id.pb_loading_history)
    protected ProgressBar mLoadingPB;
    @BindView(R.id.tv_no_history_data)
    protected TextView mNoHistoryDataTV;
    //endregion

    private HistoryContract.Presenter mPresenter;
    private ActionMode mActionMode;
    private int mHistoryType = TYPE_MATCHED;
    private boolean mHistoryItemsHaveBeenDeleted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        mHistoryType = getIntent().getIntExtra(UserDetailsFragment.EXTRA_KEY_HISTORY_TYPE, TYPE_MATCHED);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setActionbarTitle(getString(R.string.history_type_ab_title, mHistoryType == TYPE_MATCHED
                ? getString(R.string.home_matched) : getString(R.string.home_unmatched)));

        // set up presenter
        AppDatabase database = AppDatabase.getInstance(HistoryActivity.this);
        PlantsRepository plantsRepository = PlantsRepository.getInstance(
                new PlantsLocalDataSource(database.plantDao()),
                new PlantsRemoteDataSource());
        new HistoryPresenter(
                HistoryRepository.getInstance(new HistoryLocalDataSource(database.historyDao()),
                        new HistoryRemoteDataSource()), plantsRepository, this, mHistoryType);

        // subscribe and load history on first load
        if (savedInstanceState == null) {
            mPresenter.subscribe();
        } else {
            // on config changes restore lists state
            ArrayList<History> list = savedInstanceState.getParcelableArrayList(STATE_LIST);
            setHistoryData(list);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull HistoryContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void showNoHistoryData() {
        mLoadingPB.setVisibility(View.GONE);
        mHistoryListRV.setVisibility(View.GONE);
        mNoHistoryDataTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void setHistoryData(List<History> historyList) {
        setLoadingIndicator(false);
        mHistoryListRV.setVisibility(View.VISIBLE);
        mHistoryListRV.setAdapter(new HistoryAdapter(historyList, this));
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mHistoryListRV.setVisibility(View.GONE);
            mNoHistoryDataTV.setVisibility(View.GONE);
            mLoadingPB.setVisibility(View.VISIBLE);
        } else {
            mLoadingPB.setVisibility(View.GONE);
        }
    }

    @Override
    public void showLoadingHistoryError() {
        mLoadingPB.setVisibility(View.GONE);
        mHistoryListRV.setVisibility(View.GONE);
        mNoHistoryDataTV.setVisibility(View.VISIBLE);
        mNoHistoryDataTV.setText(R.string.error_loading_history);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    @Override
    public void onRecyclerItemClick(int position) {
        if (mActionMode != null && mHistoryListRV.getAdapter() != null) {
            // if action mode is started already toggle items selection
            toggleItemSelection(position);
        } else {
            if (mHistoryType == TYPE_MATCHED) {
                openPlantDetailsUi(position);
            }
        }
    }

    private void toggleItemSelection(int position) {
        ((SelectableAdapter) mHistoryListRV.getAdapter()).toggleSelection(position);
        setActionModeTitle();
    }

    @Override
    public boolean onRecyclerItemLongClick(int position) {
        startActionMode();
        toggleItemSelection(position);
        return true;
    }

    private void openPlantDetailsUi(int position) {
        if (mHistoryListRV.getAdapter() != null) {
            History item = ((HistoryAdapter) mHistoryListRV.getAdapter()).getItemAtPos(position);
            if (item != null && !item.getPlantId().equals("-1")) {
                Intent intent = new Intent(HistoryActivity.this, PlantDetailActivity.class);
                intent.putExtra(PlantDetailActivity.EXTRA_PLANT_ID, item.getPlantId());
                startActivity(intent);
                overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save adapters state on orientation change
        if (mHistoryListRV.getAdapter() != null) {
            ((SelectableAdapter) mHistoryListRV.getAdapter()).onSaveInstanceState(outState);
            outState.putParcelableArrayList(STATE_LIST, ((HistoryAdapter) mHistoryListRV.getAdapter()).getList());
        }
        outState.putBoolean(STATE_ITEMS_DELETED, mHistoryItemsHaveBeenDeleted);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (mHistoryListRV.getAdapter() != null) {
            // restore adapters state on orientation change
            ((SelectableAdapter) mHistoryListRV.getAdapter()).onRestoreInstanceState(savedInstanceState);
            if (((SelectableAdapter) mHistoryListRV.getAdapter()).getMode() == SelectableAdapter.MODE_MULTI) {
                startActionMode();
            }
        }
        mHistoryItemsHaveBeenDeleted = savedInstanceState.getBoolean(STATE_ITEMS_DELETED);
    }

    //region Action mode
    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        // inflate menu for subclass
        actionMode.getMenuInflater().inflate(R.menu.cab_history, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.action_delete_selected) {
            // if show confirmation dialog is set in settings
            if (PlantPreferenceManager.showDeleteConfirmation(HistoryActivity.this)) {
                ConfirmationDialog.newInstance(getString(R.string.dialog_title_delete_history),
                        getString(R.string.dialog_msg_delete_history))
                        .show(getSupportFragmentManager(), "KEY_CONFIRM_DIALOG");
            } else {
                // otherwise delete items without confirmation
                deleteSelectedItems();
                mActionMode.finish();
            }
        } else if (menuItem.getItemId() == R.id.action_select_all) {
            // toggle select all, if all are already selected then unselect all
            ((HistoryAdapter) mHistoryListRV.getAdapter()).selectAllHistoryItems();
            setActionModeTitle();
        }
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        // reset adapter, actionmode and reset status bar colour
        mActionMode = null;
        ((SelectableAdapter) mHistoryListRV.getAdapter()).clearSelection();
        ((SelectableAdapter) mHistoryListRV.getAdapter()).setMode(SelectableAdapter.MODE_SINGLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(HistoryActivity.this, R.color.colorPrimaryDark));
        }
    }

    public void startActionMode() {
        // start actionmode if it is not already started
        if (mActionMode == null) {
            // change adapters selection mode
            ((SelectableAdapter) mHistoryListRV.getAdapter()).setMode(SelectableAdapter.MODE_MULTI);
            mActionMode = startActionMode(this);
            setActionModeTitle();
            getWindow().setStatusBarColor(UiUtils.darkenColor(ContextCompat.getColor(HistoryActivity.this, R.color.black)));
        }
    }

    // set the action modes title
    public void setActionModeTitle() {
        // if selected item count greater than 0 show number of selected items and show done icon
        if (((SelectableAdapter) mHistoryListRV.getAdapter()).getSelectedItemCount() > 0) {
            mActionMode.setTitle(getString(R.string.selected_history, ((SelectableAdapter) mHistoryListRV.getAdapter()).getSelectedItemCount()));
        } else {
            mActionMode.setTitle(R.string.select_history);
        }
    }
    //endregion

    private void deleteSelectedItems() {
        List<History> itemsToDelete = new ArrayList<>();
        List<Integer> itemstToDeletePositions = ((SelectableAdapter) mHistoryListRV.getAdapter()).getSelectedItems();
        for (Integer pos : itemstToDeletePositions) {
            itemsToDelete.add(((HistoryAdapter) mHistoryListRV.getAdapter()).getItemAtPos(pos));
        }
        if (!itemsToDelete.isEmpty()) {
            mPresenter.deleteHistoryItems(itemsToDelete);
            ((HistoryAdapter) mHistoryListRV.getAdapter()).deleteItems(itemsToDelete);
        }

        // clear selection and finish action mode after item is pressed
        ((SelectableAdapter) mHistoryListRV.getAdapter()).clearSelection();
        ((SelectableAdapter) mHistoryListRV.getAdapter()).setMode(SelectableAdapter.MODE_SINGLE);
        if (!mHistoryItemsHaveBeenDeleted) {
            mHistoryItemsHaveBeenDeleted = true;
        }
    }

    @Override
    public void onConfirmationDialogConfirmClicked() {
        deleteSelectedItems();
        mActionMode.finish();
    }

    // Override from super class as we are sending result back to caller (UserDetailsFragment)
    @Override
    public void finish() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(EXTRA_ITEMS_DELETED, mHistoryItemsHaveBeenDeleted);
        setResult(UserDetailsFragment.REQUEST_CODE_HISTORY, resultIntent);
        super.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
