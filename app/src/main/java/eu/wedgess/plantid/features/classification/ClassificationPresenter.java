/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.classification;

import android.util.Log;

import com.google.common.base.Strings;

import java.io.File;
import java.util.List;

import eu.wedgess.plantid.data.Classifier;
import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.repositories.ClassificationRepository;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link ClassificationActivity} calls {@link TensorflowImageClassifier} to classify
 * the image and passes results back tot he View.
 * <p>
 * Created by gar on 09/11/17.
 */

public class ClassificationPresenter implements ClassificationContract.Presenter {

    private static final String TAG = ClassificationPresenter.class.getSimpleName();

    private final ClassificationContract.View mClassificationView;
    private final ClassificationRepository mRepository;
    private final PlantsRepository mPlantsRepository;
    private final HistoryRepository mHistoryRepository;
    private final CompositeDisposable mCompositeDisposable;

    ClassificationPresenter(ClassificationContract.View classificationView, ClassificationRepository repository,
                            PlantsRepository plantsRepository, HistoryRepository historyRepository) {
        this.mClassificationView = checkNotNull(classificationView);
        this.mClassificationView.setPresenter(this);
        this.mRepository = checkNotNull(repository);
        this.mPlantsRepository = checkNotNull(plantsRepository);
        this.mHistoryRepository = checkNotNull(historyRepository);
        this.mCompositeDisposable = new CompositeDisposable();
    }

    // process results of classification, no classification data then show no matched view
    private void processClassifications(final List<Classifier.Recognition> classifications) {
        if (classifications.isEmpty()) {
            // Show a message indicating there are no tasks for that filter type.
            mClassificationView.showNoMatchesView();
        } else {
            mClassificationView.setClassificationData(classifications);
        }
    }

    /**
     * Load classification results, by running the image through tensorflow lite. Load the plant
     * into the {@link Classifier.Recognition} object so the image is displayed in list. Then
     * sort the list with the highest confidence level first.
     *
     * @param location - users location to save on history item
     * @param uid      - users id
     * @param img      - the img file to be classified
     */
    @SuppressWarnings("Guava")
    private void loadClassificationResults(String location, String uid, File img) {
        mCompositeDisposable.add(mRepository.getClassificationResults(location, uid, img)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flattenAsObservable(recognitions -> recognitions)
                .flatMap(recognition ->
                        mPlantsRepository
                                .getPlantById(recognition.getPlantId())
                                .toObservable()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .map(plant -> {
                                    // plant is an optional so check it is present
                                    if (plant.isPresent()) {
                                        recognition.setPlantInfo(plant.get());
                                    }
                                    return recognition;
                                }))
                .toSortedList((recognition, t1) ->
                        t1.getConfidence() < recognition.getConfidence() ? -1
                                : t1.getConfidence() > recognition.getConfidence() ? 1 : 0)
                .subscribe(classificationResults -> {
                    mClassificationView.setLoadingIndicator(false);
                    processClassifications(classificationResults);
                }, throwable -> {
                    Log.e(TAG, "Error getting classification results");
                    mClassificationView.showLoadingMatchesError("Error getting results");
                    // handle error event
//                    throw Exceptions.propagate(throwable);
                }));
    }

    @Override
    public void openPlantDetailsActivity(Plant plant) {
        checkNotNull(plant, "requestedPlant cannot be null!");
        mClassificationView.showPlantDetailsUi(plant.getId());
    }

    /**
     * Called when activity is originally called. Upload the image on the IO thread if user is logged in
     * Then load the image classification results.
     *
     * @param location
     * @param imgPath
     */
    @Override
    public void startClassification(String location, String imgPath) {
        mClassificationView.setLoadingIndicator(true);
        final File img = new File(imgPath);
        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
        String uid = currentUser == null ? null : currentUser.getUid();
        // if user is signed in then upload the image to be classified as a history item
        if (uid != null) {
            mCompositeDisposable.add(mRepository.uploadIdentificationImage(location, uid, img)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(history -> {
                        Log.e(TAG, "Updating history item: " + history.toString());
                        mCompositeDisposable.add(mHistoryRepository.updateHistoryItem(history)
                                .observeOn(Schedulers.io())
                                .subscribe(() -> Log.i(TAG, "History item inserted successfully"),
                                        throwable -> {
                                            Log.e(TAG, "Error updating history item");
//                                            throw Exceptions.propagate(throwable);
                                        }));

                    }, throwable -> {
                        Log.e(TAG, "Error uploading history item -- user probably offline");
                    }));
        }

        loadClassificationResults(location, uid, img);
    }

    /**
     * If an item is matched then the history item needs to be updated on remote so it is matched.
     *
     * @param plantId
     */
    @Override
    public void updateHistoryItem(String plantId) {
        History history = updateHistoryItemWithPlant(plantId);
        if (history != null) {
            mCompositeDisposable.add(mHistoryRepository.updateHistoryItem(history)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .subscribe(() -> Log.i(TAG, "History item updated successfully"),
                            throwable -> {
                                Log.e(TAG, "Error updating history item");
//                                Exceptions.propagate(throwable);
                            }));
        }
    }

    @Override
    public String getHistoryItemImgUrl() {
        History item = mRepository.getHistoryItem();
        if (item == null) {
            return null;
        } else {
            return item.getImgUrl();
        }
    }

    private History updateHistoryItemWithPlant(String plantId) {
        History mHistoryItem = mRepository.getHistoryItem();
        if (mHistoryItem != null && !Strings.isNullOrEmpty(plantId)) {
            mHistoryItem.setPlantId(plantId);
            mHistoryItem.setMatch(true);
            Log.i(TAG, "Updated history item.");
            return mHistoryItem;
        } else {
            Log.i(TAG, "Error updating history item.");
            return null;
        }
    }

    @Override
    public void subscribe() {
        //IGNORE
        if (CurrentUserInstance.getInstance().getCurrentUser() == null) {
            mClassificationView.hideMatchAndCreateThreadButtons();
        }
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }
}
