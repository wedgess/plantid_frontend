/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threaddetails;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.custom.dialogs.ConfirmationDialog;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.local.CommentsLocalDataSource;
import eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource;
import eu.wedgess.plantid.data.source.local.UsersLocalDataSource;
import eu.wedgess.plantid.data.source.remote.CommentsRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.ThreadsRemoteDataSource;
import eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.data.source.repositories.ThreadsRepository;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.features.createeditcomment.CreateEditCommentActivity;
import eu.wedgess.plantid.features.createeditthread.CreateEditThreadActivity;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.PlantPreferenceManager;
import eu.wedgess.plantid.utils.ProfanityFilter;

/**
 * Activity to display thread details and the threads comment.
 * Called by {@link eu.wedgess.plantid.features.threads.ThreadsFragment}
 */
public class ThreadDetailActivity extends BaseActivity implements ThreadDetailContract.View, View.OnClickListener,
        ConfirmationDialog.ConfirmationDialogCompleteListener {

    private static final String TAG = ThreadDetailActivity.class.getSimpleName();

    public static final String EXTRA_THREAD_ID = "EXTRA_THREAD_ID";
    public static final String EXTRA_COMMENT_RESULT = "EXTRA_COMMENT_RESULT";
    public static final String EXTRA_COMMENT_POSITION = "EXTRA_COMMENT_POSITION";
    public static final String EXTRA_COMMENT_MSG = "EXTRA_COMMENT_MSG";
    private static final int REQUEST_CODE_COMMENTS = 112;

    //region View binding
    @BindView(R.id.rv_thread_comments)
    RecyclerView mCommentsRV;
    @BindView(R.id.thread_collapsing_tb)
    CollapsingToolbarLayout mCollapsingTBLayout;
    @BindView(R.id.tv_thread_title_detail)
    TextView mThreadTitleTV;
    @BindView(R.id.tv_thread_message_detail)
    TextView mThreadMessageTV;
    @BindView(R.id.thread_header_iv)
    ImageView mThreadHeaderImageIV;
    @BindView(R.id.tv_thread_detail_author)
    TextView mThreadAuthorTV;
    @BindView(R.id.tv_thread_detail_location)
    TextView mThreadLocationTV;
    @BindView(R.id.tv_no_comments_data)
    TextView mNoCommentMessageTV;
    @BindView(R.id.tv_thread_detail_date)
    TextView mThreadDetailDateTV;
    @BindView(R.id.tv_comments_title)
    TextView mThreadCommentsTitleTV;
    @BindView(R.id.fab_write_comment)
    FloatingActionButton mFabWriteComment;
    @BindView(R.id.pb_loading_thread_details)
    ProgressBar mThreadDetailsPB;
    @BindView(R.id.pb_loading_comments)
    ProgressBar mThreadCommentsPB;
    @BindView(R.id.cv_thread_info)
    CardView mThreadInfoCV;
    @BindView(R.id.thread_details_op_panel)
    RelativeLayout mThreadAuthorControlsLayout;
    @BindView(R.id.op_panel_divider)
    View mOpControlsDivider;
    @BindView(R.id.ib_edit_thread_op)
    ImageButton mEditThreadIB;
    @BindView(R.id.ib_delete_thread_op)
    ImageButton mDeleteThreadIB;
    @BindView(R.id.iv_thread_author_img)
    ImageView mThreadAuthorProfileImgIV;
    //endregion

    private ThreadDetailContract.Presenter mPresenter;
    private boolean isThreadAuthor = false;
    private boolean mFilterProfanity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_details);

        ButterKnife.bind(this);
        Toolbar tb = findViewById(R.id.toolbar_thread);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFilterProfanity = PlantPreferenceManager.filterProfanity(ThreadDetailActivity.this);

        mEditThreadIB.setOnClickListener(this);
        mDeleteThreadIB.setOnClickListener(this);

        // Get the requested plant id
        String threadId = getIntent().getStringExtra(EXTRA_THREAD_ID);

        AppDatabase database = AppDatabase.getInstance(ThreadDetailActivity.this);

        UserRepository userRepository = UserRepository.getInstance(
                new UsersLocalDataSource(database.userDao(),
                        PreferenceManager.getDefaultSharedPreferences(this)),
                new UsersRemoteDataSource());
        new ThreadDetailPresenter(threadId, userRepository,
                CommentsRepository.getInstance(
                        new CommentsLocalDataSource(database.commentDao()),
                        new CommentsRemoteDataSource()),
                ThreadsRepository.getInstance(
                        new ThreadsLocalDataSource(database.threadDao()),
                        new ThreadsRemoteDataSource()), this);

        setAppbarOffsetListener();
        mFabWriteComment.setOnClickListener(__ -> mPresenter.canPostComment());
    }

    private void setAppbarOffsetListener() {
        AppBarLayout appBarLayout = findViewById(R.id.app_bar_thread_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset < 120) {
                    mCollapsingTBLayout.setTitle(mThreadTitleTV.getText().toString());
                    isShow = true;
                } else if (isShow) {
                    mCollapsingTBLayout.setTitle(" ");// careful there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }


    // sets all the bundle information in intent and starts the create/edit comment activity
    private void startCommentActivity(String threadId, String commentId, int position) {
        Bundle bundle = new Bundle();
        bundle.putString(CreateEditCommentActivity.EXTRA_KEY_THREAD_ID, threadId);
        bundle.putString(CreateEditCommentActivity.EXTRA_KEY_COMMENT_ID, commentId);
        bundle.putInt(CreateEditCommentActivity.EXTRA_KEY_COMMENT_POSITION, position);
        Intent intent = new Intent(ThreadDetailActivity.this, CreateEditCommentActivity.class);
        intent.putExtra(CreateEditCommentActivity.EXTRA_KEY_COMMENT_BUNDLE, bundle);
        startActivityForResult(intent, REQUEST_CODE_COMMENTS);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @Override
    public void setPresenter(@NonNull ThreadDetailPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setCommentsUi(@NonNull List<Comment> commentList) {
        mNoCommentMessageTV.setVisibility(View.GONE);
        setCommentsLoadingIndicator(false);
        if (mCommentsRV.getAdapter() == null) {
            setCommentsAdapter(commentList);
        }
    }

    private void setCommentsAdapter(@NonNull List<Comment> commentList) {
        mCommentsRV.setVisibility(View.VISIBLE);
        mCommentsRV.setAdapter(new ThreadCommentsAdapter(commentList, isThreadAuthor, mPresenter.isThreadSolved(),
                mFilterProfanity, new ThreadCommentsAdapter.RecyclerClickListener() {
            @Override
            public void onCommentAnswerAccepted(int position) {
                mPresenter.setCommentAsAnswer(getCommentsAdapter().getCommentItem(position));
                // set thread as solved as a comment was selected
                getCommentsAdapter().setThreadSolved(true);
                // notify adapter of data changes
                mCommentsRV.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onEditCommentClicked(int position) {
                Comment comment = getCommentsAdapter().getCommentItem(position);
                if (comment != null) {
                    startCommentActivity(getIntent().getStringExtra(EXTRA_THREAD_ID), comment.getId(), position);
                }

            }

            @Override
            public void onDeleteCommentClicked(int position) {
                Comment comment = ((ThreadCommentsAdapter) mCommentsRV.getAdapter()).getCommentItem(position);
                if (comment != null) {
                    mPresenter.deleteComment(comment, position);
                }
            }
        }));
    }

    @Override
    public void showMissingThread() {
        setCommentsLoadingIndicator(false);
        mCommentsRV.setVisibility(View.GONE);
        mNoCommentMessageTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void setThreadLoadingIndicator(boolean isLoading) {
        if (isLoading) {
            mThreadDetailsPB.setVisibility(View.VISIBLE);
            mThreadInfoCV.setVisibility(View.INVISIBLE);
            mCommentsRV.setVisibility(View.GONE);
            mThreadCommentsTitleTV.setVisibility(View.GONE);
        } else {
            mThreadCommentsPB.setVisibility(View.VISIBLE);
            mThreadDetailsPB.setVisibility(View.GONE);
            mThreadCommentsTitleTV.setVisibility(View.VISIBLE);
            mThreadInfoCV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setCommentsLoadingIndicator(boolean isLoading) {
        if (isLoading) {
            mThreadCommentsPB.setVisibility(View.VISIBLE);
            mCommentsRV.setVisibility(View.GONE);
        } else {
            mThreadCommentsPB.setVisibility(View.GONE);
            mCommentsRV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setThreadTitle(@NonNull String title) {
        mThreadTitleTV.setText(mFilterProfanity ? ProfanityFilter.filter(title) : title);
    }

    @Override
    public void setThreadMessage(@NonNull String message) {
        mThreadMessageTV.setText(mFilterProfanity ? ProfanityFilter.filter(message) : message);
    }

    @Override
    public void setThreadAuthor(@NonNull String author) {
        mThreadAuthorTV.setText(author);
    }

    @Override
    public void setThreadAuthorProfileImage(@NonNull String imgUrl) {
        GlideImageHelper.glideLoadCircleImage(mThreadAuthorProfileImgIV, imgUrl);
    }

    @Override
    public void setThreadImage(@NonNull String imgUrl) {
        GlideImageHelper.glideLoadImage(mThreadHeaderImageIV, imgUrl);
    }

    @Override
    public void setThreadLocation(@NonNull String location) {
        mThreadLocationTV.setText(location);
    }

    @Override
    public void setThreadDate(String date) {
        mThreadDetailDateTV.setText(date);
    }


    @Override
    public void showNoComments() {
        setCommentsLoadingIndicator(false);
        mThreadCommentsTitleTV.setVisibility(View.GONE);
        mNoCommentMessageTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void setIsThreadAuthor(@NonNull String authorId) {
        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
        if (currentUser != null) {
            if (authorId.equals(currentUser.getUid())) {
                this.isThreadAuthor = true;
                mOpControlsDivider.setVisibility(View.VISIBLE);
                mThreadAuthorControlsLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void openEditThreadDetails(@NonNull String threadId) {
        Intent intent = new Intent(ThreadDetailActivity.this, CreateEditThreadActivity.class);
        intent.putExtra(CreateEditThreadActivity.EXTRA_KEY_EDIT_THREAD_ID, threadId);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @Override
    public void threadDeleteComplete() {
        finish();
    }

    @Override
    public void lockThread() {
        // thread is answered no replies accepted
        mFabWriteComment.setVisibility(View.GONE);
    }

    @Override
    public boolean adapterIsNotCreated() {
        return mCommentsRV.getAdapter() == null;
    }

    @Override
    public void updateCommentsUi(@NonNull List<Comment> comments) {
        ThreadCommentsAdapter adapter = getCommentsAdapter();
        if (adapter != null) {
            adapter.updateCommentsList(comments);
        }
    }

    @Override
    public void commentDeleted(int position) {
        ThreadCommentsAdapter adapter = getCommentsAdapter();
        if (adapter != null) {
            adapter.removeCommentItem(position);
            if (adapter.getItemCount() == 0) {
                showNoComments();
            }
        }
    }

    @Override
    public void openCreateCommentActivity() {
        startCommentActivity(getIntent().getStringExtra(EXTRA_THREAD_ID), null, -1);
    }

    @Override
    public void showNotLoggedInError() {
        Toast.makeText(ThreadDetailActivity.this, R.string.toast_msg_no_internet_comments, Toast.LENGTH_LONG).show();
    }

    private ThreadCommentsAdapter getCommentsAdapter() {
        if (adapterIsNotCreated()) {
            return null;
        } else {
            return ((ThreadCommentsAdapter) mCommentsRV.getAdapter());
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ib_edit_thread_op) {
            mPresenter.editThread();
        } else if (view.getId() == R.id.ib_delete_thread_op) {
            if (PlantPreferenceManager.showDeleteConfirmation(ThreadDetailActivity.this)) {
                ConfirmationDialog.newInstance(getString(R.string.dialog_title_delete_thread), getString(R.string.dialog_msg_delete_thread))
                        .show(getSupportFragmentManager(), "DELETE_THREAD_DIALOG");
            } else {
                mPresenter.deleteThread();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_COMMENTS) {
            if (resultCode == RESULT_OK) {
                // when comment activity is finished get bundle info to update comment
                Bundle resultBundle = data.getBundleExtra(EXTRA_COMMENT_RESULT);
                int position = resultBundle.getInt(EXTRA_COMMENT_POSITION);
                String updatedMsg = resultBundle.getString(EXTRA_COMMENT_MSG);
                ThreadCommentsAdapter adapter = getCommentsAdapter();
                if (adapter != null) {
                    adapter.updateComment(position, updatedMsg);
                }
            }
        }
    }

    @Override
    public void onConfirmationDialogConfirmClicked() {
        mPresenter.deleteThread();
    }
}
