/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.login;

import android.support.annotation.NonNull;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;

/**
 * Contract between {@link LoginActivity} & {@link LoginPresenter}
 * <p>
 * Created by gar on 27/12/17.
 */

class LoginContract {

    interface View extends BaseView<Presenter> {

        void setLoggingInIndicator(boolean active);

        void setLoginErrorText(boolean showError);

        void setUsernameEmptyError();

        void setPasswordEmptyError();


        void setSuccessfulLogin();

    }

    interface Presenter extends BasePresenter {
        void performLogin(@NonNull String username, @NonNull String password);

        boolean validateLoginInfo(@NonNull String username, @NonNull String password);

    }
}
