/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threads;

/**
 * Filter used by {@link ThreadsFragment} and {@link ThreadsPresenter} to filter threads.
 * <p>
 * Created by gar on 28/01/18.
 */

public enum ThreadsFilterType {

    ALL_THREADS("All"),
    SOLVED_THREADS("Solved"),
    UNSOLVED_THREADS("Unsolved"),
    NO_COMMENTED_THREADS("No comments");


    ThreadsFilterType(String tag) {
        this.type = tag;
    }

    private String type;

    public String getStringType() {
        return type;
    }
}
