/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.createeditcomment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.base.Strings;

import java.util.UUID;

import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.repositories.CommentsRepository;
import eu.wedgess.plantid.features.createeditthread.CreateEditThreadPresenter;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import eu.wedgess.plantid.utils.DateUtility;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link CreateEditCommentActivity}
 * <p>
 * Created by gar on 03/01/18.
 */

public class CreateEditCommentPresenter implements CreateEditCommentContract.Presenter {

    public static final String TAG = CreateEditThreadPresenter.class.getSimpleName();

    private final CreateEditCommentContract.View mCommentView;
    private final CommentsRepository mRepository;
    private final CompositeDisposable mCompositeDisposable;

    @Nullable
    private String mCommentId;
    @NonNull
    private String mThreadId;


    CreateEditCommentPresenter(@NonNull String threadId, @Nullable String commentId, @NonNull CommentsRepository commentsRepository,
                               @NonNull CreateEditCommentContract.View view) {
        this.mThreadId = checkNotNull(threadId);
        this.mCommentId = commentId;
        this.mCommentView = checkNotNull(view);
        this.mRepository = checkNotNull(commentsRepository);
        this.mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        if (!isNewComment()) {
            checkNotNull(mCommentId, "Editing comment but comment ID is NULL");
            checkNotNull(mThreadId, "Editing comment but thread ID is NULL");
            mCompositeDisposable.add(mRepository.getCommentById(mThreadId, mCommentId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::loadCommentUi, throwable -> {
                        Log.e(TAG, "Error retrieving comment by id");
//                        Exceptions.propagate(throwable);
                    }));

        }
    }

    // check if new or update comment
    private boolean isNewComment() {
        return mCommentId == null;
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    private void loadCommentUi(@NonNull Comment comment) {
        if (!Strings.isNullOrEmpty(comment.getMessage())) {
            mCommentView.setCommentMessage(comment.getMessage());
        }
    }

    @Override
    public boolean validateCommentMessage(@NonNull String message) {
        if (Strings.isNullOrEmpty(message)) {
            mCommentView.setCommentEmptyError();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void submitComment(@NonNull String message, boolean isOffline) {
        checkNotNull(mThreadId, "Thread ID cannot be NULL");
        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();

        // if signed in insert comment
        if (currentUser != null) {
            if (isNewComment()) {
                createNewComment(message, currentUser.getUid(), isOffline);
            } else {
                checkNotNull(mCommentId, "Comment id to update cannot be NULL");
                updateComment(message, isOffline);
            }
        }
    }

    private void updateComment(@NonNull String message, boolean isOffline) {
        checkNotNull(mCommentId, "Comment ID cannot be NULL");
        mCompositeDisposable.add(mRepository.getCommentById(mThreadId, mCommentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(comment -> {
                    comment.setMessage(message);
                    if (isOffline) {
                        comment.setDirty(true);
                    }
                    mCompositeDisposable.add(mRepository.updateComment(comment)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> mCommentView.commentUpdateSuccessful(comment), throwable -> {
                                Log.e(TAG, "Error updating comment");
//                                throw Exceptions.propagate(throwable);
                            }));
                }, throwable -> {
                    Log.e(TAG, "Error retrieving comment by id");
//                            throw Exceptions.propagate(throwable);
                }));
    }

    private void createNewComment(@NonNull String message, @NonNull String userId, boolean isOffline) {
        Comment comment = new Comment(UUID.randomUUID().toString(), message, userId,
                DateUtility.getDBDateFormat(), mThreadId, false, isOffline);
        mCompositeDisposable.add(mRepository.insertOrUpdateComment(comment)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mCommentView::commentInsertSuccessful, throwable -> {
                    // if offline we will get an exception here so close activity regardless as if offline wil be synced later
                    mCommentView.commentInsertSuccessful();
                    Log.e(TAG, "Error inserting comment, possibly offline");
//                            throw Exceptions.propagate(throwable);
                }));
    }
}
