/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Rule;
import org.junit.Test;

import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.utils.JsonFileResource;
import eu.wedgess.plantid.utils.JsonParsingRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test parsing threads JSON to thread object {@link Thread}
 * Created by gar on 07/03/18.
 */

public class ParseThreadsTest {

    private static final Gson GSON;

    static {
        GSON = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Rule
    public JsonParsingRule jsonParsingRule = new JsonParsingRule(GSON);

    @Test
    @JsonFileResource(fileName = "threads.json", clazz = Thread[].class)
    public void testGetThreads() throws Exception {
        Thread[] threads = jsonParsingRule.getValue();
        // should have parsed 3 threads
        assertThat(threads).hasSize(3);
        // ensure that the first thread item has title "Dead plant??"
        assertThat(threads[0].getTitle()).isEqualTo("Dead plant??");
    }
}