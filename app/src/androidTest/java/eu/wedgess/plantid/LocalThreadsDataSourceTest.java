/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource;

/**
 * Integration tests for the {@link ThreadsLocalDataSource} implementation with Room.
 * <p>
 * Created by gar on 07/03/18.
 */
public class LocalThreadsDataSourceTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private static final Thread THREAD = new Thread("24f15v13-n61z-494b-8fse-4ba1gc19a331", "Thread title", "Thread messgae", "12w02b36-a40a-494b-8dde-0ba3dc19a883",
            "2018-02-16T01:14:47+00:00", "http://garethwilliams.host:5000/static/images/profile/66b02b36-a40a-494b-8dde-0ba3dc19a883_25.jpg",
            "Deansgrange", false, false);

    private ThreadsLocalDataSource mDataSource;

    @Before
    public void initDb() throws Exception {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        AppDatabase mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                AppDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();
        mDataSource = new ThreadsLocalDataSource(mDatabase.threadDao());
    }

    @Test
    public void insertAndGetThread() {
        // When inserting a new thread in the data source, need to block and wait til thread is inserted
        mDataSource.insertThread(THREAD).blockingAwait();

        mDataSource
                .getThreadById(THREAD.getId())
                .test()
                // assertValue asserts that there was only one emission of the thread
                .assertValue(thread -> {
                    // The emitted thread is the expected one
                    return thread != null && thread.getId().equals(THREAD.getId()) &&
                            thread.getTitle().equals(THREAD.getTitle());
                });
    }

    @Test
    public void updateAndGetThread() {
        // Given that we have a thread in the data source
        mDataSource.insertThread(THREAD).blockingAwait();

        // When we are updating the name of the thread
        Thread updatedThread = new Thread(THREAD.getId(), THREAD.getTitle(), "New message", THREAD.getAuthorId(), THREAD.getDateCreated(),
                THREAD.getThreadImgUrl(), THREAD.getLocation(), THREAD.isSolved(), false);
        mDataSource.updateThread(updatedThread).blockingAwait();

        // When subscribing to the emissions of the thread
        mDataSource
                .getThreadById(THREAD.getId())
                .test()
                // assertValue asserts that there was only one emission of the thread
                .assertValue(thread -> {
                    // The emitted user is the expected one
                    return thread != null && thread.getId().equals(THREAD.getId()) &&
                            thread.getMessage().equals("New message");
                });
    }

    @Test
    public void deleteAndGetThread() {
        // Given that we have a thread in the data source
        mDataSource.insertThread(THREAD).blockingAwait();

        // When we are deleting the thread
        mDataSource.deleteThread(THREAD).blockingAwait();

        // get the emissions of the thread
        mDataSource
                .getThreadById(THREAD.getId())
                .test()
                // check that there's no user emitted
                .assertNoValues();
    }

    @Test
    public void insertAndGetThreads() {
        // Given that we have a thread in the data source
        mDataSource.insertThread(THREAD).blockingAwait();

        // get the emissions of the thread
        mDataSource.getThreads()
                .test()
                // check that there's no user emitted
                .assertValue(threads -> !threads.isEmpty() && threads.size() == 1);
    }

    @Test
    public void insertAndGetThreadByAuthorId() {
        // insert the thread
        mDataSource.insertThread(THREAD).blockingAwait();

        // get the thread by author id
        mDataSource
                .getThreadsByAuthorId(THREAD.getAuthorId())
                .test()
                .assertValue(threadList -> !threadList.isEmpty() && threadList.size() == 1);
    }

    @Test
    public void insertAndGetDirtyThreads() {
        // set the thread as dirty and insert it
        THREAD.setDirty(true);
        mDataSource.insertThread(THREAD).blockingAwait();

        // get the list of dirty threads which should be of size 1
        mDataSource
                .getDirtyThreads()
                .test()
                .assertValue(threadList -> !threadList.isEmpty() && threadList.size() == 1);
    }

    @Test
    public void insertAndUpdateDirtyThreads() {
        // insert dirty thread
        THREAD.setDirty(true);
        mDataSource.insertThread(THREAD).blockingAwait();

        // make sure thread was inserted
        mDataSource.getDirtyThreads()
                .test()
                .assertValue(threadList -> !threadList.isEmpty() && threadList.size() == 1);

        // update dirty thread
        THREAD.setDirty(false);
        mDataSource
                .updateDirtyThread(THREAD)
                .test()
                .assertNoErrors();

        // make sure dirty threads is empty
        mDataSource.getDirtyThreads()
                .test()
                .assertValue(List::isEmpty);
    }
}
