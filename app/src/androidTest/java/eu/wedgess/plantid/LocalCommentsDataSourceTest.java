/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.local.CommentsLocalDataSource;
import eu.wedgess.plantid.data.source.local.ThreadsLocalDataSource;

/**
 * Integration tests for the {@link eu.wedgess.plantid.data.source.local.CommentsLocalDataSource} implementation with Room.
 * <p>
 * Created by gar on 07/03/18.
 */
public class LocalCommentsDataSourceTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    // Thread is a Foreign Key so must be inserted before comments
    private static final Thread THREAD = new Thread("24f15v13-n61z-494b-8fse-4ba1gc19a331", "Thread title", "Thread messgae", "12w02b36-a40a-494b-8dde-0ba3dc19a883",
            "2018-02-16T01:14:47+00:00", "http://garethwilliams.host:5000/static/images/profile/66b02b36-a40a-494b-8dde-0ba3dc19a883_25.jpg",
            "Deansgrange", false, false);

    private static final Comment COMMENT = new Comment("12e34r21-f32s-123r-8fse-4ba1gc19a331", "message", "12w02b36-a40a-494b-8dde-0ba3dc19a883",
            "2018-02-16T01:14:48+00:00", "24f15v13-n61z-494b-8fse-4ba1gc19a331", false, false);

    private CommentsLocalDataSource mCommentsDataSource;
    private ThreadsLocalDataSource mThreadsDataSource;

    @Before
    public void initDb() throws Exception {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        AppDatabase mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                AppDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();
        mThreadsDataSource = new ThreadsLocalDataSource(mDatabase.threadDao());
        mCommentsDataSource = new CommentsLocalDataSource(mDatabase.commentDao());
    }

    @Test
    public void insertAndGetComment() {
        // Insert new thread before comment due to foreign key constraint
        mThreadsDataSource.insertThread(THREAD).blockingAwait();
        mCommentsDataSource.insertOrUpdateComment(COMMENT).blockingAwait();

        mCommentsDataSource
                .getCommentById(COMMENT.getThreadId(), COMMENT.getId())
                .test()
                // assertValue asserts that there was only one emission of the comment
                .assertValue(comment -> {
                    // The emitted comment is the expected one
                    return comment != null && comment.getId().equals(COMMENT.getId()) &&
                            comment.getMessage().equals(COMMENT.getMessage());
                });
    }

    @Test
    public void updateAndGetComment() {
        // Insert new thread before comment due to foreign key constraint
        mThreadsDataSource.insertThread(THREAD).blockingAwait();
        mCommentsDataSource.insertOrUpdateComment(COMMENT).blockingAwait();

        // When we are updating the name of the comment
        Comment updatedComment = new Comment(COMMENT.getId(), "New message", COMMENT.getAuthorId(), COMMENT.getDateCreated(),
                COMMENT.getThreadId(), true, false);

        mCommentsDataSource.updateComment(updatedComment).blockingAwait();

        // When subscribing to the emissions of the comment
        mCommentsDataSource
                .getCommentById(COMMENT.getThreadId(), COMMENT.getId())
                .test()
                // assertValue asserts that there was only one emission of the comment
                .assertValue(comment -> {
                    // The emitted comment is the expected one
                    return comment != null && comment.getId().equals(COMMENT.getId()) &&
                            comment.getMessage().equals("New message") && comment.isAnswer();
                });
    }

    @Test
    public void deleteAndGetComment() {
        // Insert new thread before comment due to foreign key constraint
        mThreadsDataSource.insertThread(THREAD).blockingAwait();
        mCommentsDataSource.insertOrUpdateComment(COMMENT).blockingAwait();

        // When we are deleting the comment
        mCommentsDataSource.deleteComment(COMMENT).blockingAwait();

        // get the emissions of the comment
        mCommentsDataSource
                .getCommentById(COMMENT.getThreadId(), COMMENT.getId())
                .test()
                // check that there's no user emitted
                .assertNoValues();
    }

    @Test
    public void insertAndGetCommentsByThreadId() {
        // Insert new thread before comment due to foreign key constraint
        mThreadsDataSource.insertThread(THREAD).blockingAwait();
        mCommentsDataSource.insertOrUpdateComment(COMMENT).blockingAwait();

        // get the emissions of the comment
        mCommentsDataSource.getCommentsByThreadId(COMMENT.getThreadId())
                .test()
                // check that list is not empty and contains one comment
                .assertValue(comments -> !comments.isEmpty() && comments.size() == 1);
    }

    @Test
    public void insertAndGetDirtyComments() {
        // Insert new thread before comment due to foreign key constraint
        mThreadsDataSource.insertThread(THREAD).blockingAwait();
        COMMENT.setDirty(true);
        mCommentsDataSource.insertOrUpdateComment(COMMENT).blockingAwait();

        // get the list of dirty comments which should be of size 1
        mCommentsDataSource
                .getDirtyComments()
                .test()
                .assertValue(comments -> !comments.isEmpty() && comments.size() == 1);
    }

    @Test
    public void insertAndUpdateDirtyComments() {
        // Insert new thread before comment due to foreign key constraint
        mThreadsDataSource.insertThread(THREAD).blockingAwait();
        COMMENT.setDirty(true);
        mCommentsDataSource.insertOrUpdateComment(COMMENT).blockingAwait();

        // make sure comment was inserted
        mCommentsDataSource.getDirtyComments()
                .test()
                .assertValue(commentList -> !commentList.isEmpty() && commentList.size() == 1);

        // update dirty comment
        COMMENT.setDirty(false);
        mCommentsDataSource
                .updateDirtyComment(COMMENT)
                .test()
                .assertNoErrors();

        // make sure dirty comments is empty
        mCommentsDataSource.getDirtyComments()
                .test()
                .assertValue(List::isEmpty);
    }
}
