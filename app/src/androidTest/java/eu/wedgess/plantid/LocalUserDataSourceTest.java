/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

/**
 * Created by gar on 07/03/18.
 */

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.local.UsersLocalDataSource;
import eu.wedgess.plantid.utils.PlantPreferenceManager;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

/**
 * Integration tests for the {@link eu.wedgess.plantid.data.source.local.UsersLocalDataSource} implementation with Room.
 */
public class LocalUserDataSourceTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private static final User USER = new User("12w02b36-a40a-494b-8dde-0ba3dc19a883", "wedgess",
            "http://garethwilliams.host:5000/static/images/profile/66b02b36-a40a-494b-8dde-0ba3dc19a883_25.jpg", "2018-02-16T01:14:45+00:00");

    private static final User CURRENT_USER = new User("66b02b36-a40a-494b-8dde-0ba3dc19a883", "Gardener101",
            "http://garethwilliams.host:5000/static/images/profile/66b02b36-a40a-494b-8dde-0ba3dc19a883_25.jpg", "2018-02-16T01:14:45+00:00");

    private static final String USER_JSON_STRING = "{\n" +
            "    \"u_date_created\": \"2018-02-14T01:14:45+00:00\", \n" +
            "    \"u_id\": \"66b02b36-a40a-494b-8dde-0ba3dc19a883\", \n" +
            "    \"u_img_url\": \"http://garethwilliams.host:5000/static/images/profile/66b02b36-a40a-494b-8dde-0ba3dc19a883_25.jpg\", \n" +
            "    \"u_pwd\": \"pbkdf2:sha256:50000$oLK8b2Kf$bc7c8defd95ee8a047134ff0fd161646d2ac5a8582e0157250129b22f57b2611\", \n" +
            "    \"u_uname\": \"Gardener101\"\n" +
            "}";

    private UsersLocalDataSource mDataSource;

    @Before
    public void initDb() throws Exception {
        SharedPreferences mSharedPreferences = Mockito.mock(SharedPreferences.class);
        // Mocking getting the current user from preferences
        Mockito.when(mSharedPreferences.getString(eq(PlantPreferenceManager.PREF_KEY_USER), anyString())).thenReturn(USER_JSON_STRING);
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        AppDatabase mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                AppDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();
        mDataSource = new UsersLocalDataSource(mDatabase.userDao(), mSharedPreferences);
    }

    @After
    public void closeDb() throws Exception {
//        mDatabase.close();
    }

    @SuppressWarnings("Guava")
    @Test
    public void getCurrentUser() {
        // When subscribing to the emissions of the user
        mDataSource.getCurrentUser()
                .test()
                // assertValue asserts that there was only one emission of the user
                .assertValue(user -> {
                    // The emitted user is the expected one
                    return user != null && user.isPresent() && user.get().getUid().equals(CURRENT_USER.getUid()) &&
                            user.get().getUserName().equals(CURRENT_USER.getUserName());
                });
    }

    @Test
    public void insertAndGetUser() {
        // When inserting a new user in the data source, need to block and wait til user is inserted
        mDataSource.insertOrUpdateUser(USER).blockingAwait();

        mDataSource
                .getUserById(USER.getUid())
                .test()
                // assertValue asserts that there was only one emission of the user
                .assertValue(user -> {
                    // The emitted user is the expected one
                    return user != null && user.getUid().equals(USER.getUid()) &&
                            user.getUserName().equals(USER.getUserName());
                });
    }

    @Test
    public void updateAndGetUser() {
        // Given that we have a user in the data source
        mDataSource.insertOrUpdateUser(USER).blockingAwait();

        // When we are updating the name of the user
        User updatedUser = new User(USER.getUid(), "wedgess112", USER.getPassword(), USER.getDateCreated());
        mDataSource.insertOrUpdateUser(updatedUser).blockingAwait();

        // FAILS - For some reason value is empty (works on local DB)
        // When subscribing to the emissions of the user
        mDataSource
                .getUserById(USER.getUid())
                .test()
                // assertValue asserts that there was only one emission of the user
                .assertValue(user -> {
                    // The emitted user is the expected one
                    return user != null && user.getUid().equals(USER.getUid()) &&
                            user.getUserName().equals("wedgess112");
                });
    }

    @Test
    public void deleteAndGetUser() {
        // Given that we have a user in the data source
        mDataSource.insertOrUpdateUser(USER).blockingAwait();

        // When we are deleting all users
        mDataSource.deleteUser(USER).blockingAwait();

        // When subscribing to the emissions of the user
        mDataSource.getUserById(USER.getUid())
                .test()
                // check that there's no user emitted
                .assertNoValues();
    }
}
