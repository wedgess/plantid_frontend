# About

This is the client for the PlantID project. The backend(REST API) can be found [here](https://bitbucket.org/wedgess/plantid_backend/src).
The CNN model and build environment can be found [here](https://bitbucket.org/wedgess/plantid_transfer_learning/src).
The model and labels are already present in the assets folder of this project. If you want to train a custom CNN model the environment is ready to be downloaded [here](https://bitbucket.org/wedgess/plantid-keras-cnn/src) and contains instructions on how to build it.

**The application can be downloaded [here](https://bitbucket.org/wedgess/plantid_frontend/downloads/app-release_v1.apk).**

--------------------------------------------------------
## Screenshots

![Home Screen](https://i.imgur.com/Q7z0gQL.jpg "Home Screen")
![Threads Screen](https://i.imgur.com/eVMLhPD.png "Threads Screen")

![Plants Screen](https://i.imgur.com/lJJBxNn.jpg "Plants Screen")
![Settings Screen](https://i.imgur.com/bcfsRHY.jpg "Settings Screen")

![History Screen](https://i.imgur.com/BI42oRU.png "History Screen")
![Edit User Profile Screen](https://i.imgur.com/B9mr3sY.jpg "Edit User Profile Screen")

![Thread Details Screen](https://i.imgur.com/JqUUbes.png "Thread Details Screen")
![Classification Screen](https://i.imgur.com/N6xehFW.jpg "Classification Screen")

![Plant Details Screen](https://i.imgur.com/NdvpQnL.jpg "Plant Details Screen")
